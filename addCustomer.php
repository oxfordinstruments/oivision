<?php
if (isset($_COOKIE["mm1"])){
	if (!isset($_COOKIE["mm2"])){
		header("location:error.php?e='You do not have permission to access this page!'");
	}
}else{
	header("location:index.php");
}
ob_start();
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$result = mysql_query("SELECT * FROM `customers` ORDER BY customer_name ASC");
$resultStates = mysql_query("SELECT * FROM `states`");

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>




<meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible">
<meta content="Oxford Instruments: OiVision" name="title">
<meta content="Justin Davis" name="author">
<meta content="Nanotechnology, XRF analysers, microanalysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS microanalysis, Oxford Instruments" name="keywords">

<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/form.css" />

<script type='text/javascript'>
function submitcheck()
{
	var $blank="";

	
	if(document.getElementById("name").value=="") $blank = $blank + "Please fill in Site Name\n";
	if(document.getElementById("customer_id").value=="") $blank = $blank + "Please fill in Customer ID\n";
	if(document.getElementById("address").value=="") $blank = $blank + "Please fill in Address\n";
	if(document.getElementById("city").value=="") $blank = $blank + "Please fill in City\n";
	if(document.getElementById("state").value=="") $blank = $blank + "Please fill in State\n";
	if(document.getElementById("zip").value=="") $blank = $blank + "Please fill in Zip\n";
	if(document.getElementById("phone").value=="") $blank = $blank + "Please fill in Phone\n";
	if(document.getElementById("rcvemail").checked == true){
		if(document.getElementById("email").value=="") $blank = $blank + "Please fill in Email Address\n";
	}
	if(document.getElementById("txtCustomer").innerHTML!=" ") $blank = $blank + "Customer ID already exsists\n";
			
	if($blank=="")
	{
		document.forms["form"].submit();
	}else{
		alert($blank);		
	}

}

function checkCustomer(str)
{
if (str=="")
  {
  document.getElementById("txtCustomer").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtCustomer").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","addCheckCustomerId.php?q="+str,true);
xmlhttp.send();
}
</script>
</head>
<body>
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div>
<br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>

<div id="stylized" class="myform">
<form id="form" name="form" method="post" autocomplete="off" action="addCustomerDo.php">
<h1>Add Customer Form</h1>
<p>Fields with a * are required</p>

<label>Name</label>
<input type="text" name="name" id="name" />

<label>ID<span class="small" id="txtCustomer" style='color:#FF0000'> </span></label>
<input type="text" name="customer_id" id="customer_id" onblur="checkCustomer(this.value)" />

<label>Address</label>
<input type="text" name="address" id="address" />

<label>City</label>
<input type="text" name="city" id="city" />

<label>State</label>

<select name="state" id="state">
		  <option value=""></option>
          <?php
			while($rowStates = mysql_fetch_array($resultStates))
  			{
    			echo "<option value='" . $rowStates['abv'] . "'>" . $rowStates['name'] . "</option>\n";
  			}
          ?>
</select>

<label>Zip</label>
<input type="text" name="zip" id="zip" />

<label>Phone</label>
<input type="text" name="phone" id="phone" />

<label>Email</label>
<input type="text" name="email" id="email" />

<label>Receive Email</label>
<input type="checkbox" name="rcvemail" id="rcvemail" value="Y"/>

<label>Notes</label>
<textarea name="notes" id="notes"></textarea>
<input name="Submit" type="button" value="Add Customer" class="button" onclick="submitcheck()" />
<div class="spacer"></div>

</form>
</div>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>
