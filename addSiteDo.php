<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />

<script type="text/javascript">
<!--
function delayer(){
    window.location = "manage.php"
}
//-->
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div>
<br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>

  <h1>
      <?php

ob_start();
require("mysqlInfo.php");
$tbl_name="users"; // Table name
$settings = new SimpleXMLElement('settings.xml', null, true);
// Connect to server and select databse.
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$site_id=$_POST['site_id'];
$customer_id=$_POST['customer_id'];
$mac=$_POST['mac'];
$name=$_POST['name'];
$address=$_POST['address'];
$city=$_POST['city'];
$state=$_POST['state'];
$zip=$_POST['zip'];
if(isset($_POST['active'])){$enabled=$_POST['active'];}else{$enabled="N";}
if(isset($_POST['Storage'])){$storage=$_POST['Storage'];}else{$storage="N";}
if (isset($_POST['notes'])){$notes=$_POST['notes'];}else{$notes="";}
	
	$sql="CREATE TABLE `$mac` (
	`index` INT(11) NOT NULL AUTO_INCREMENT,
	`date` VARCHAR(50) NULL DEFAULT NULL,
	`Tc` DOUBLE NULL DEFAULT NULL,
	`Tf` DOUBLE NULL DEFAULT NULL,
	`Rh` DOUBLE NULL DEFAULT NULL,
	`He` DOUBLE NULL DEFAULT NULL,
	`HeP` DOUBLE NULL DEFAULT NULL,
	`Wf` DOUBLE NULL DEFAULT NULL,
	`Wtc` DOUBLE NULL DEFAULT NULL,
	`Wtf` DOUBLE NULL DEFAULT NULL,
	`FieldOn` VARCHAR(2) NULL DEFAULT NULL,
	`CompOn` VARCHAR(2) NULL DEFAULT NULL,
	`Storage` VARCHAR(2) NULL DEFAULT NULL,
	`insertDate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  	`new` TINYINT(1) NULL DEFAULT '0',
	PRIMARY KEY (`index`)) COMMENT='Name = ".$name."';";
	
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}
	
	$sql="ALTER TABLE `$mac`  COMMENT='$name';";
	
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}
	
//Old SQL 8-21-12	//$sql="INSERT INTO `sites` (`mac`, `enabled`, `site_id`, `site_name`, `site_address`, `site_city`, `site_st`, `site_zip`, `customer_id`, `He`, `HeB`, `HeP`, `Tf`, `Rh`, `Wf`, `Wtf`, `FieldOn`, `CompOn`, `Storage`, `notes`) VALUES ('$mac', '$enabled', '$site_id', '$name', '$address', '$city', '$state', '$zip', '$customer_id', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '$storage', '$notes');";
	
$ahe=$settings->HeLevelThreshhold ; //he
$ahec=$settings->HeLevelCriticalThreshhold ; //he critical
$aheb=$settings->HeBurnThreshhold ; //heburn
$avphc=$settings->HePressHighCriticalThreshhold ; //hep h critical
$avph=$settings->HePressHighThreshhold ; //hep h
$avpl=$settings->HePressLowThreshhold ; //hep l
$avplc=$settings->HePressLowCriticalThreshhold ; //hep l critical
$awfhc=$settings->WaterFlowHighCriticalThreshhold ; //wf h Critical
$awfh=$settings->WaterFlowHighThreshhold ; //wf h
$awfl=$settings->WaterFlowLowThreshhold ; //wf l
$awflc=$settings->WaterFlowLowCriticalThreshhold ; //wf l Critical
$awthc=$settings->WaterTempHighCriticalThreshhold ; //wt h Critical
$awth=$settings->WaterTempHighThreshhold ; //wt h
$awtl=$settings->WaterTempLowThreshhold ; //wt l
$awtlc=$settings->WaterTempLowCriticalThreshhold ; //wt l Critical
$arthc=$settings->RoomTempHighCriticalThreshhold ; //rt h Critical
$arth=$settings->RoomTempHighThreshhold ; //rt h
$artl=$settings->RoomTempLowThreshhold ; //rt l
$artlc=$settings->RoomTempLowCriticalThreshhold ; //rt l Critical
$arhhc=$settings->RoomHumHighCriticalThreshhold ; // rh h Critical
$arhh=$settings->RoomHumHighThreshhold ; // rh h
$arhl=$settings->RoomHumLowThreshhold ; // rh l
$arhlc=$settings->RoomHumLowCriticalThreshhold ; // rh l Critical
$ashe=$settings->StoreHeLevelThreshhold ; // Storage He 
$ashec=$settings->StoreHeLevelCriticalThreshhold ; // Storage He Critical
$asvphc=$settings->StoreHePressHighCriticalThreshhold ; // Storage Hep h Critical
$asvph=$settings->StoreHePressHighThreshhold ; // Storage Hep h Critical
$asvpl=$settings->StoreHePressLowThreshhold ;  // Storage Hep l Critical
$asvplc=$settings->StoreHePressLowCriticalThreshhold ;  // Storage Hep l Critical
	
$sql="INSERT INTO `sites` (`mac`, `enabled`, `site_id`, `site_name`, `site_address`, `site_city`, `site_st`, `site_zip`, `customer_id`, `He`, `HeB`, `HeP`, `Tf`, `Rh`, `Wf`, `Wtf`, `FieldOn`, `CompOn`, `Storage`, `notes`, `alarm_heb`, `alarm_he`, `alarm_hec`, `alarm_vphc`, `alarm_vph`, `alarm_vpl`, `alarm_vplc`, `alarm_wthc`, `alarm_wth`, `alarm_wtl`, `alarm_wtlc`, `alarm_wfhc`, `alarm_wfh`, `alarm_wfl`, `alarm_wflc`, `alarm_rthc`, `alarm_rth`, `alarm_rtl`, `alarm_rtlc`, `alarm_rhhc`, `alarm_rhh`, `alarm_rhl`, `alarm_rhlc`, `alarm_she`, `alarm_shec`, `alarm_svphc`, `alarm_svph`, `alarm_svpl`, `alarm_svplc`) VALUES ('$mac', '$enabled', '$site_id', '$name', '$address', '$city', '$state', '$zip', '$customer_id', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '$storage', '$notes', '$aheb', '$ahe', '$ahec', '$avphc', '$avph', '$avpl', '$avplc', '$awthc', '$awth', '$awtl', '$awtlc', '$awfhc', '$awfh', '$awfl', '$awflc', '$arthc', '$arth', '$artl', '$artlc', '$arhhc', '$arhh', '$arhl', '$arhlc', '$ashe', '$ashec', '$asvphc', '$asvph', '$asvpl', '$asvplc');";
	
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}
	echo "Added Site !";
	echo "<br />";
	echo "Name = ". $name;
	echo "<br />";
	echo "Site ID = ". $site_id;
	echo "<br />";

mysql_close();
?>
      <br />
      <span class="red">Page will return to management page in 3 seconds</span></h1>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>

