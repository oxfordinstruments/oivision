<?php
if (isset($_COOKIE["mm1"])){
		if (!isset($_COOKIE["mm2"])){
			header("location:error.php?e='You do not have permission to access this page!'");
		}
	}else{
		header("location:index.php");
	}
ob_start();
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$result = mysql_query("SELECT * FROM `customers` ORDER BY customer_name ASC");
$resultcarrier = mysql_query("SELECT * FROM `carrier` ORDER BY company ASC");


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<link rel="icon" type="image/png" href="/images/OIIcon.png" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/form.css" />
<script type='text/javascript' src='js/jsmd5.js'></script>
<script type='text/javascript'>
function submitcheck()
{
  $blank = "";
  if(document.getElementById("name").value == "") $blank = "Name cannot be blank\n" + $blank;
  if(document.getElementById("uid").value == "") $blank = "User ID cannot be blank\n" + $blank;
  if(document.getElementById("cust").value == "") $blank = "Customer cannot be blank\n" + $blank;
  if(document.getElementById("sms").checked){
	  if(document.getElementById("cell").value == "") $blank = "Cell Phone number cannot be blank\n" + $blank; 
	  if(document.getElementById("carrier").value == "") $blank = "Cell Carrier cannot be blank\n" + $blank; 	
  }
  if(document.getElementById("emailaddy").value == "") $blank = "Email Address cannot be blank\n" + $blank; 	
  if($blank != ""){alert($blank);}
  if($blank == ""){
	if (confirm("Add new user?"))
		{
			document.forms["form"].submit();
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function checkUser(str)
{
if (str=="")
  {
  document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtUser").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","addCheckUserId.php?q="+str,true);
xmlhttp.send();
}
/////////////////////////////////////////////////////////////////////////////////////
function checkCell(str){
if (str=="")
  {
  //document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    //document.getElementById("txtUser").innerHTML=xmlhttp.responseText;
	  if(xmlhttp.responseText.replace(/ /g,'') == "1"){
		  alert("The cell number format is invalid e.g. 123-123-1234");
	  }else{
		if(xmlhttp.responseText.replace(/ /g,'') != "0"){
			alert("The cell number already exsists for user(s) " + xmlhttp.responseText);
		}
	  }
    }
  }
xmlhttp.open("GET","addCheckCell.php?q="+str,true);
xmlhttp.send();
	
}
/////////////////////////////////////////////////////////////////////////////////////
function checkEmail(str){
if (str=="")
  {
  //document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		//var emailAdd = $(xmlhttp.responseText).text().replace(/ /g,'');
	  if(xmlhttp.responseText.replace(/ /g,'') == "2"){
		  alert("The email address is an invalid format");
	  }else{
		if(xmlhttp.responseText.replace(/ /g,'') != "0"){
			alert("The email address already exsists for user(s) " + xmlhttp.responseText);
		}
	  }
    }
  }
xmlhttp.open("GET","addCheckEmail.php?q="+str,true);
xmlhttp.send();     	
	
}
function validateEmail(elementValue){  
       var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
       return emailPattern.test(elementValue);  
} 
/////////////////////////////////////////////////////////////////////////////////////
function showhidesms(){
    if(document.getElementById("sms").checked){
		document.getElementById("smsinfo").style.display = "block";	
	}else{
		document.getElementById("smsinfo").style.display = "none";
		document.getElementById("cell").value = "";
		document.getElementById("carrier").selectedIndex = 0;		
	}
}

/////////////////////////////////////////////////////////////////////////////////////
function showhideemail(){ 
    if(document.getElementById("email").checked){
		document.getElementById("emailinfo").style.display = "block";	
	}else{
		document.getElementById("emailinfo").style.display = "none";
		document.getElementById("emailaddy").value = "";		
	}
}
</script>
</head>
<body>
<div id="center-x">
  <div id="header"></div>
  <div id="menu">
    <?php include("menu/manageMenu.php");?>
  </div>
  <br>
  <div class="bodytext" style="margin:15px;margin-top:5px;">
    <div id="main-box" style="padding-left:30px;"></div>
    <div id="stylized" class="myform">
      <form id="form" name="form" method="post" autocomplete="off" action="addUserDo.php">
        <h1>Add User Form</h1>
        <p>All <span class="redtxt">Red</span> fields are required</p>
        
        <label class="red">Name <span class="small">Users Real name</span></label>
        <input type="text" name="name" id="name" />
        
        <label class="red">User Name <span class="small" id="txtUser" style='color:#FF0000'> </span></label>
        <input type="text" name="uid" id="uid" onblur="checkUser(this.value)" />
        
        <label class="red">Email Address <span class="small">Required</span></label>
        <input name="emailaddy" type="text" id="emailaddy" onblur="checkEmail(this.value)" />
        
        <label class="red">Customer <span class="small">User belongs to</span></label>
        <select name="cust" id="cust">
          <option value=""></option>
          <?php
while($row = mysql_fetch_array($result))
  {
    echo "<option value='" . $row['customer_id'] . "'>" . $row['customer_name'] . " - " . $row['customer_id'] . "</option>\n";
  }
?>
        </select>

        
        <label class="red">Enabled <span class="small">Active User</span></label>
        <input name="active" type="checkbox" id="active" value="Y" checked="checked" />
        
        <div class="line"></div>
        <br />
        
        <label>Administrator <span class="small">OiService Employees Only</span></label>
        <input name="mgt" type="checkbox" id="mgt" value="Y" />
        
        <label>Site Manager <span class="small">Change Site Info</span></label>
        <input name="site_mgr" type="checkbox" id="site_mgr" value="Y" />
        
        <label>SMS Alarms <span class="small">Recv Critical Txt Msgs</span></label>
        <input name="sms" type="checkbox" id="sms" value="Y" onChange="showhidesms()" />
        
        <div id="smsinfo" style="display:none">
        
          <label class="red">Cell Number <span class="small">Required for sms alarms</span></label>
          <input name="cell" type="text" id="cell" onblur="checkCell(this.value)"  />
        
          <label class="red">Carrier <span class="small">Users Cell Carrier</span></label>
          <select name="carrier" id="carrier">
            <option value=""></option>
            <?php
while($row = mysql_fetch_array($resultcarrier))
  {
    echo "<option value='" . $row['carrierid'] . "'>" . $row['company'] . "</option>\n";
  }
?>
          </select>
        </div>
        
        <label>Email Alarms <span class="small">Send alarms to email</span></label>
        <input name="email" type="checkbox" id="email" value="Y" onChange="showhideemail()" />
                
        <input name="Submit" type="button" value="Add User" class="button" onclick="submitcheck()" />
        <div class="spacer"></div>
      </form>
    </div>
  </div>
  <br>
  <div id="footer"></div>
</div>
</body>
</html>
