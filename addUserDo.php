<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/form.css" />


<script type="text/javascript">
<!--
function delayer(){
    window.location = "manage.php"
}
//-->
</script>
</head>
<body onLoad="setTimeout('delayer()', 15000)">
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div>
<br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>

<?php
ob_start();
set_include_path('/home/oivisi5/php/');
require("mysqlInfo.php");
$tbl_name="users"; // Table name

// Connect to server and select databse.
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$username=$_POST['uid'];
$name=$_POST['name'];
$customer_id=$_POST['cust'];
$cell=$_POST['cell'];
$cell = ereg_replace("[^0-9]", "", $cell);
$carrier=$_POST['carrier'];
$emailaddy=$_POST['emailaddy'];
if (isset($_POST['mgt']))
{
	$manager=strtoupper($_POST['mgt']);
}
else
{
	$manager="N";
}
if (isset($_POST['active']))
{
	$active=strtoupper($_POST['active']);
}
else
{
	$active="N";
}
if (isset($_POST['sms']))
{
	$sms=strtoupper($_POST['sms']);
}
else
{
	$sms="N";
}
if (isset($_POST['email']))
{
	$email=strtoupper($_POST['email']);
}
else
{
	$email="N";
}
if (isset($_POST['site_mgr']))
{
	$site_mgr=strtoupper($_POST['site_mgr']);
}
else
{
	$site_mgr="N";
}


if($manager<>"Y"){ $manager="N";}
$pwd=createRandomPassword();
$password=md5($pwd."b00b135");

	$sql="INSERT INTO `users` (`uid`, `pwd`, `name`, `customer_id`, `active`, `mgt`, `sms`, `cell`, `carrier`, `email`, `emailaddy`, `site_mgr`) VALUES ('$username', '$password', '$name', '$customer_id', '$active', '$manager', '$sms', '$cell', '$carrier', '$email', '$emailaddy', '$site_mgr') ON DUPLICATE KEY UPDATE `pwd` = '$password';";
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}
	echo "<h1>";
	echo "Added user !";
	echo "<br />";
	echo "Name = ". $name;
	echo "<br />";
	echo "Username = ". $username;
	echo "<br />";
	echo "Active = ". $active;
	echo "<br />";
	echo "Administrator = ". $manager;
	echo "<br />";
	echo "Site Manager = ". $site_mgr;
	echo "<br />";
	echo "Email = ". $emailaddy;
	echo "<br />";
	echo "Recieve Email Alerts = ". $email;
	echo "<br />";
	echo "Recieve SMS Alerts = ". $sms;
	echo "<br />";
	if(strtolower($sms) == "y"){
		echo "Cell = ". $cell;
		echo "<br />";
	}
	echo "Linked to Customer ID ". $customer_id;
	echo "<br /><br />";
	echo "DO NOT REFRESH THIS PAGE !";
	echo "<br /><br />";
	echo "Write down this password now !";
	echo "<br />";
	echo "<strong style=\"color:#ff0000\">Users Password = ".$pwd."</strong>";
	echo "</h1><br /><br />";
	echo "The password can be changed if the user logs in and navigates to the user settings page.";

$sql="SELECT `emailaddy` FROM users WHERE `uid` = 'root';";
$result=mysql_query($sql);
$row = mysql_fetch_array($result);
//error_reporting(0); //show no errors
include('Mail.php');
include('Mail/mime.php');

// Constructing the email
$sender = "Users@Oi-Vision.com"; 
if($emailaddy != ""){
	$recipient = $row['emailaddy'] .", ". $emailaddy;
}else{
	$recipient = $row['emailaddy'];
}
$html = "<html><head></head><body>
<h1>User Added to Oi-Vision.com !
<br />
Name = ". $name . "
<br />
Username = ". $username ."
<br />
Active = ". $active ."
<br />
Administrator = ". $manager ."
<br />
Site Manager = ". $site_mgr ."
<br />
Email = ". $emailaddy ."
<br />
Recieve Email Alerts = ". $email ."
<br />
Recieve SMS Alerts = ". $sms ."
<br />";
if(strtolower($sms) == "y"){
	$html .= "Cell = ". $cell ."
	<br />";	
}
$html .="

Linked to Customer ID ". $customer_id ."
<br /><br />
<strong style=\"color:#ff0000\">Users Password = ".$pwd."</strong>
</h1><br /><br />
The password can be changed if the user logs in and navigates to the user settings page.
</body></html>";

$subject = 'Oi-Vision.com User added ' . $username;
$text = 'Oi-Vision.com User added ' . $username;
$headers = array(
					  'From'          => $sender,
					  'Return-Path'   => $sender,
					  'Subject'       => $subject
					  );
// Creating the Mime message
$mime = new Mail_mime($crlf);

// Setting the body of the email
$mime->setTXTBody($text);
$mime->setHTMLBody($html);

// Set body and headers ready for base mail class
$body = $mime->get();
$headers = $mime->headers($headers);
//echo $html; // Testing only
$mail =& Mail::factory('mail');
$result = $mail->send($recipient, $headers, $body);
if($result == 1)
{
	//echo "sent";
	echo "";
}
else
{
	header("location:error.php?e=Your message was not sent: " . $result);

}
mysql_close();					  




function createRandomPassword() {
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pass = '' ;
    while ($i <= 7) {
        $num = rand() % 33;
        $tmp = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}

?>
<br />
      <span class="red">Page will return to management page in 15 seconds</span></h1>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>

