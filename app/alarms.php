<?php
ob_start();
//$settings = new SimpleXMLElement('../settings.xml', null, true);
$rows = array();
$data = false;
$sitealarm = false;
$alarm = false;
$sitecritical = false;
$critical = false;
$alarmsites  = array();
$criticalsites  = array();
$reading = "";
$custid = "9999";
require("mysqlInfo.php");

// Connect to server and select databse.
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");

if(isset($_POST{"data"})){
	$data=true;
}
if(isset($_GET{"data"})){
	$data=true;
}
if(isset($_POST{"count"})){
	$count=true;
}
if(isset($_GET{"count"})){
	$count=true;
}
if(isset($_POST{"custid"})){
	$custid=$_POST{"custid"};
}
if(isset($_GET{"custid"})){
	$custid=$_GET{"custid"};	
}

if($custid == "0"){
	$sql = "select * from sites where enabled='Y'";
}else{
	$sql = "select * from `sites` where `enabled`='Y' and `customer_id`='".$custid."'";
}
$resultSites=mysql_query($sql);
//$rowSites = mysql_fetch_array($resultSites);
//$count = mysql_num_rows($result);

while ($rowSites = mysql_fetch_array($resultSites)) {
	$settings = null;
	if(strtolower($rowSites['use_global_alarm']) == "y"){
		$settings = new SimpleXMLElement('../settings.xml', null, true);
	}else{
		$settings = new SimpleXMLElement("<settings></settings>");	
		$settings->HeLevelThreshhold = $rowSites['alarm_he'];
		$settings->HeLevelCriticalThreshhold = $rowSites['alarm_hec'];
		$settings->HeBurnThreshhold = $rowSites['alarm_heb'];
		$settings->HePressHighCriticalThreshhold = $rowSites['alarm_vphc'];
		$settings->HePressHighThreshhold = $rowSites['alarm_vph'];
		$settings->HePressLowThreshhold = $rowSites['alarm_vpl'];
		$settings->HePressLowCriticalThreshhold = $rowSites['alarm_vplc'];
		$settings->WaterFlowHighCriticalThreshhold = $rowSites['alarm_wfhc'];
		$settings->WaterFlowHighThreshhold = $rowSites['alarm_wfh'];
		$settings->WaterFlowLowThreshhold = $rowSites['alarm_wfl'];
		$settings->WaterFlowLowCriticalThreshhold = $rowSites['alarm_wflc'];
		$settings->WaterTempHighCriticalThreshhold = $rowSites['alarm_wthc'];
		$settings->WaterTempHighThreshhold = $rowSites['alarm_wth'];
		$settings->WaterTempLowThreshhold = $rowSites['alarm_wtl'];
		$settings->WaterTempLowCriticalThreshhold = $rowSites['alarm_wtlc'];
		$settings->RoomTempHighCriticalThreshhold = $rowSites['alarm_rthc'];
		$settings->RoomTempHighThreshhold = $rowSites['alarm_rth'];
		$settings->RoomTempLowThreshhold = $rowSites['alarm_rtl'];
		$settings->RoomTempLowCriticalThreshhold = $rowSites['alarm_rtlc'];
		$settings->RoomHumHighCriticalThreshhold = $rowSites['alarm_rhhc'];
		$settings->RoomHumHighThreshhold = $rowSites['alarm_rhh'];
		$settings->RoomHumLowThreshhold = $rowSites['alarm_rhl'];
		$settings->RoomHumLowCriticalThreshhold = $rowSites['alarm_rhlc'];
		$settings->StoreHeLevelThreshhold = $rowSites['alarm_she'];
		$settings->StoreHeLevelCriticalThreshhold = $rowSites['alarm_shec'];
		$settings->StoreHePressHighCriticalThreshhold = $rowSites['alarm_svphc'];
		$settings->StoreHePressHighThreshhold = $rowSites['alarm_svph'];
		$settings->StoreHePressLowThreshhold = $rowSites['alarm_svpl'];
		$settings->StoreHePressLowCriticalThreshhold = $rowSites['alarm_svplc'];	
	}

	
	$sql = "SELECT * FROM `". strtolower($rowSites["mac"])."` ORDER BY `index` DESC Limit 1";
	$resultData=mysql_query($sql);
	$rowData = mysql_fetch_array($resultData);
	
	
	
	if($rowData["index"] !="" && $rowData['insertDate'] != ""){
		
		
		$format = 'Y-m-d H:i:s';
		$date = DateTime::createFromFormat($format, $rowData['insertDate']);
		//echo "Format: $format; " . $date->format('Y-m-d H:i:s') . "<br />";
		
		$datetime1 = date_create('now');
		$interval = date_diff($datetime1, $date);
		//echo $interval->format('%r%a') . "<br />";
		
		if($interval->format('%r%a') <= -1){
			$noreportsites[] = array(
				  0 => $rowSites['site_name'],
				  1 => $rowSites['site_id'],
				  2 => $interval->format('%r%a')
			  );	
		}else{		
		
//----------------------------------------------------------------------------------------------------------------		
			$reading = "He";
			if ($rowSites[$reading] == "Y") {
				if ($rowSites['Storage'] != "Y") {
					$lowthresh = $settings->HeLevelThreshhold;
					$critlowthresh = $settings->HeLevelCriticalThreshhold;
				} else {
					$lowthresh = $settings->StoreHeLevelThreshhold;
					$critlowthresh = $settings->StoreHeLevelCriticalThreshhold;
				}
				if($rowData[$reading] <= $lowthresh){
					$alarm = true;
					if($rowData[$reading] <= $critlowthresh){
						$critical = true;	
					}
				}
			}
	//----------------------------------------------------------------------------------------------------------------		
			$reading = "HeP";
			if ($rowSites[$reading] == "Y") {
				if ($rowSites['Storage'] != "Y") {
					$crithighthresh = $settings->HePressHighCriticalThreshhold;
					$highthresh = $settings->HePressHighThreshhold;
					$lowthresh = $settings->HePressLowThreshhold;
					$critlowthresh = $settings->HePressLowCriticalThreshhold;
				} else {
					$crithighthresh = $settings->StoreHePressHighCriticalThreshhold;
					$highthresh = $settings->StoreHePressHighThreshhold;
					$lowthresh = $settings->StoreHePressLowThreshhold;
					$critlowthresh = $settings->StoreHePressLowCriticalThreshhold;
				}
				if($rowData[$reading] >= $highthresh or $rowData[$reading] <= $lowthresh){
					$alarm = true;
					if($rowData[$reading] >= $crithighthresh or $rowData[$reading] <= $critlowthresh){
						$critical = true;	
					}
				}
			}
	//----------------------------------------------------------------------------------------------------------------
			$reading = "Wf";
			if ($rowSites[$reading] == "Y") {
				$crithighthresh = $settings->WaterFlowHighCriticalThreshhold;
				$highthresh = $settings->WaterFlowHighThreshhold;
				$lowthresh = $settings->WaterFlowLowThreshhold;
				$critlowthresh = $settings->WaterFlowLowCriticalThreshhold;
				if($rowData[$reading] >= $highthresh or $rowData[$reading] <= $lowthresh){
					$alarm = true;
					if($rowData[$reading] >= $crithighthresh or $rowData[$reading] <= $critlowthresh){
						$critical = true;	
					}
				}
			}
	//----------------------------------------------------------------------------------------------------------------
			$reading = "Wtf";
			if ($rowSites[$reading] == "Y") {
				$crithighthresh = $settings->WaterTempHighCriticalThreshhold;
				$highthresh = $settings->WaterTempHighThreshhold;
				$lowthresh = $settings->WaterTempLowThreshhold;
				$critlowthresh = $settings->WaterTempLowCriticalThreshhold;
				if($rowData[$reading] >= $highthresh or $rowData[$reading] <= $lowthresh){
					$alarm = true;
					if($rowData[$reading] >= $crithighthresh or $rowData[$reading] <= $critlowthresh){
						$critical = true;	
					}
				}
			}
	//----------------------------------------------------------------------------------------------------------------
			$reading = "Tf";
			if ($rowSites[$reading] == "Y") {
				$crithighthresh = $settings->RoomTempHighCriticalThreshhold;
				$highthresh = $settings->RoomTempHighThreshhold;
				$lowthresh = $settings->RoomTempLowThreshhold;
				$critlowthresh = $settings->RoomTempLowCriticalThreshhold;
				if($rowData[$reading] >= $highthresh or $rowData[$reading] <= $lowthresh){
					$alarm = true;
					if($rowData[$reading] >= $crithighthresh or $rowData[$reading] <= $critlowthresh){
						$critical = true;	
					}
				}
			}
	//----------------------------------------------------------------------------------------------------------------
			$reading = "Rh";
			if ($rowSites[$reading] == "Y") {
				$crithighthresh = $settings->RoomHumHighCriticalThreshhold;
				$highthresh = $settings->RoomHumHighThreshhold;
				$lowthresh = $settings->RoomHumLowThreshhold;
				$critlowthresh = $settings->RoomHumLowCriticalThreshhold;
				if($rowData[$reading] >= $highthresh or $rowData[$reading] <= $lowthresh){
					$alarm = true;
					if($rowData[$reading] >= $crithighthresh or $rowData[$reading] <= $critlowthresh){
						$critical = true;	
					}
				}
			}
	//----------------------------------------------------------------------------------------------------------------
			
			
			
			
	
			
			 if ($alarm == true) {
				 $sitealarm = true;
				  $alarmsites[] = array(
					  0 => $rowSites['site_name'],
					  1 => $rowSites['site_id']
				  );
			 }
			 $alarm = false;
			 
			 if ($critical == true) {
				 $sitecritical = true;
				  $criticalsites[] = array(
					  0 => $rowSites['site_name'],
					  1 => $rowSites['site_id']
				  );
			 }
			 $critical = false;
		}//end if
	}//end if
}//end while
if(!$data && !$count){
	if ($sitealarm){
		echo "alarm";
	}else{
		echo "noalarm";
	}
}

if($data || $count){
	$return_arr_alarm = array();
	$return_arr_critical = array();
	$return_arr_noreport = array();
	if(count($alarmsites) != 0){
		foreach ($alarmsites as &$value) {
			$row_array_alarm['site_id'] = $value[1];
			$row_array_alarm['site_name'] = $value[0];
			array_push($return_arr_alarm,$row_array_alarm);
		}	
	}
	$json = json_encode($return_arr_alarm);
	if(!$count){
		echo "{\"alarms\":" . $json . ", ";
	}
	if(count($criticalsites) != 0){
		foreach ($criticalsites as &$value) {
			$row_array_critical['site_id'] = $value[1];
			$row_array_critical['site_name'] = $value[0];
			array_push($return_arr_critical,$row_array_critical);
		}	
	}
	$json = json_encode($return_arr_critical);
	if(!$count){
		echo "\"critical\":" . $json . ", ";
	}
	if(count($noreportsites) != 0){
		foreach ($noreportsites as &$value) {
			$row_array_noreport['site_id'] = $value[1];
			$row_array_noreport['site_name'] = $value[0];
			$row_array_noreport['day_value'] = intval($value[2]);
			array_push($return_arr_noreport,$row_array_noreport);
		}	
	}
	$json = json_encode($return_arr_noreport);
	if(!$count){
		echo "\"noreport\":" . $json . "}";
	}

	if($count){
		echo "Count:".count($return_arr_alarm).":".count($return_arr_critical).":".count($return_arr_noreport);
	}
}
mysql_close();
?>