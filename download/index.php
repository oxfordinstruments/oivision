<?php
if (isset($_COOKIE["mm1"])){
	if (!isset($_COOKIE["mm3"])){
		header("location:../error.php?e='You do not have permission to access this page!'");
	}
}else{
	header("location:../index.php?download");
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<link rel="icon" type="image/png" href="../images/OIIcon.png" />
<link rel="stylesheet" type="text/css" media="screen" href="../css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="../menu/menu_style.css" />
</head>
<body>
<div id="center-x">
	<div id="header"></div>
	<div id="menu">
		<?php if(isset($_COOKIE["mm2"])){include("../menu/manageMenu.php");}else{include("../menu/menu.php");}?>
	</div>
	<br>
	<div class="bodytext" style="margin:15px;margin-top:5px;">
		<div id="main-box" style="padding-left:30px;"></div>
		<h1>Oi Vision Monitor Version 1.7 Firmware</h1>
		<h3>Not compatible with Oi Vision Monitor version 1.4</h3>
		<h3>Flashing OiVision 1.0.7.802_1.7 firmware to an Oi Vision Monitor 1.6 can possibly damage the monitor, voiding any agreements or warranties.</h3>
		<br>
		<h2>Current Release 08-15-2014 </h2>
		<p>&rarr;<a href="OiVision 1.0.7.912_1.7 Firmware.exe">OiVision 1.0.7.912_1.7 Firmware (6.2Mb Download) 8-15-14</a></p>
		<p>&nbsp;</p>
		<br />
		<div class="line"></div>
		<br />
		
		<h1>Oi Vision Monitor Version 1.6 Firmware</h1>
		<h3>Not compatible with Oi Vision Monitor version 1.4</h3>
		<h3>Flashing OiVision 1.0.5.419_1.6 firmware to an Oi Vision Monitor 1.4 can possibly damage the monitor, voiding any agreements or warranties.</h3>
		<br>
		<h2>Current Release 10-2-12 </h2>
		<p>&rarr;<a href="OiVision 1.0.5.419_1.6 Firmware.exe">OiVision 1.0.5.419_1.6 Firmware (5.93Mb Download) 10-2-12</a></p>
		<p>&nbsp;</p>
		<h2>Oi Vision Deployment Tool </h2>
		<p>&rarr;<a href="../deploy/deploy1.6">Oi Vision Deploy 1.6</a></p>
		<br />
		<div class="line"></div>
		
		<br>
		<h1>Oi Vision Monitor Version 1.4 Firmware</h1>
		<br>
		<h2>Current Release 1-23-12 </h2>
		<p>&rarr;<a href="OiVision 1.0.4.311_1.4 Firmware.exe">OiVision 1.0.4.311_1.4 Firmware (6.93Mb Download) 7-18-12</a></p>
		<br />
		<h2>Old Releases</h2>
		<p>&rarr;<a href="OiVision 1.0.4.309_1.4 Firmware.exe">OiVision 1.0.4.309_1.4 Firmware (7.30Mb Download) 5-23-12</a></p>
		<p>&rarr;<a href="OiVision 1.0.4.307_1.4 Firmware.exe">OiVision 1.0.4.307_1.4 Firmware (7.30Mb Download) 3-14-12</a></p>
		<p>&nbsp;</p>
		<h2>Oi Vision Deployment Tool </h2>
		<p>&rarr;<a href="../deploy/deploy1.4">Oi Vision Deploy 1.4</a></p>
		<p><br />
			<br>
		</p>
		<div class="line"></div>
		<br />
		
		<h1>Config Cable Drivers</h1>
		<br>
		<p>&rarr;<a href="CDM20824_Setup.exe">Config Cable Drivers (1.66Mb Download)</a></p>
	</div>
	<br>
	<div id="footer"></div>
</div>
</body>
</html>
