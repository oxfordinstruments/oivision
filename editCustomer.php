<?php
if (isset($_COOKIE["mm1"])){
		if (!isset($_COOKIE["mm2"])){
			header("location:error.php?e='You do not have permission to access this page!'");
		}
	}else{
		header("location:index.php");
	}
ob_start();
if(isset($_GET['customer_id'])) {$customer_id=$_GET['customer_id'];}else{header("location:error.php?e=invalid customer id");}
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$resultC = mysql_query("SELECT * FROM `customers` WHERE `customer_id` = '".$customer_id."' limit 1");
$rowC = mysql_fetch_array($resultC);
$resultStates = mysql_query("SELECT * FROM `states`");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/form.css" />

<script type='text/javascript'>
function deletecheck()
{
	if (confirm("Really delete <?php $rowC['customer_name'] ?> ?"))
	{
		document.getElementById("del").value="yes";
		document.forms["form"].submit();
	}
}
function submitcheck()
{
	var $blank="";

	
	if(document.getElementById("name").value=="") $blank = $blank + "Please fill in Site Name\n";
	if(document.getElementById("customer_id").value=="") $blank = $blank + "Please fill in Customer ID\n";
	if(document.getElementById("address").value=="") $blank = $blank + "Please fill in Address\n";
	if(document.getElementById("city").value=="") $blank = $blank + "Please fill in City\n";
	if(document.getElementById("state").value=="") $blank = $blank + "Please fill in State\n";
	if(document.getElementById("zip").value=="") $blank = $blank + "Please fill in Zip\n";
	if(document.getElementById("phone").value=="") $blank = $blank + "Please fill in Phone\n";
	if(document.getElementById("rcvemail").checked == true){
		if(document.getElementById("email").value=="") $blank = $blank + "Please fill in Email Address\n";
	}
	//if(document.getElementById("txtCustomer").innerHTML!=" ") $blank = $blank + "Customer ID already exsists\n";
			
	if($blank=="")
	{
		document.forms["form"].submit();
	}else{
		alert($blank);		
	}

}

function checkCustomer(str)
{
if (str=="")
  {
  document.getElementById("txtCustomer").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtCustomer").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","addCheckCustomerId.php?q="+str,true);
xmlhttp.send();
}
</script>
</head>
<body>
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div><br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>

<div id="stylized" class="myform">
<form id="form" name="form" method="post" autocomplete="off" action="editCustomerDo.php">
<h1>Edit Customer Form</h1>
<p>Fields with a * are required
<br />
<br />
Deleting a customer will asign the deleted customer's site(s) to <br />Platinum Medical Imaging - customer id 0
</p>

<label>Name</label>
<input type="text" name="name" id="name" value="<?php echo $rowC['customer_name']; ?>" />

<label>ID<span class="small"  style='color:#FF0000'>Cannont Change</span></label>
<input type="text" name="customer_id" id="customer_id" readonly value="<?php echo $rowC['customer_id']; ?>" />


<label>Address</label>
<input type="text" name="address" id="address" value="<?php echo $rowC['customer_address']; ?>" />

<label>City</label>
<input type="text" name="city" id="city" value="<?php echo $rowC['customer_city']; ?>" />

<label>State</label>

<select name="state" id="state">
		  <option value=""></option>
          <?php
			while($rowStates = mysql_fetch_array($resultStates))
 			{
			    echo "<option value='" . $rowStates['abv'] . "'";
				if(strtolower($rowStates['abv']) == strtolower($rowC['customer_st'])){echo "selected=selected";}
				echo ">" . $rowStates['name'] ."</option>\n";
			}
		  ?>
</select>

<label>Zip</label>
<input type="text" name="zip" id="zip" value="<?php echo $rowC['customer_zip']; ?>" />

<label>Phone</label>
<input type="text" name="phone" id="phone" value="<?php echo $rowC['customer_phone']; ?>"/>

<label>Email</label>
<input type="text" name="email" id="email" value="<?php echo $rowC['customer_email']; ?>"/>

<label>Receive Email</label>
<input type="checkbox" name="rcvemail" id="rcvemail" value="Y" <?php if(strtolower($rowC['rcv_email']) == "y"){echo "checked";} ?>/>

<label>Notes</label>
<textarea name="notes" id="notes"><?php echo $rowC['notes']; ?></textarea>
<input name="Submit" type="button" value="Edit Customer" class="button" onclick="submitcheck()" />
<?php
if($customer_id != 0)
{
	echo "<input name='Submit' type='button' value='Delete Customer' class='button' onclick='deletecheck()' />";
}
?>
<input name="del" id="del" type="hidden" value="" />
<div class="spacer"></div>

</form>
</div>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>
