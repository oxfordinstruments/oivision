<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />


<script type="text/javascript">
<!--
function delayer(){
    window.location = "manage.php"
}
//-->
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div><br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>
<h1>
<?php

ob_start();
require("mysqlInfo.php");
$tbl_name="users"; // Table name

// Connect to server and select databse.
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$c_id=$_POST['customer_id'];
$c_name=$_POST['name'];
$c_address=$_POST['address'];
$c_city=$_POST['city'];
$c_state=$_POST['state'];
$c_zip=$_POST['zip'];
$c_phone=$_POST['phone'];
if (isset($_POST['email'])){$c_email=$_POST['email'];}else{$c_email="";}
if (isset($_POST['rcvemail'])){$c_rcvemail="Y";}else{$c_rcvemail="N";}
//$c_email=$_POST['email'];
if (isset($_POST['notes'])){$c_notes=$_POST['notes'];}else{$c_notes="";}

if($_POST['del'] == "yes")
{
	$sql="DELETE FROM `customers` WHERE `customer_id`='$c_id'";
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}
	$sql="SELECT * FROM `sites` WHERE `customer_id`='$c_id';";
	$resultChange = mysql_query($sql);
	
	$sql="UPDATE `sites` SET `customer_id`='0' WHERE `customer_id`='$c_id';";
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}
	
	echo "Customer Deleted !!";
	echo "<br />";
	while($rowChange = mysql_fetch_array($resultChange))
	{
		echo "Site ". $rowChange['site_name'] ." has been reasigned to Oi Service - Customer ID 0";
		echo "<br />";
	}
}else{
		$sql="UPDATE `customers` SET  `customer_name`='$c_name', `customer_address`='$c_address', `customer_city`='$c_city', `customer_st`='$c_state', `customer_zip`='$c_zip', `customer_phone`='$c_phone', `customer_email`='$c_email', `rcv_email`='$c_rcvemail', `notes`='$c_notes' WHERE `customer_id`='$c_id';";
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}

	
	
	echo "Edited Customer !";
	echo "<br />";
	echo "Name = ". $c_name;
	echo "<br />";
	echo "ID = ". $c_id;
	echo "<br />";
}

mysql_close();
?>
<br />
      <span class="red">Page will return to management page in 3 seconds</span></h1>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>

