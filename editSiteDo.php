<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />


<script type="text/javascript">
<!--
function delayer(){
    window.location = "manage.php"
}
//-->
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div>
<br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>
<h1>
<?php

ob_start();
require("mysqlInfo.php");
$tbl_name="users"; // Table name

// Connect to server and select databse.
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$site_id=$_POST['site_id'];
$customer_id=$_POST['customer_id'];
$mac=$_POST['mac'];
$name=$_POST['name'];
$address=$_POST['address'];
$city=$_POST['city'];
$state=$_POST['state'];
$zip=$_POST['zip'];
if (isset($_POST['active'])){$enabled=$_POST['active'];}else{$enabled="N";}
if (isset($_POST['Storage'])){$storage=$_POST['Storage'];}else{$storage="N";}
if (isset($_POST['notes'])){$notes=$_POST['notes'];}else{$notes="";}

if (isset($_POST['He'])){$He=$_POST['He'];}else{$He="N";}
if (isset($_POST['HeB'])){$HeB=$_POST['HeB'];}else{$HeB="N";}
if (isset($_POST['HeP'])){$HeP=$_POST['HeP'];}else{$HeP="N";}
if (isset($_POST['Tf'])){$Tf=$_POST['Tf'];}else{$Tf="N";}
if (isset($_POST['Rh'])){$Rh=$_POST['Rh'];}else{$Rh="N";}
if (isset($_POST['Wf'])){$Wf=$_POST['Wf'];}else{$Wf="N";}
if (isset($_POST['Wtf'])){$Wtf=$_POST['Wtf'];}else{$Wtf="N";}
if (isset($_POST['FieldOn'])){$FieldOn=$_POST['FieldOn'];}else{$FieldOn="N";}
if (isset($_POST['CompOn'])){$CompOn=$_POST['CompOn'];}else{$CompOn="N";}




if($_POST['del'] == "yes")
{
	$sql="DELETE FROM `sites` WHERE `site_id`='$site_id'";
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}
	
	$sql="DROP TABLE `".strtolower($mac)."`;";
		
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}
	
	echo "Site Deleted !!";
}else{
		$sql="UPDATE `sites` SET `enabled`='$enabled', `Storage`='$storage', `site_name`='$name', `site_address`='$address', `site_city`='$city', `site_st`='$state', `site_zip`='$zip', `customer_id`='$customer_id', `notes`='$notes', `He`='$He', `HeB`='$HeB', `HeP`='$HeP', `Tf`='$Tf', `Rh`='$Rh', `Wf`='$Wf', `Wtf`='$Wtf', `FieldOn`='$FieldOn', `CompOn`='$CompOn' WHERE `site_id`='$site_id';";
	if (!mysql_query($sql))
	{
		die('Error: ' . mysql_error());
	}

	
	
	echo "Edited Site !";
	echo "<br />";
	echo "Name = ". $name;
	echo "<br />";
	echo "Site ID = ". $site_id;
	echo "<br />";
	echo "Enabled = ". $enabled;
	echo "<br />";
	echo "In Storage = ". $storage;
}

mysql_close();
?>
<br />
      <span class="red">Page will return to management page in 3 seconds</span></h1>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>

