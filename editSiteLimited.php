<?php
if (isset($_COOKIE["mm1"])){
		if (isset($_COOKIE["mm3"]) == false){
			header("location:error.php?e='You do not have permission to access this page!'");
		}
	}else{
		header("location:index.php");
	}
ob_start();
if(isset($_GET['site_id'])) {$site_id=$_GET['site_id'];}else{header("location:error.php?e=invalid site id");}
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$result = mysql_query("SELECT * FROM `customers` ORDER BY customer_name ASC");
$resultS = mysql_query("SELECT * FROM `sites` WHERE `site_id` = '".$site_id."' limit 1");
$rowS = mysql_fetch_array($resultS);
$resultStates = mysql_query("SELECT * FROM `states`");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/form.css" />

<script type='text/javascript'>
function deletecheck()
{
	if (confirm("Really delete <?php $rowS['site_name'] ?> ?"))
	{
		document.getElementById("del").value="yes";
		document.forms["form"].submit();
	}
}
function submitcheck()
{
	var $blank="";

	
	if(document.getElementById("name").value=="") $blank = $blank + "Please fill in Site Name\n";
	if(document.getElementById("mac").value=="") $blank = $blank + "Please fill in MAC\n";
	if(document.getElementById("site_id").value=="") $blank = $blank + "Please fill in Site ID\n";	
	if(document.getElementById("customer_id").value=="") $blank = $blank + "Please fill in Customer ID\n";
	if(document.getElementById("address").value=="") $blank = $blank + "Please fill in Address\n";
	if(document.getElementById("city").value=="") $blank = $blank + "Please fill in City\n";
	if(document.getElementById("state").value=="") $blank = $blank + "Please fill in State\n";
	if(document.getElementById("zip").value=="") $blank = $blank + "Please fill in Zip\n";
	//if(document.getElementById("txtSite").innerHTML!=" ") $blank = $blank + "Site ID already exsists\n";
			
	if($blank=="")
	{
		document.forms["form"].submit();
	}else{
		alert($blank);		
	}

}

function checkSite(str)
{
if (str=="")
  {
  document.getElementById("txtSite").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtSite").innerHTML="Cannot Change Site ID";
    }
  }
xmlhttp.open("GET","addCheckSiteId.php?q="+str,true);
xmlhttp.send();
}

function allalarms()
{
	if(document.getElementById("all").checked){
		document.getElementById("He").checked = true;
		document.getElementById("HeB").checked = true;
		document.getElementById("HeP").checked = true;
		document.getElementById("Tf").checked = true;
		document.getElementById("Rh").checked = true;
		document.getElementById("Wf").checked = true;
		document.getElementById("Wtf").checked = true;
		document.getElementById("FieldOn").checked = true;
		document.getElementById("CompOn").checked = true;		
	}else{
		if (confirm("Are you sure you want to disable all alarms ?"))
		{
			document.getElementById("He").checked = false;
			document.getElementById("HeB").checked = false;
			document.getElementById("HeP").checked = false;
			document.getElementById("Tf").checked = false;
			document.getElementById("Rh").checked = false;
			document.getElementById("Wf").checked = false;
			document.getElementById("Wtf").checked = false;
			document.getElementById("FieldOn").checked = false;
			document.getElementById("CompOn").checked = false;
		}else{
			document.getElementById("all").checked = true;		
		}	
	}	
}

function alarms(){
	if(document.getElementById("He").checked == false){
		document.getElementById("all").checked = false;
	}
	if(document.getElementById("HeB").checked == false){
		document.getElementById("all").checked = false;
	}
	if(document.getElementById("HeP").checked == false){
		document.getElementById("all").checked = false;
	}
	if(document.getElementById("Tf").checked == false){
		document.getElementById("all").checked = false;
	}
	if(document.getElementById("Rh").checked == false){
		document.getElementById("all").checked = false;
	}
	if(document.getElementById("Wf").checked == false){
		document.getElementById("all").checked = false;
	}
	if(document.getElementById("Wtf").checked == false){
		document.getElementById("all").checked = false;
	}
	if(document.getElementById("FieldOn").checked == false){
		document.getElementById("all").checked = false;
	}
	if(document.getElementById("CompOn").checked == false){
		document.getElementById("all").checked = false;
	}	
	if(document.getElementById("He").checked &&
		document.getElementById("HeB").checked &&
	 	document.getElementById("HeP").checked &&
		document.getElementById("Tf").checked &&
		document.getElementById("Rh").checked &&
		document.getElementById("Wf").checked &&
		document.getElementById("Wtf").checked &&
		document.getElementById("FieldOn").checked &&
		document.getElementById("CompOn").checked){ 
		
		document.getElementById("all").checked = true;
	}
}

function CheckStorage(){
	if(document.getElementById("Storage").checked){
		if (confirm("Are you sure you want mark this site as 'In Storage' ?")){	
			document.getElementById("all").checked = false;
			document.getElementById("He").checked = true;
			document.getElementById("HeB").checked = false;
			document.getElementById("HeP").checked = true;
			document.getElementById("Tf").checked = false;
			document.getElementById("Rh").checked = false;
			document.getElementById("Wf").checked = false;
			document.getElementById("Wtf").checked = false;
			document.getElementById("FieldOn").checked = false;
			document.getElementById("CompOn").checked = false;
							
		}else{
			document.getElementById("Storage").checked = false;	
		}
	}else{
		document.getElementById("all").checked = true;
		document.getElementById("He").checked = true;
		document.getElementById("HeB").checked = true;
		document.getElementById("HeP").checked = true;
		document.getElementById("Tf").checked = true;
		document.getElementById("Rh").checked = true;
		document.getElementById("Wf").checked = true;
		document.getElementById("Wtf").checked = true;
		document.getElementById("FieldOn").checked = true;
		document.getElementById("CompOn").checked = true;		
	}
}
</script>
</head>
<body onLoad="alarms()">
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php if(isset($_COOKIE["mm2"])){include("menu/manageMenu.php");}else{include("menu/menu.php");}?></div><br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>

<div id="stylized" class="myform">
<form id="form" name="form" method="post" autocomplete="off" action="editSiteDo.php">
<h1>Edit Site Form</h1>
<p>Fields with a * are required</p>

<label>Name</label>
<input type="text" name="name" id="name" value="<?php echo $rowS['site_name']; ?>" />

<input style="display:none" type="text" name="mac" id="mac" readonly value="<?php echo $rowS['mac']; ?>" />
<input style="display:none" type="text" name="site_id" id="site_id" readonly value="<?php echo $rowS['site_id']; ?>" />
<label>Address</label>
<input type="text" name="address" id="address" value="<?php echo $rowS['site_address']; ?>" />
<label>City</label>
<input type="text" name="city" id="city" value="<?php echo $rowS['site_city']; ?>" />
<label>State</label>
<select name="state" id="state">
		  <option value=""></option>
          <?php
			while($rowStates = mysql_fetch_array($resultStates))
 			{
			    echo "<option value='" . $rowStates['abv'] . "'";
				if(strtolower($rowStates['abv']) == strtolower($rowS['site_st'])){echo "selected=selected";}
				echo ">" . $rowStates['name'] ."</option>\n";
			}
		  ?>
</select>
<label>Zip</label>
<input name="zip" id="zip" value="<?php echo $rowS['site_zip']; ?>" />
<select name="customer_id" id="customer_id" style="display:none">
<option value=""></option>
<?php
while($row = mysql_fetch_array($result))
  {
    echo "<option value='" . $row['customer_id'] . "'";
	if($row['customer_id'] == $rowS['customer_id']){echo "selected=selected";}
	echo ">" . $row['customer_name'] . " - " . $row['customer_id'] . "</option>\n";
  }
?>
</select>

<label>Enabled
<span class="small">Active Site</span></label>
<input name="active" type="checkbox" id="active" value="Y" <?php if(strtolower($rowS['enabled']) == "y"){echo "checked='checked'"; } ?> />

<label>In Storage
<span class="small">Site in storage</span></label>
<input name="Storage" type="checkbox" id="Storage" value="Y" onChange="CheckStorage()" <?php if(strtolower($rowS['Storage']) == "y"){echo "checked='checked'"; } ?> />

<textarea style="display:none" name="notes" id="notes"><?php echo $rowS['notes']; ?></textarea>
<div class="line"></div>
<div><h2>Readings to monitor for alarms</h2></div><br>

<label>Select All</label>
<input name="all" type="checkbox" id="all" value="Y" onClick="allalarms()" />

<label>Helium Level</label>
<input name="He" type="checkbox" id="He" value="Y" onClick="alarms()" <?php if(strtolower($rowS['He']) == "y"){echo "checked='checked'"; } ?> />

<label>Helium Burn</label>
<input name="HeB" type="checkbox" id="HeB" value="Y" onClick="alarms()" <?php if(strtolower($rowS['HeB']) == "y"){echo "checked='checked'"; } ?> />

<label>Helium Pressure</label>
<input name="HeP" type="checkbox" id="HeP" value="Y" onClick="alarms()" <?php if(strtolower($rowS['HeP']) == "y"){echo "checked='checked'"; } ?> />

<label>Water Temp</label>
<input name="Wtf" type="checkbox" id="Wtf" value="Y" onClick="alarms()" <?php if(strtolower($rowS['Wtf']) == "y"){echo "checked='checked'"; } ?> />

<label>Water Flow</label>
<input name="Wf" type="checkbox" id="Wf" value="Y" onClick="alarms()" <?php if(strtolower($rowS['Wf']) == "y"){echo "checked='checked'"; } ?> />

<label>Equip Room Temp</label>
<input name="Tf" type="checkbox" id="Tf" value="Y" onClick="alarms()" <?php if(strtolower($rowS['Tf']) == "y"){echo "checked='checked'"; } ?> />

<label>Equip Room RH</label>
<input name="Rh" type="checkbox" id="Rh" value="Y" onClick="alarms()" <?php if(strtolower($rowS['Rh']) == "y"){echo "checked='checked'"; } ?> />

<label>Magnet Field</label>
<input name="FieldOn" type="checkbox" id="FieldOn" value="Y" onClick="alarms()" <?php if(strtolower($rowS['FieldOn']) == "y"){echo "checked='checked'"; } ?> />

<label>Compressor On</label>
<input name="CompOn" type="checkbox" id="CompOn" value="Y" onClick="alarms()" <?php if(strtolower($rowS['CompOn']) == "y"){echo "checked='checked'"; } ?> />

<div class="line"></div><br>

<input name="Submit" type="button" value="Edit Site" class="button" onclick="submitcheck()" />
<input name="del" id="del" type="hidden" value="" />
<div class="spacer"></div>

</form>
</div>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>
