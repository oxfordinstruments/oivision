<?php
if (isset($_COOKIE["mm1"])){
	if (!isset($_COOKIE["mm2"])){
		header("location:error.php?e='You do not have permission to access this page!'");
	}
}else{
	header("location:index.php");
}
ob_start();
if(isset($_GET['uname'])) {$uname=$_GET['uname'];}else{header("location:error.php?e=invalid username");}
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
$result = mysql_query("SELECT * FROM `customers` ORDER BY customer_name ASC");
$resultU = mysql_query("SELECT * FROM `users` WHERE `uid` = '".$uname."' limit 1");
$rowU = mysql_fetch_array($resultU);
$resultcarrier = mysql_query("SELECT * FROM `carrier` ORDER BY company ASC");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/form.css" />
<script type='text/javascript' src='js/jsmd5.js'></script>
<script type='text/javascript'>

function deletecheck()
{
	if (confirm("Really delete <?php $rowU['name'] ?> ?"))
	{
		document.getElementById("del").value="yes";
		document.forms["form"].submit();
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function submitcheck()
{
	if (confirm("Save changes to user <?php $rowU['name'] ?> ?"))
	{
		$blank = "";
		if(document.getElementById("name").value == "" || document.getElementById("name").value == null){$blank = "Must enter the users real name\n" + $blank;};
		if(document.getElementById("cust").selectedIndex == 0){$blank = "Must select a customer\n" + $blank;};
		if(document.getElementById("emailaddy").value == "" || document.getElementById("emailaddy").value == null){$blank = "Must enter the users email address\n" + $blank;};
		if(document.getElementById("sms").checked){
			if(document.getElementById("cell").value == null || document.getElementById("cell").value == ""){$blank = "Must enter the users cell number\n" + $blank;};
			if(document.getElementById("carrier").selectedIndex == 0){$blank = "Must select the users cell phone carrier\n" + $blank;};
		}
		if($blank != ""){
			alert($blank);
		}else{
			checkEmail(document.getElementById("emailaddy").value);
			if(document.getElementById("sms").checked){
				checkCell(document.getElementById("cell").value);
			}
			if(document.getElementById("emailcheck").value == "" && document.getElementById("cellcheck").value == ""){
				document.forms["form"].submit();
			}else{
				if(document.getElementById("sms").checked){
					alert("User not saved\n" + document.getElementById("emailcheck").value + "\n" + document.getElementById("cellcheck").value);
				}else{
					alert("User not saved\n" + document.getElementById("emailcheck").value);
				}
			}
		}
	}

}
/////////////////////////////////////////////////////////////////////////////////////
function checkUser(str)
{
if (str=="")
  {
  document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtUser").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","addCheckUserId.php?q="+str,true);
xmlhttp.send();
}
/////////////////////////////////////////////////////////////////////////////////////
function pwdchg(){
	if ( document.getElementById("pwdreset").checked == true)
	{
		if (confirm("Do you want to reset the password for <?php $rowU['name'] ?> ?"))
			{
				document.getElementById("pwd").value="yes";
			}
			else
			{
				document.getElementById("pwdreset").checked = false;
			}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function checkCell(str){
if (str=="")
  {
  //document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		//var emailAdd = $(xmlhttp.responseText).text().replace(/ /g,'');
	  if(xmlhttp.responseText.replace(/ /g,'') == "1"){
		  //alert("The email address is an invalid format");
		  document.getElementById("cellcheck").value = "The cell number format is invalid e.g. 123-123-1234";
	  }else{
		if(xmlhttp.responseText.replace(/ /g,'') != "0"){
			//alert("The email address already exsists for user(s) " + xmlhttp.responseText);
			document.getElementById("cellcheck").value = "The cell number already exsists for user(s) " + xmlhttp.responseText;
		}else{
			document.getElementById("cellcheck").value = "";
		}
	  }
    }
  }
xmlhttp.open("GET","editCheckCell.php?q=q="+str+"&x="+ document.getElementById("uid").value ,false);
xmlhttp.send();
	
}
/////////////////////////////////////////////////////////////////////////////////////
function checkEmail(str){
if (str=="")
  {
  //document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		//var emailAdd = $(xmlhttp.responseText).text().replace(/ /g,'');
	  if(xmlhttp.responseText.replace(/ /g,'') == "2"){
		  //alert("The email address is an invalid format");
		  document.getElementById("emailcheck").value = "The email address is an invalid format";
	  }else{
		if(xmlhttp.responseText.replace(/ /g,'') != "0"){
			alert("The email address already exsists for user(s) " + xmlhttp.responseText);
			//document.getElementById("emailcheck").value = "The email address already exsists for user(s) " + xmlhttp.responseText;
		}else{
			document.getElementById("emailcheck").value = "";
		}
	  }
    }
  }
xmlhttp.open("GET","editCheckEmail.php?q="+str+"&x="+ document.getElementById("uid").value ,false);
xmlhttp.send();     	
	
}
/////////////////////////////////////////////////////////////////////////////////////
function validateEmail(elementValue){  
       var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
       return emailPattern.test(elementValue);  
} 
/////////////////////////////////////////////////////////////////////////////////////
function showhidesms(){
    if(document.getElementById("sms").checked){
		document.getElementById("smsinfo").style.display = "block";	
	}else{
		document.getElementById("smsinfo").style.display = "none";
		document.getElementById("cell").value = "";
		document.getElementById("carrier").selectedIndex = 0;		
	}
}

/////////////////////////////////////////////////////////////////////////////////////
/*function showhideemail(){ 
    if(document.getElementById("email").checked){
		document.getElementById("emailinfo").style.display = "block";	
	}else{
		document.getElementById("emailinfo").style.display = "none";
		document.getElementById("emailaddy").value = "";		
	}
}*/
</script>
</head>
<body>
<div id="center-x">
  <div id="header"></div>
  <div id="menu">
    <?php include("menu/manageMenu.php");?>
  </div>
  <br>
  <div class="bodytext" style="margin:15px;margin-top:5px;">
    <div id="main-box" style="padding-left:30px;"></div>
    <div id="stylized" class="myform">
      <form id="form" name="form" method="post" autocomplete="off" action="editUserDo.php">
        <h1>Edit User Form</h1>
        <p>All <span class="redtxt">red</span> fields are required.</p>
       
        <label class="red">Name <span class="small">Users Real name</span></label>
        <input type="text" name="name" id="name" value="<?php echo $rowU['name'];  ?>" />
       
        <label>User Name<span class="small">Cannot be changed</span></label>
        <input type="text" name="uid" id="uid" readonly value="<?php echo $rowU['uid'];  ?>" />
       
        <label class="red">Customer <span class="small">User belongs to</span></label>
        <select name="cust" id="cust">
          <option value=""> </option>
          <?php
while($row = mysql_fetch_array($result))
  {
    echo "<option value='" . $row['customer_id'] . "'";
	if($row['customer_id'] == $rowU['customer_id']){echo "selected=selected";}
	echo ">" . $row['customer_name'] . " - " . $row['customer_id'] . "</option>\n";
  }
?>
        </select>
        
        <label>Enabled <span class="small">Active User</span></label>
        <input name="active" type="checkbox" id="active" value="Y" <?php if(strtolower($rowU['active']) == "y"){echo "checked='checked'"; } ?> />
        
        <label>Administrator <span class="small">OiService Employees Only</span></label>
        <input name="mgt" type="checkbox" id="mgt" value="Y" <?php if(strtolower($rowU['mgt']) == "y"){echo "checked='checked'"; } ?> />
        
        <label>Site Manager <span class="small">Change Site Info</span></label>
        <input name="site_mgr" type="checkbox" id="site_mgr" value="Y" <?php if(strtolower($rowU['site_mgr']) == "y"){echo "checked='checked'"; } ?> />
        
        <label>SMS Alarms <span class="small">Recv Critical Txt Msgs</span></label>
        <input name="sms" type="checkbox" id="sms" value="Y" onChange="showhidesms()" <?php if(strtolower($rowU['sms']) == "y"){echo "checked='checked'"; } ?> />
        
        <div id="smsinfo" <?php if(strtolower($rowU['sms']) != "y"){echo "style=\"display:none\""; } ?>>
          
          <label class="red">Cell Number <span class="small">Required for sms alarms</span></label>
          <input name="cell" type="text" id="cell" value="<?php echo $rowU['cell'];  ?>"  />
          
          <label class="red">Carrier <span class="small">Users Cell Carrier</span></label>
          <select name="carrier" id="carrier">
            <option value=""></option>
            <?php
while($row = mysql_fetch_array($resultcarrier))
  {
	echo "<option value='" . $row['carrierid'] . "'";
	if($row['carrierid'] == $rowU['carrier']){echo "selected=selected";}
	echo ">" . $row['company'] . "</option>\n";  
	  
    //echo "<option value='" . $row['carrierid'] . "'>" . $row['company'] . "</option>\n";
  }
?>
          </select>
        </div>
        
        <label>Email Alarms <span class="small">Send alarms to email</span></label>
        <input name="email" type="checkbox" id="email" value="Y" <?php if(strtolower($rowU['email']) == "y"){echo "checked='checked'"; } ?>/>
      
        <label class="red">Email Address <span class="small">Required</span></label>
        <input name="emailaddy" type="text" id="emailaddy" value="<?php echo $rowU['emailaddy'];  ?>" />

        
        <div class="line"></div>
        <br />
        
        <label>Reset Password <span class="small"></span></label>
        <input name="pwdreset" type="checkbox" id="pwdreset" value="Y" onchange="pwdchg()"/>
        
        <input name="Submit" type="button" value="Edit User" class="button" onclick="submitcheck()" />
        <input name="Submit" type="button" value="Delete User" class="button" onclick="deletecheck()" />
        <input name="pwd" id="pwd" type="hidden" value="" />
        <input name="del" id="del" type="hidden" value="" />
        <input name="emailcheck" id="emailcheck" type="hidden" value="" />
        <input name="cellcheck" id="cellcheck" type="hidden" value="" />
        <div class="spacer"></div>
      </form>
    </div>
  </div>
  <br>
  <div id="footer"></div>
</div>
</body>
</html>
