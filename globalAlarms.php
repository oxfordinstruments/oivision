<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />

<script src="js/smartspin/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="js/smartspin/smartspinner.js" type="text/javascript"></script>
<link href="js/smartspin/smartspinner.css" type="text/css" rel="stylesheet"/>

<script type="text/javascript">
<?php
ob_start();
$settings = new SimpleXMLElement('settings.xml', null, true);
 
 echo "
        $(document).ready(function() {
			$('#chk1').spinit({min:0,max:23,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->AlarmCheckOne." }); //he
			$('#chk2').spinit({min:0,max:23,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->AlarmCheckTwo." }); //he
            $('#he').spinit({min:0,max:99,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->HeLevelThreshhold." }); //he
			$('#hec').spinit({min:0,max:100,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->HeLevelCriticalThreshhold." }); //he critical
			$('#heb').spinit({min:0,max:100,stepInc:0.01,pageInc:0.1, height: 22, width: 140, initValue: ".$settings->HeBurnThreshhold." }); //heburn
			$('#hephc').spinit({min:1,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$settings->HePressHighCriticalThreshhold." }); //hep h critical
			$('#heph').spinit({min:1,max:19,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$settings->HePressHighThreshhold." }); //hep h
			$('#hepl').spinit({min:0,max:5,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$settings->HePressLowThreshhold." }); //hep l
			$('#heplc').spinit({min:0,max:5,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$settings->HePressLowCriticalThreshhold." }); //hep l critical
			$('#wfhc').spinit({min:0,max:50,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterFlowHighCriticalThreshhold." }); //wf h Critical
			$('#wfh').spinit({min:0,max:49,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterFlowHighThreshhold." }); //wf h
			$('#wfl').spinit({min:0,max:49,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterFlowLowThreshhold." }); //wf l
			$('#wflc').spinit({min:0,max:48,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterFlowLowCriticalThreshhold." }); //wf l Critical
			$('#wthc').spinit({min:0,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterTempHighCriticalThreshhold." }); //wt h Critical
			$('#wth').spinit({min:0,max:179,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterTempHighThreshhold." }); //wt h
			$('#wtl').spinit({min:-20,max:179,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterTempLowThreshhold." }); //wt l
			$('#wtlc').spinit({min:-20,max:179,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterTempLowCriticalThreshhold." }); //wt l Critical
			$('#rthc').spinit({min:100,max:120,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomTempHighCriticalThreshhold." }); //rt h Critical
			$('#rth').spinit({min:100,max:119,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomTempHighThreshhold." }); //rt h
			$('#rtl').spinit({min:5,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomTempLowThreshhold." }); //rt l
			$('#rtlc').spinit({min:5,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomTempLowCriticalThreshhold." }); //rt l Critical
			$('#rhhc').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomHumHighCriticalThreshhold." }); // rh h Critical
			$('#rhh').spinit({min:0,max:199,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomHumHighThreshhold." }); // rh h
			$('#rhl').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomHumLowThreshhold." }); // rh l
			$('#rhlc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomHumLowCriticalThreshhold." }); // rh l Critical
			$('#she').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHeLevelThreshhold." }); // Storage He 
			$('#shec').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHeLevelCriticalThreshhold." }); // Storage He Critical
			$('#shephc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHePressHighCriticalThreshhold." }); // Storage Hep h Critical
			$('#sheph').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHePressHighThreshhold." }); // Storage Hep h Critical
			$('#shepl').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHePressLowThreshhold." }); // Storage Hep l Critical
			$('#sheplc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHePressLowCriticalThreshhold." }); // Storage Hep l Critical
	";
?> 
        });
</script>

</head>
<body>
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div>
<br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>
        <div id="aform">
          <form id="alarms" method="post" action="globalAlarmsDo.php">
         	<div id="field" class="afield" style="height:auto; width:100%">
           	    <div id="txt" class="atxt">Alarm Check Hour 1</div>
                <div id="inp" class="ainp"><input type="text" id="chk1" name="chk1" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
           	    <div id="txt" class="atxt">Alarm Check Hour 2</div>
                <div id="inp" class="ainp"><input type="text" id="chk2" name="chk2" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
          	<div id="field" class="afield" style="height:auto; width:100%">
           	    <div id="txt" class="atxt">He Level Low</div>
                <div id="inp" class="ainp"><input type="text" id="he" name="he" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
           	    <div id="txt" class="atxt">He Level Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="hec" name="hec" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Burn</div>
                <div id="inp" class="ainp"><input type="text" id="heb" name="heb" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Pressure High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="hephc" name="hephc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Pressure High</div>
                <div id="inp" class="ainp"><input type="text" id="heph" name="heph" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Pressure Low</div>
                <div id="inp" class="ainp"><input type="text" id="hepl" name="hepl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Pressure Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="heplc" name="heplc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Flow High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="wfhc" name="wfhc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Flow High</div>
                <div id="inp" class="ainp"><input type="text" id="wfh" name="wfh" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Flow Low</div>
                <div id="inp" class="ainp"><input type="text" id="wfl" name="wfl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Flow Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="wflc" name="wflc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Temp High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="wthc" name="wthc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Temp High</div>
                <div id="inp" class="ainp"><input type="text" id="wth" name="wth" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Temp Low</div>
                <div id="inp" class="ainp"><input type="text" id="wtl" name="wtl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Temp Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="wtlc" name="wtlc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Temp High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="rthc" name="rthc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Temp High</div>
                <div id="inp" class="ainp"><input type="text" id="rth" name="rth" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Temp Low</div>
                <div id="inp" class="ainp"><input type="text" id="rtl" name="rtl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Temp Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="rtlc" name="rtlc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Humidity High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="rhhc" name="rhhc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Humidity High</div>
                <div id="inp" class="ainp"><input type="text" id="rhh" name="rhh" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Humidity Low</div>
                <div id="inp" class="ainp"><input type="text" id="rhl" name="rhl" class="smartspinner" /></div>
            </div>
             <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Humidity Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="rhlc" name="rhlc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Level Low</div>
                <div id="inp" class="ainp"><input type="text" id="she" name="she" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Level Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="shec" name="shec" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Press High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="shephc" name="shephc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Press High</div>
                <div id="inp" class="ainp"><input type="text" id="sheph" name="sheph" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Press Low</div>
                <div id="inp" class="ainp"><input type="text" id="shepl" name="shepl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Press Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="sheplc" name="sheplc" class="smartspinner" /></div>
            </div>
              <input type="submit" name="Save" id="asave" value="Save" />	  
          </form>
        </div>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>
