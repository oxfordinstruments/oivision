<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />


<script type="text/javascript">
<!--
function delayer(){
    window.location = "manage.php"
}
//-->
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div><br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>

<?php

ob_start();
$settings = new SimpleXMLElement('settings.xml', null, true);
$settings->AlarmCheckOne=$_POST['chk1'];
$settings->AlarmCheckTwo=$_POST['chk2'];
$settings->HeLevelThreshhold=round((double)$_POST['he'],2);
$settings->HeLevelCriticalThreshhold=round((double)$_POST['hec'],2);
$settings->HeBurnThreshhold=round((double)$_POST['heb'],2);
$settings->HePressHighCriticalThreshhold=round((double)$_POST['hephc'],2);
$settings->HePressHighThreshhold=round((double)$_POST['heph'],2);
$settings->HePressLowThreshhold=round((double)$_POST['hepl'],2);
$settings->HePressLowCriticalThreshhold=round((double)$_POST['heplc'],2);
$settings->WaterFlowHighCriticalThreshhold=round((double)$_POST['wfhc'],2);
$settings->WaterFlowHighThreshhold=round((double)$_POST['wfh'],2);
$settings->WaterFlowLowThreshhold=round((double)$_POST['wfl'],2);
$settings->WaterFlowLowCriticalThreshhold=round((double)$_POST['wflc'],2);
$settings->WaterTempHighCriticalThreshhold=round((double)$_POST['wthc'],2);
$settings->WaterTempHighThreshhold=round((double)$_POST['wth'],2);
$settings->WaterTempLowThreshhold=round((double)$_POST['wtl'],2);
$settings->WaterTempLowCriticalThreshhold=round((double)$_POST['wtlc'],2);
$settings->RoomTempHighCriticalThreshhold=round((double)$_POST['rthc'],2);
$settings->RoomTempHighThreshhold=round((double)$_POST['rth'],2);
$settings->RoomTempLowThreshhold=round((double)$_POST['rtl'],2);
$settings->RoomTempLowCriticalThreshhold=round((double)$_POST['rtlc'],2);
$settings->RoomHumHighCriticalThreshhold=round((double)$_POST['rhhc'],2);
$settings->RoomHumHighThreshhold=round((double)$_POST['rhh'],2);
$settings->RoomHumLowThreshhold=round((double)$_POST['rhl'],2);
$settings->RoomHumLowCriticalThreshhold=round((double)$_POST['rhlc'],2);
$settings->StoreHeLevelThreshhold=round((double)$_POST['she'],2);
$settings->StoreHeLevelCriticalThreshhold=round((double)$_POST['shec'],2);
$settings->StoreHePressHighCriticalThreshhold=round((double)$_POST['shephc'],2);
$settings->StoreHePressHighThreshhold=round((double)$_POST['sheph'],2);
$settings->StoreHePressLowThreshhold=round((double)$_POST['shepl'],2);
$settings->StoreHePressLowCriticalThreshhold=round((double)$_POST['sheplc'],2);

$settings->asXML('settings.xml');
echo "<h1>Alarm Settings have been saved.</h1>";
?>
<br />
      <span class="red">Page will return to management page in 3 seconds</span></h1>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>

