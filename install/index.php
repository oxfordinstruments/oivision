<?php 
$sender ='support@oi-vision.com';// <<=== update to your email address
$recipient ='support@oi-vision.com';// <<=== update to your email address

set_include_path('/home/oivisi5/php/');

session_start();
$errors = '';
$name = '';
$visitor_email = '';
$user_message = '';

if(isset($_POST['submit']))
{
	
	$name = $_POST['name'];
	$visitor_email = $_POST['email'];
	$user_message = $_POST['message'];
	///------------Do Validations-------------
	if(empty($name)||empty($visitor_email))
	{
		$errors .= "\n Name and Email are required fields. ";	
	}
	if(IsInjected($visitor_email))
	{
		$errors .= "\n Bad email value!";
	}
	if(empty($_SESSION['6_letters_code'] ) ||
	  strcasecmp($_SESSION['6_letters_code'], $_POST['6_letters_code']) != 0)
	{
	//Note: the captcha code is compared case insensitively.
	//if you want case sensitive match, update the check above to
	// strcmp()
		$errors .= "\n The captcha code does not match!";
	}
	
	if(empty($errors))
	{
		//send the email
		$to = $recipient;
		$subject="Oi Vision Monitor Support Request";
		$from = $sender;
		$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
		
		$html = "<html><head></head><body><h2>A visitor $name submitted the install.oi-vision.com support contact form:</h2><br />".
		"Name: $name <br />".
		"Email: $visitor_email <br />".
		"Message: <br />".
		"<p>". nl2br($user_message) ."</p><br /><br />".
		"IP: $ip <br /></body></html>";
		
		$text = "A visitor $name submitted the install.oi-vision.com support contact form:\n\n".
		"Name: $name\n".
		"Email: $visitor_email \n".
		"Message: \n ".
		"$user_message\n".
		"IP: $ip\n";	
		
		$headers = array(
					  'From'          => $sender,
					  'Return-Path'   => $sender,
					  'Subject'       => $subject
					  );
		
		include('Mail.php');
		include('Mail/mime.php');
		//mail($to, $subject, $body,$headers);//old way
		// Creating the Mime message
		$mime = new Mail_mime($crlf);
		
		// Setting the body of the email
		$mime->setTXTBody($text);
		$mime->setHTMLBody($html);
		
		// Set body and headers ready for base mail class
		$body = $mime->get();
		$headers = $mime->headers($headers);
		//echo $html; // Testing only
		$mail =& Mail::factory('mail');
		$result = $mail->send($recipient, $headers, $body);
		header('Location: thank-you.html');
	}
}

// Function to validate against any email injection attempts
function IsInjected($str)
{
  $injections = array('(\n+)',
              '(\r+)',
              '(\t+)',
              '(%0A+)',
              '(%0D+)',
              '(%08+)',
              '(%09+)'
              );
  $inject = join('|', $injections);
  $inject = "/$inject/i";
  if(preg_match($inject,$str))
    {
    return true;
  }
  else
    {
    return false;
  }
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/install.css" />
<script language="JavaScript" src="scripts/gen_validatorv31.js" type="text/javascript"></script>
</head>
<body>
<div id="center-x">
    <div id="header"></div>
    <br>
    <div class="bodytext" style="margin:15px;margin-top:5px;">
        <div id="main-box" style="padding-left:30px;"></div>
        <div id="title"> Oi Vision Magnet Monitor </div>
        <div id="title2"> Installation requirements and proceedure </div>
        <div id="topimgandmenu">
            <div id="navcontainer">
                <ul id="navlist">
	                <li><a href="#prerequisits">Prerequisits</a></li>
                    <li><a href="#inthebox">What's In The Box</a></li>
                    <li><a href="#installation">Installation</a>
                        <ul id="subnavlist">
                            <li><a href="#mounting">Mounting</a></li>
                            <li><a href="#wall">Wall Transformer Assembly</a></li>
                            <li><a href="#cable">Cabling</a></li>
                        </ul>
                    </li>
                    <li><a href="#support">Support</a>
                     <ul id="subnavlist">
                            <li><a href="#troubleshooting">Troubleshooting</a></li>
                            <li><a href="#contact">Support Contact Information</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            
            <div id="topimg"> <img src="images/bwbox.JPG" width="100%" height="100%"> </div>
        </div>
        <div id="require" class="item"> 
        <a id="prerequisits"></a> 
        <h1>Prerequisits</h1>
            <ul>
                <li>1 – Internet connection</li>
                <li>1 – CAT5 network jack</li>
                <li>1 – CAT5 Ethernet cable capable of reaching from the GE Magnet Monitor to the customer supplied CAT5 network connection</li>
                <li>1 – 110 VAC power outlet within 6 feet of GE Magnet Monitor</li>
            </ul>
        </div>
        <div id="whatinbox" class="item"> 
        <a id="inthebox"></a> 
        <h1>Items Included in the Oi Vision Package</h1>
            <ul>
                <li>1 – Oi Vision box</li>
                <li>2 – 3 foot 25 pin cables</li>
                <li>1 – 3 foot 15 pin cable</li>
                <li>1 – Wall transformer base</li>
                <li>1 – 2 prong outlet adaptor</li>
                <li>4 – Wall inserts</li>
                <li>4 – Mounting screws</li>
            </ul>
        </div>
        <div id="install" class="item">
        	<div>
        		<a id="installation"></a> 
        	    <h1>Installation</h1> 
            </div>
            <div class="subitem">
    	        <a id="mounting"></a> 
	            <h2>Mounting</h2> 
                <ul>
                	<li>Using the supplied mounting template determine a secure mounting position at least 3 feet above the floor and within 2.5 feet of the GE Magnet Monitor.</li>
					<li>The mounting location should not be in the direct path of any air conditioning vent, as the effect of the vent will throw off the temperature and humidity readings.</li>
					<li>Mark the four mounting holes located on the mounting template in the predetermined mounting location.</li>
					<li>Using the proper type drill bit for drilling into the mounting material drill a ¼ inch hole at least 1 ¼ inches deep into the mounting material.</li>
					<li>Insert the four plastic wall inserts into the four previously drilled holes until the insert is no longer protruding from the wall.</li>
					<li>Using two of the supplied mounting screws insert them into the bottom two inserts and screw them three quarters of the way down.</li>
					<li>Have a second person position the Oi Vision with the bottom of the Oi Vision facing down over the two previously inserted screws then slide the Oi Vision down onto the screws making sure the two screws slide into the bottom mounting slots in the Oi Vision.</li>
					<li>Making sure the top mounting slots align with the top mount holes insert the last two supplied mounting screws and screw them in fully.</li>
					<li>Finally tighten the bottom two mounting screws</li>
				</ul>
            </div>
            <div class="subitem">
            	<a id="wall"></a> 
        	    <h2>Wall Transformer Assembly</h2> 
                <img src="images/plug1.JPG" width="170" height="113" class="indent">
                <img src="images/plug2.JPG" width="170" height="113">
                <img src="images/plug3.JPG" width="170" height="113">
				<ul>
                	<li>Remove the wall transformer base from its packaging.</li>
					<li>Remove the clear plastic protective cover from the wall transformer base.</li>
					<li>Remove the two prong outlet adaptor from its packaging.</li>
					<li>Slide the two prong outlet adaptor into the two slides in the wall transformer base making sure not to force the two prong outlet adaptor.</li>
					<li>Press firmly on the two prong outlet adaptor to snap it into place in the wall transformer base.</li>
                </ul>
            </div>
            <div class="subitem">
	            <a id="cable"></a> 
    	        <h2>Cabling</h2> 
                <ul>
                	<li>Disconnect the cryogen cable from the GE Magnet Monitor and reconnect it to the Oi Vision J7.</li>
					<li>Connect one of the supplied 3 foot long 25 pin cables between J6 on the Oi Vision and the cryogen connection on the GE Magnet Monitor.</li>
					<li>Disconnect the scan room cable from the GE Magnet Monitor and reconnect it to the Oi Vision J8.</li>
					<li>Connect one of the supplied 3 foot long 25 pin cables between J9 on the Oi Vision and the scan room connection on the GE Magnet Monitor.</li>
					<li>Disconnect the compressor cable from the GE Magnet Monitor and reconnect it to the Oi Vision J5.</li>
					<li>Connect the supplied 3 foot long 15 pin cables between J4 on the Oi Vision and the compressor connection on the GE Magnet Monitor.</li>
					<li>Connect the customer supplied CAT5 Ethernet cable between J3 on the Oi Vision and the customer supplied CAT5 network jack.</li>
					<li>Plug the wall transformer into the outlet provided by the customer</li>
					<li>Plug the wall transformer’s small round plug into J1 on the Oi Vision.</li>
                </ul>
            </div>
        </div>
        <div id="supportdiv" class="item"> 
        <a id="support"></a> 
        <h1>Support</h1>
        <div id="supporttable" class="subitem">
        <a id="troubleshooting"></a>
        <h2>Troubleshooting</h2>
           <table border="1" cellspacing="0" cellpadding="0" class="indent">
               <tr>
                   <td width="213" valign="top"><p align="left">Symptom</p></td>
                   <td width="213" valign="top"><p align="left">Problem</p></td>
                   <td width="213" valign="top"><p align="left">Solution</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Green light is on solid</p></td>
                   <td width="213" valign="top"><p align="left">Normal condition</p></td>
                   <td width="213" valign="top"><p align="left">None</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Blue light is flashing once every 3 seconds</p></td>
                   <td width="213" valign="top"><p align="left">Normal condition</p></td>
                   <td width="213" valign="top"><p align="left">None</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Display is blank</p></td>
                   <td width="213" valign="top"><p align="left">No power to the Oi Vision.</p></td>
                   <td width="213" valign="top"><p align="left">Check that the wall    transformer is plugged in to the wall outlet.<br>
                       Check that the power    cord is plugged into J1.<br>
                       Check that there is    power at the wall outlet.<br>
                       If the green light is on    then call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Green light is off</p></td>
                   <td width="213" valign="top"><p align="left">No power to the Oi Vision.</p></td>
                   <td width="213" valign="top"><p align="left">Check that the wall    transformer is plugged in to the wall outlet.<br>
                       Check that the power    cord is plugged into J1.<br>
                       Check that there is    power at the wall outlet.<br>
                       If the display is on    then call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Blue light is on solid</p></td>
                   <td width="213" valign="top"><p align="left">Abnormal Oi Vision    condition.</p></td>
                   <td width="213" valign="top"><p align="left">Call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Display reads &lsquo;Config Error&rsquo;</p></td>
                   <td width="213" valign="top"><p align="left">Abnormal Oi Vision    condition.</p></td>
                   <td width="213" valign="top"><p align="left">Call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Display reads &lsquo;HELLO not valid&rsquo;</p></td>
                   <td width="213" valign="top"><p align="left">Oi Vision has been    disabled.</p></td>
                   <td width="213" valign="top"><p align="left">Call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Display reads &lsquo;HELLO error&rsquo;</p></td>
                   <td width="213" valign="top"><p align="left">Oi Vision has been    disabled.</p></td>
                   <td width="213" valign="top"><p align="left">Call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">&nbsp;</p></td>
                   <td width="213" valign="top"><p align="left">&nbsp;</p></td>
                   <td width="213" valign="top"><p align="left">&nbsp;</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Display reads &lsquo;NTP Error&rsquo;</p></td>
                   <td width="213" valign="top"><p align="left">No Connection to    oi-vision.com</p></td>
                   <td width="213" valign="top"><p align="left">Unplug J1 for 5 minutes    then plug back in.<br>
                       If the problem persists    call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Display reads &lsquo;Connect Error&rsquo;</p></td>
                   <td width="213" valign="top"><p align="left">Lost internet    connection.</p></td>
                   <td width="213" valign="top"><p align="left">Unplug J1 for 5 minutes    then plug back in.<br>
                       If the problem persists    call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Display reads &lsquo;SHTX Error&rsquo;</p></td>
                   <td width="213" valign="top"><p align="left">Abnormal Oi Vision    condition.</p></td>
                   <td width="213" valign="top"><p align="left">Call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Display reads &lsquo;MAC Check Failed&rsquo;</p></td>
                   <td width="213" valign="top"><p align="left">Abnormal Oi Vision    condition.</p></td>
                   <td width="213" valign="top"><p align="left">Call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">&nbsp;</p></td>
                   <td width="213" valign="top"><p align="left">&nbsp;</p></td>
                   <td width="213" valign="top"><p align="left">&nbsp;</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">Noise from Oi Vision</p></td>
                   <td width="213" valign="top"><p align="left">Abnormal Oi Vision    condition.</p></td>
                   <td width="213" valign="top"><p align="left">Call Service</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">GE Magnet Monitor he level incorrect</p></td>
                   <td width="213" valign="top"><p align="left">Abnormal Oi Vision    condition.</p></td>
                   <td width="213" valign="top"><p align="left">Unplug J1 for 5 minutes    then plug back in.<br>
                       If the problem persists    call service.</p></td>
               </tr>
               <tr>
                   <td width="213" valign="top"><p align="left">GE Magnet Monitor he pressure incorrect</p></td>
                   <td width="213" valign="top"><p align="left">Abnormal Oi Vision    condition.</p></td>
                   <td width="213" valign="top"><p align="left">Unplug J1 for 5 minutes    then plug back in.<br>
                       If the problem persists    call service.</p></td>
               </tr>
           </table>
           </div>
           <div id="contactdiv" class="subitem">
           <a id="contact"></a>
           <h2>Support Contact</h2>
           For support please contact OiService at 888.673.5151 or use the form below.<br><br>

           <div id="contactemail">
           		<?php
				if(!empty($errors)){
				echo "<p class='err'>".nl2br($errors)."</p>";
				}
				?>
				<div id='contact_form_errorloc' class='err'></div>
				<form method="POST" name="contact_form" 
				action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"> 
				<p>
				<label for='name'>Name: </label><br>
				<input type="text" name="name" value='<?php echo htmlentities($name) ?>'>
				</p>
				<p>
				<label for='email'>Email: </label><br>
				<input type="text" name="email" value='<?php echo htmlentities($visitor_email) ?>'>
				</p>
				<p>
				<label for='message'>Message:</label> <br>
				<textarea name="message" rows=8 cols=30><?php echo htmlentities($user_message) ?></textarea>
				</p>
				<p>
				<img src="captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg' ><br>
				<label for='message'>Enter the code above here :</label><br>
				<input id="6_letters_code" name="6_letters_code" type="text"><br>
				<small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
				</p>
				<input type="submit" value="Submit" name='submit'>
				</form>
				<script language="JavaScript">
				// Code for validating the form
				// Visit http://www.javascript-coder.com/html-form/javascript-form-validation.phtml
				// for details
				var frmvalidator  = new Validator("contact_form");
				//remove the following two lines if you like error message box popups
				//frmvalidator.EnableOnPageErrorDisplaySingleBox();
				frmvalidator.EnableMsgsTogether();
				
				frmvalidator.addValidation("name","req","Please provide your name"); 
				frmvalidator.addValidation("email","req","Please provide your email"); 
				frmvalidator.addValidation("email","email","Please enter a valid email address"); 
				</script>
				<script language='JavaScript' type='text/javascript'>
				function refreshCaptcha()
				{
					var img = document.images['captchaimg'];
					img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
				}
				</script>
           </div>     
           </div>
        </div>
       <!-- Type your body here--> 
    </div>
    <br>
    <div id="footer"></div>
    <div id="date">last updated 8-29-12</div>
</div>
</body>
</html>
