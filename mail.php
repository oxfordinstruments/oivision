<?php
 set_include_path('/home/oivisi5/php/');
/**
 * Minimal test
 * 
 * @package PHP_Debug
 * @filesource
 * 
 * @version    CVS: $Id: PHP_Debug_test.php,v 1.1 2008/05/02 14:26:37 c0il Exp $
 */ 
 

include 'Mail.php';
include 'Mail/mime.php' ;

$text = 'Text version of email';
$html = '<html><body>HTML version of email</body></html>';
//$file = '/home/richard/example.php';
$crlf = "\n";
$hdrs = array(
              'From'    => 'alarms@oi-vision.com',
              'Subject' => 'Test mime message'
              );

$mime = new Mail_mime(array('eol' => $crlf));

$mime->setTXTBody($text);
$mime->setHTMLBody($html);
//$mime->addAttachment($file, 'text/plain');

$body = $mime->get();
$hdrs = $mime->headers($hdrs);

$mail =& Mail::factory('mail');
$mail->send('evotodi@gmail.com', $hdrs, $body);

 
?>
