<?php
if (isset($_COOKIE["mm1"]))
  {
  echo "";
  }
else
  {
  header("location:index.php");
  }
  
ob_start();

require("mysqlInfo.php");
$tbl_name="users"; // Table name
// Connect to server and select databse.
mysql_connect("$host", "$username", "$password");//or die("cannot connect");
mysql_select_db("$db_name");//or die("cannot select DB");

$sql="SELECT COUNT(*) FROM users;";
$result=mysql_query($sql);
$usercount=mysql_result($result, 0);

$sql="SELECT COUNT(*) FROM sites;";
$result=mysql_query($sql);
$sitecount=mysql_result($result, 0);

$sql="SELECT COUNT(*) FROM customers ";
$result=mysql_query($sql);
$custcount=mysql_result($result, 0);

$sql="SELECT COUNT(*) FROM `users` WHERE `active`='Y';";
$result=mysql_query($sql);
$useractivecount=mysql_result($result, 0);

$sql="SELECT COUNT(*) FROM `sites` WHERE `enabled`='Y';";
$result=mysql_query($sql);
$siteactivecount=mysql_result($result, 0);

$sql="SELECT COUNT(*) FROM `banlist`;";
$result=mysql_query($sql);
$bancount=mysql_result($result, 0);


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<link href="css/scrollableFixedHeaderTable.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.pack.js"></script>
<script type="text/javascript" src="js/jquery.dimensions.min.js"></script>
<script type="text/javascript" src="js/jquery.scrollableFixedHeaderTable.js"></script>
<script>
	$(document).ready(function(){
		$('#_myTable1').scrollableFixedHeaderTable(800,200);
		$('#_myTable2').scrollableFixedHeaderTable(800,200);
		$('#_myTable3').scrollableFixedHeaderTable(800,200);
		$('#_myTable4').scrollableFixedHeaderTable(800,200);
	});
</script>
<script type="text/javascript">
	function bancheck(t, v){
		if(v.toLowerCase() === "root" || v == "69.27.61.60"){
			alert("Root cannot be banned");
			return;	
		}
		if(confirm("Are you sure you want to ban the " + t + ": " + v + " ?")){
			switch(t)
			{
			case "IP":
			  window.location.assign("ban.php?ip=" + v + "&buser=<?php echo $_COOKIE['mm1']; ?>");
			  break;
			case "User":
			  window.location.assign("ban.php?user=" + v + "&buser=<?php echo $_COOKIE['mm1']; ?>");
			  break;
            case "Domain":
			  window.location.assign("ban.php?domain=" + v + "&buser=<?php echo $_COOKIE['mm1']; ?>");
			  break;
			default:
			  window.location.assign("error.php?e=bad type");
			}	
		}
	}
	
	function unbancheck(t, v){
		if(confirm("Are you sure you want to remove the ban for " + t + ": " + v + " ?")){			
			  window.location.assign("ban.php?unban=" + v);	
		}
	}
</script>
</head>
<body>
<style>
  .div {
		font-family: Arial, Helvetica, sans-serif;
		border: 1px solid #CCCCCC;
		width: 802px;
  }
  
	.myTable {
		background-color: BLACK;
		font-size: 12px;
	}

	.myTable td {
		background-color: WHITE;
	}

  .myTable .header td {
    font-weight: bold;
    background-color: #CCCCCC;
  }

</style>
<div id="center-x">
    
<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div>
    <br>
    
    <div class="bodytext" style="margin:15px;margin-top:5px;">
      <div id="main-box" style="padding-left:30px;"></div>
     
        <div id="main-box-inner-left">
        <h4>Statistics</h4>
        <table  border="1" class="bodytext">
      <tr>
        <td width="168">Total Users</td>
        <td width="224"><?php echo $usercount; ?></td>
      </tr>
      <tr>
        <td>Active Users</td>
        <td><?php echo $useractivecount; ?></td>
      </tr>
      <tr>
        <td>Inactive Users</td>
        <td><?php echo $usercount - $useractivecount; ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Total Sites</td>
        <td><?php echo $sitecount; ?></td>
      </tr>
      <tr>
        <td>Enabled Sites</td>
        <td><?php echo $siteactivecount; ?></td>
      </tr>
      <tr>
        <td>Disabled Sites</td>
        <td><?php echo $sitecount - $siteactivecount; ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Total Customers</td>
        <td><?php echo $custcount; ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Total Banned</td>
        <td><?php echo $bancount; ?></td>
      </tr>
      
    </table>
    </div>
    <div id="main-box-inner-right">
    <h4>Other</h4>
    	<table width="25%" border="1" style="text-align:center">
            <tr>
                <td width="50%">Links</td>
            </tr>
            <tr>
                <td><a href="monitor/mm_insert.log.txt" target="_new">Monitor Log</a></td>
            </tr>
            <tr>
                <td><a href="outputsqltable.php?table=users" target="_new">Users Table</a></td>
            </tr>
            <tr>
                <td><a href="outputsqltable.php?table=customers" target="_new">Customers Table</a></td>
            </tr>
            <tr>
                <td><a href="outputsqltable.php?table=sites" target="_new">Sites Table</a></td>
            </tr>
            <tr>
                <td><a href="outputsqltable.php?table=banlist" target="_new">Ban List</a></td>
            </tr>
            <tr>
                <td><a href="outputsqltable.php?table=alarms" target="_new">Alarms</a></td>
            </tr>
		</table>

    </div>
    <div id="logins" style="width:600px; margin-bottom:15px; clear:both">
        <?php
        mysql_connect("$host", "$username", "$password");//or die("cannot connect");
        mysql_select_db("$db_name");//or die("cannot select DB");
        $sql="SELECT * FROM `logins` ORDER BY `id` DESC LIMIT 50;";
        $result=mysql_query($sql);
        //$sitelogins=mysql_fetch_array($result);
        ?>
    	<h4>Last 50 user logins</h4>
    	<div class="div">
          <table class="myTable scrollableFixedHeaderTable" width="780" id="_myTable1" cellspacing="1">
            <tr class="header">
              <td width="81">UID</td>
              <td width="39">Failed</td>
              <td width="85">IP</td>
              <td width="302">Domain</td>
              <td width="200">Date</td>
              <td width="55">Type</td>
            </tr>
                    <?php
                        while($sitelogins=mysql_fetch_array($result))
                        {
                            echo "<tr>\n";
                            echo "<td><a onClick=\"bancheck('User', '".$sitelogins['uid']."')\">".$sitelogins['uid']."</a></td>\n";
                            echo "<td>".$sitelogins['failed']."</td>\n";
                            echo "<td><a onClick=\"bancheck('IP', '".$sitelogins['ip']."')\">".$sitelogins['ip']."</a></td>\n";
                            echo "<td><a onClick=\"bancheck('Domain', '".$sitelogins['domain']."')\">".$sitelogins['domain']."</a></td>\n";
                            echo "<td>".date('Y-m-d H:i:s',tz_correct($sitelogins['timestamp']))."</td>\n";
							echo "<td>".$sitelogins['type']."</td>\n";
                            echo "</tr>\n";
                        }  
                    ?>   
                          
          </table>
        </div>
    </div>
    
        <div id="logins" style="width:600px; margin-bottom:15px">
        <?php
        mysql_connect("$host", "$username", "$password");//or die("cannot connect");
        mysql_select_db("$db_name");//or die("cannot select DB");
        $sql="SELECT * FROM `logins` WHERE lower(`failed`) = 'yes' ORDER BY `id` DESC LIMIT 200;";
        $result=mysql_query($sql);
        //$sitelogins=mysql_fetch_array($result);
        ?>
    	<h4>Log of failed login attempts</h4>
        <div class="div">
          <table class="myTable scrollableFixedHeaderTable" width="780" id="_myTable2" cellspacing="1">
            <tr class="header">
              <td width="100">UID</td>
              <td width="39">Failed</td>
              <td width="85">IP</td>
              <td width="302">Domain</td>
              <td width="200">Date</td>
              <td width="45">Type</td>
            </tr>
                    <?php
                        while($sitelogins=mysql_fetch_array($result))
                        {
                            echo "<tr>\n";
                            echo "<td><a onClick=\"bancheck('User', '".$sitelogins['uid']."')\">".$sitelogins['uid']."</a></td>\n";
                            echo "<td>".$sitelogins['failed']."</td>\n";
                            echo "<td><a onClick=\"bancheck('IP', '".$sitelogins['ip']."')\">".$sitelogins['ip']."</a></td>\n";
                            echo "<td><a onClick=\"bancheck('Domain', '".$sitelogins['domain']."')\">".$sitelogins['domain']."</a></td>\n";
                            echo "<td>".date('Y-m-d H:i:s',tz_correct($sitelogins['timestamp']))."</td>\n";
							echo "<td>".$sitelogins['type']."</td>\n";
                            echo "</tr>\n";
                        }  
                    ?>   
                         
          </table>
        </div>
    </div>
    
    <div id="bans" style="width:600px; margin-bottom:15px">
        <?php
        mysql_connect("$host", "$username", "$password");//or die("cannot connect");
        mysql_select_db("$db_name");//or die("cannot select DB");
        $sql="SELECT * FROM `banlist` ORDER BY `index` DESC;";
        $result=mysql_query($sql);
        //$sitelogins=mysql_fetch_array($result);
        ?>
    	<h4>List of banned User/IPs/Domains</h4>
        <div class="div">
          <table class="myTable scrollableFixedHeaderTable" width="780" id="_myTable3" cellspacing="1">
            <tr class="header">
              <td width="100">Ban Type</td>
              <td width="250">Value</td>
              <td width="100">Banned Date</td>
            </tr>
                    <?php
                        while($sitelogins=mysql_fetch_array($result))
                        {
							if($sitelogins['ip'] != ""){
								$type = "&nbsp;&nbsp;IP";
							}
							if($sitelogins['user'] != ""){
								$type = "&nbsp;&nbsp;User";
							}
							if($sitelogins['domain'] != ""){
								$type = "&nbsp;&nbsp;Domain";
							}
							
                            echo "<tr>\n";
                            echo "<td>".$type."</td>\n";
                            echo "<td><a onClick=\"unbancheck('".$type."', '".$sitelogins['ban']."')\">".$sitelogins['ban']."</a></td>\n";
                            echo "<td>".date('Y-m-d H:i:s',tz_correct($sitelogins['date']))."</td>\n";
                            echo "</tr>\n";
                        }  
                    ?>   
                          
          </table>
        </div>
    </div>
    
    <div id="hellos" style="width:600px; margin-bottom:15px">
        <?php
        mysql_connect("$host", "$username", "$password");//or die("cannot connect");
        mysql_select_db("$db_name");//or die("cannot select DB");
        $sql="SELECT * FROM `hellos` ORDER BY `index` DESC LIMIT 200;";
        $result=mysql_query($sql);
        //$sitelogins=mysql_fetch_array($result);
        ?>
    	<h4>Log of Oi Vision Monitor "hellos"</h4>
        <div class="div">
          <table class="myTable scrollableFixedHeaderTable" width="780" id="_myTable4" cellspacing="1">
            <tr class="header">
              <td width="200">MAC</td>
              <td width="268">Name</td>
              <td width="156">Version</td>
              <td width="156">Date</td>
            </tr>
                    <?php
                        while($sitelogins=mysql_fetch_array($result))
                        {
                            echo "<tr>\n";
                            echo "<td>".$sitelogins['mac']."</td>\n";
							echo "<td>".$sitelogins['site_name']."</td>\n";
                            echo "<td>".$sitelogins['ver']."</td>\n";
                            echo "<td>".date('Y-m-d H:i:s',tz_correct($sitelogins['date']))."</td>\n";
                            echo "</tr>\n";
                        }  
                    ?>   
                          
          </table>
        </div>
    </div>
</div>
<br>


<div id="footer"></div>
</div>
</body>
</html>
<?php
function tz_correct($indate) {
date_default_timezone_set('America/New_York');
$offset = date("Z");
$timestamp = strtotime($indate . " " . $offset . " seconds");
return($timestamp);	
}
?>