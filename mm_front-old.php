<?php
if (isset($_COOKIE["mm1"]))
    echo "";
else
    header("location:index.php");
ob_start();
$settings            = new SimpleXMLElement('settings.xml', null, true);
$mysql_tz_offset     = $settings->mysql_tz_offset;
$gauge_hour          = $settings->gauge_hour;
$alarmsites          = array();
$nodatasites         = array();
$sitevals["he"]      = 0;
$sitevals["heb"]     = 0;
$sitevals["hep"]     = 0;
$sitevals["wf"]      = 0;
$sitevals["wtf"]     = 0;
$sitevals["tf"]      = 0;
$sitevals["rh"]      = 0;
$sitevals["fieldon"] = 0;
$sitevals["compon"]  = 0;

//$HeLvlThreshhold = $settings->HeLevelThreshhold;
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password"); //or die("cannot connect");
mysql_select_db("$db_name"); //or die("cannot select DB");

$myusername = $_COOKIE["mm1"];
$sql        = "SELECT * FROM users WHERE uid='$myusername'";
$resultUser = mysql_query($sql);
$rowUser    = mysql_fetch_array($resultUser);

if ($rowUser['customer_id'] != 0) {
    $sql = "SELECT * FROM `sites` WHERE customer_id='" . $rowUser['customer_id'] . "'";
} else {
    $sql = "SELECT * FROM `sites`";
}
$resultSites = mysql_query($sql);


$sql             = "SELECT * FROM `customers` WHERE customer_id='" . $rowUser['customer_id'] . "' LIMIT 1";
$resultCustomers = mysql_query($sql);
$rowCust         = mysql_fetch_array($resultCustomers);
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta http-equiv="expires" content="0" />
<title>Oxford Instruments: Oi Vision</title>
<meta name="keywords" content="nanotechnology, xrf analyzers, micro-analysis systems, superconducting wires, nmr magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, x-ray fluorescence, eds micro-analysis, oxford instruments, oivision, oi vision" />
<meta name="description" content="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research." />
<meta name="author" content="Justin Davis" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<link rel="icon" type="image/png" href="/images/oiicon.png" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/meter.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<script type="text/javascript" src="js/flot/jquery.min.js"></script>
<!--<script type="text/javascript" src="js/swapsies.js"></script>-->

<script type="text/javascript">
		$(function() {
			$(".meter > span").each(function() {
				$(this)
					.data("origWidth", $(this).width())
					.width(0)
					.animate({
						width: $(this).data("origWidth")
					}, 1200);
			});
		});
	
</script>
<script type="text/javascript">
function goToByScroll(id){
	if(document.getElementById(id).style.display == "none"){
		id = id.replace("y","x");
		$('html,body').animate({scrollTop: $("#"+id).offset().top - 25},'slow');
	}else{
		$('html,body').animate({scrollTop: $("#"+id).offset().top - 25},'slow');
	}
	
	//alert(id);
	//$('#center-x').animate({scrollTop: $("#"+id).offset().top},'slow');

}

function shownoreportsite(id, msgid){
	document.getElementById(id).style.display = "";
	document.getElementById(msgid).style.display = "none";	
}
function settings(){
	
}
</script>
</head>
<body onLoad="settings()">
<div id="center-x">
  <div id="header"></div>
  <div id="menu"><?php include("menu/menu.php");?></div>
  <br />
  <?php
  
mysql_connect("$host", "$username", "$password"); //or die("cannot connect");
mysql_select_db("$db_name"); //or die("cannot select DB");
if ($rowUser['customer_id'] != 0) {
    $sql = "SELECT * FROM `sites` WHERE customer_id='" . $rowUser['customer_id'] . "' ORDER BY site_name, site_id ASC";
} else {
    $sql = "SELECT * FROM `sites` ORDER BY site_name, site_id ASC";
}
$resultSites = mysql_query($sql);

?>
  <div class="bodytext" style="margin:15px;margin-top:5px;">
    <div id="main-box" style="padding-left:30px;"></div>
    <div id="toptoptop">
      <h6 align="center">
        <?php
if (mysql_num_rows($resultSites) == 1) {
    echo "Site Overview For " . $rowCust['customer_name'];
} else {
    echo "Sites Overview For " . $rowCust['customer_name'];
}
?>
      </h6>
      <br />
    </div>
    <div id="bottom_div">
      <h1 align="center">Site List</h1>
      <div title="line" class="line"></div>
      <p><br />
          <?php
while ($rowSites = mysql_fetch_array($resultSites)) {
	
	if(strtolower($rowSites['use_global_alarm']) == "y"){
		$settings = new SimpleXMLElement('settings.xml', null, true);
	}else{
		$settings = new SimpleXMLElement("<settings></settings>");	
		$settings->HeLevelThreshhold = $rowSites['alarm_he'];
		$settings->HeLevelCriticalThreshhold = $rowSites['alarm_hec'];
		$settings->HeBurnThreshhold = $rowSites['alarm_heb'];
		$settings->HePressHighCriticalThreshhold = $rowSites['alarm_vphc'];
		$settings->HePressHighThreshhold = $rowSites['alarm_vph'];
		$settings->HePressLowThreshhold = $rowSites['alarm_vpl'];
		$settings->HePressLowCriticalThreshhold = $rowSites['alarm_vplc'];
		$settings->WaterFlowHighCriticalThreshhold = $rowSites['alarm_wfhc'];
		$settings->WaterFlowHighThreshhold = $rowSites['alarm_wfh'];
		$settings->WaterFlowLowThreshhold = $rowSites['alarm_wfl'];
		$settings->WaterFlowLowCriticalThreshhold = $rowSites['alarm_wflc'];
		$settings->WaterTempHighCriticalThreshhold = $rowSites['alarm_wthc'];
		$settings->WaterTempHighThreshhold = $rowSites['alarm_wth'];
		$settings->WaterTempLowThreshhold = $rowSites['alarm_wtl'];
		$settings->WaterTempLowCriticalThreshhold = $rowSites['alarm_wtlc'];
		$settings->RoomTempHighCriticalThreshhold = $rowSites['alarm_rthc'];
		$settings->RoomTempHighThreshhold = $rowSites['alarm_rth'];
		$settings->RoomTempLowThreshhold = $rowSites['alarm_rtl'];
		$settings->RoomTempLowCriticalThreshhold = $rowSites['alarm_rtlc'];
		$settings->RoomHumHighCriticalThreshhold = $rowSites['alarm_rhhc'];
		$settings->RoomHumHighThreshhold = $rowSites['alarm_rhh'];
		$settings->RoomHumLowThreshhold = $rowSites['alarm_rhl'];
		$settings->RoomHumLowCriticalThreshhold = $rowSites['alarm_rhlc'];
		$settings->StoreHeLevelThreshhold = $rowSites['alarm_she'];
		$settings->StoreHeLevelCriticalThreshhold = $rowSites['alarm_shec'];
		$settings->StoreHePressHighCriticalThreshhold = $rowSites['alarm_svphc'];
		$settings->StoreHePressHighThreshhold = $rowSites['alarm_svph'];
		$settings->StoreHePressLowThreshhold = $rowSites['alarm_svpl'];
		$settings->StoreHePressLowCriticalThreshhold = $rowSites['alarm_svplc'];	
	}
	
    if (strtolower($rowSites['enabled']) == "y") {
        $sql              = "SELECT `date` FROM `" . strtolower($rowSites['mac']) . "` ORDER BY `index` DESC LIMIT 1;";
        $resultSiteUpdate = mysql_query($sql);
        $rowSiteUpdate    = mysql_fetch_array($resultSiteUpdate);
        
		$sql = "select case (select `enabled` from sites where `mac`='" . strtolower($rowSites['mac']) . "') when 'y' then IF((select count(*) as nodata from `" . strtolower($rowSites['mac']) . "` where `date` > (DATE(DATE_SUB(CURDATE(), INTERVAL 4 HOUR)) - interval 12 hour) order by `index` desc) >= 1, 'good', 'bad') Else 'disabled' end as nodata;";
        $resultNoData = mysql_query($sql);
        $rowNoData    = mysql_fetch_array($resultNoData);
		  if ($rowNoData["nodata"] == "bad") {
			  	$notreporting = true;
				$nodatasites[] = array(
					0 => $rowSites['site_name'],
					1 => $rowSites['site_id']
				);
          }else{
				$notreporting = false;  
		  }
        
        
        
        $sql            = "SELECT ROUND((SELECT AVG(he) FROM `" . strtolower($rowSites['mac']) . "` where he < 111.11 and `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval $gauge_hour hour))), 2) AS 'he';";
        $resultSiteData = mysql_query($sql);
        $rowSiteData    = mysql_fetch_array($resultSiteData);
        $sql            = "SELECT ROUND((SELECT(SELECT avg(he) FROM `" . strtolower($rowSites['mac']) . "` where he < 111.11 AND date(`date`) > (DATE(DATE_SUB(CURDATE(), INTERVAL $mysql_tz_offset HOUR)) - interval 30 day) AND date(`date`) < (DATE(DATE_SUB(CURDATE(), INTERVAL $mysql_tz_offset HOUR)) - interval 15 day))-(SELECT avg(he) FROM `" . strtolower($rowSites['mac']) . "` where he < 111.11 AND date(`date`) > (DATE(DATE_SUB(CURDATE(), INTERVAL $mysql_tz_offset HOUR)) - interval 15 day))),2);";
        $resultHeBurn   = mysql_query($sql);
		echo "<div id='y" . $rowSites['site_id'] . "' class='noreport_div' ";
		if($notreporting){echo ">";}else{echo "style='display:none'>";}
		echo "<p>".$rowSites['site_name']." has not reported readings since " . $rowSiteUpdate['date']."</p><br />";
		echo "<p><a onClick=\"shownoreportsite('x" . $rowSites['site_id'] . "', 'y" . $rowSites['site_id'] . "')\">Ckick here to show ".$rowSites['site_name']."</a></p>";
		echo "</div>";
        echo "<div id='x" . $rowSites['site_id'] . "' class='overview_div' ";
		if($notreporting){echo "style='display:none'>";}else{echo ">";}
		echo "<div title='View Site Data' style='width:50%; height:40px; float:left'>
			<h2><a href='viewSite.php?site_id=" . $rowSites['site_id'] . "'>" . $rowSites['site_name'] . " - " . $rowSites['site_id'] . "</a></h2>
		  </div><div style='width:50%; height:40px; float:right'>" . $rowSites['mac'] . " &nbsp;&nbsp; Last Update: " . $rowSiteUpdate['date'] . "</div>
		  <div style='clear:both'>
			<div title='HeCont' style='width:16%; height:20px; float:left;'>Helium Level</div>
			<div title='HeReading' style='width:12%; height:20px; float:left; text-align:center'>" . $rowSiteData['he'] . "%</div>
			<div title='HeAvg' style='width:22%; height:20px; float:right";
        if (mysql_result($resultHeBurn, 0) >= $settings->HeBurnThreshhold) {
            echo "; color:red";
            $sitevals["heb"] = 1;
        }
        echo "'>30 Day Avg Burn = ";
        if (mysql_result($resultHeBurn, 0) == NULL or mysql_result($resultHeBurn, 0) <= 0.0) {
            echo "NA";
            $sitevals["heb"] = 0;
        } else {
            echo mysql_result($resultHeBurn, 0);
        }
        echo "%</div>
			<div title='Helium' style='width:48%; height:20px; float:left; margin-bottom:10px'>
			  <div class='meter";
        if ($rowSites['Storage'] != "Y") {
            $hethresh = $settings->HeLevelThreshhold;
        } else {
            $hethresh = $settings->StoreHeLevelThreshhold;
        }
        if ($rowSiteData['he'] <= $hethresh) {
            echo " red";
            $sitevals["he"] = 1;
        }
        echo "'><span style='width: ";
        if ($rowSiteData['he'] > 100.00) {
            echo "100.00";
        } else {
            echo round($rowSiteData['he']);
        }
        echo "%'></span></div>
			</div>";
        
        
        
        
        
        $sql               = "SELECT ROUND((SELECT AVG(hep) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval $gauge_hour hour))), 2) AS 'hep';";
        $resultSiteData    = mysql_query($sql);
        $rowSiteData       = mysql_fetch_array($resultSiteData);
        $sql               = "SELECT ROUND((SELECT AVG(hep) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval 30 day))), 2) AS 'hepavg';";
        $resultSiteDataAvg = mysql_query($sql);
        echo "
			<div style='clear:both'>
			  <div title='HePCont' style='width:16%; height:20px; float:left;'>Helium Pressure</div>
			  <div title='HePReading' style='width:12%; height:20px; float:left; text-align:center'>" . $rowSiteData['hep'] . " PSIG</div>
			  <div title='HePAvg' style='width:22%; height:20px; float:right'>30 Day Avg = ";
        if (mysql_result($resultSiteDataAvg, 0) != NULL) {
            echo mysql_result($resultSiteDataAvg, 0);
        } else {
            echo "NA ";
        }
        echo " PSIG</div>
			  <div title='Helium Pressure' style='width:48%; height:20px; float:left; margin-bottom:10px'>
				<div class='meter";
        if ($rowSites['Storage'] != "Y") {
            $hephthresh = $settings->HePressHighThreshhold;
            $heplthresh = $settings->HePressLowThreshhold;
        } else {
            $hephthresh = $settings->StoreHePressHighThreshhold;
            $heplthresh = $settings->StoreHePressLowThreshhold;
        }
        if ($rowSiteData['hep'] <= $heplthresh or $rowSiteData['hep'] >= $hephthresh) {
            echo " red";
            $sitevals["hep"] = 1;
        }
        echo "'><span style='width: " . round($rowSiteData['hep']) * 10 . "%'></span></div>
			  </div>";
        
        
        
        
        $sql               = "SELECT ROUND((SELECT AVG(wf) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval $gauge_hour hour))), 2) AS 'wf';";
        $resultSiteData    = mysql_query($sql);
        $rowSiteData       = mysql_fetch_array($resultSiteData);
        $sql               = "SELECT ROUND((SELECT AVG(wf) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval 30 day))), 2) AS 'wfavg';";
        $resultSiteDataAvg = mysql_query($sql);
        echo "			  
			  <div style='clear:both'>
				<div title='wfCont' style='width:16%; height:20px; float:left;'>Water Flow</div>
				<div title='wfReading' style='width:12%; height:20px; float:left; text-align:center'>" . $rowSiteData['wf'] . " GPM</div>
				<div title='wfPAvg' style='width:22%; height:20px; float:right'>30 Day Avg = ";
        if (mysql_result($resultSiteDataAvg, 0) != NULL) {
            echo mysql_result($resultSiteDataAvg, 0);
        } else {
            echo "NA ";
        }
        echo " GPM</div>
				<div title='Water Flow' style='width:48%; height:20px; float:left; margin-bottom:10px'>
				  <div class='meter";
        if ($rowSites['Storage'] != "Y") {
            if ($rowSiteData['wf'] <= $settings->WaterFlowLowThreshhold or $rowSiteData['wf'] >= $settings->WaterFlowHighThreshhold) {
                echo " red";
                $sitevals["wf"] = 1;
            }
        }
        echo "'><span style='width: " . round($rowSiteData['wf']) * 10 . "%'></span></div>
				</div>";
        
        
        
        
        $sql               = "SELECT ROUND((SELECT AVG(wtf) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval $gauge_hour hour))), 2) AS 'wtf';";
        $resultSiteData    = mysql_query($sql);
        $rowSiteData       = mysql_fetch_array($resultSiteData);
        $sql               = "SELECT ROUND((SELECT AVG(wtf) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval 30 day))), 2) AS 'wtfavg';";
        $resultSiteDataAvg = mysql_query($sql);
        echo "		
			  </div>
			  <div style='clear:both'>
				<div title='WtCont' style='width:16%; height:20px; float:left;'>Water Temperature</div>
				<div title='WtReading' style='width:12%; height:20px; float:left; text-align:center'>" . $rowSiteData['wtf'] . " Deg F</div>
				<div title='WtAvg' style='width:22%; height:20px; float:right'>30 Day Avg = ";
        if (mysql_result($resultSiteDataAvg, 0) != NULL) {
            echo mysql_result($resultSiteDataAvg, 0);
        } else {
            echo "NA ";
        }
        echo " F</div>
				<div title='Water Temperature' style='width:48%; height:20px; float:left; margin-bottom:10px'>
				  <div class='meter";
        if ($rowSites['Storage'] != "Y") {
            if ($rowSiteData['wtf'] <= $settings->WaterTempLowThreshhold or $rowSiteData['wtf'] >= $settings->WaterTempHighThreshhold) {
                echo " red";
                $sitevals["wtf"] = 1;
            }
        }
        echo "'><span style='width: " . round($rowSiteData['wtf']) . "%'></span></div>
				</div>";
        
        
        
        
        $sql               = "SELECT ROUND((SELECT AVG(tf) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval $gauge_hour hour))), 2) AS 'tf';";
        $resultSiteData    = mysql_query($sql);
        $rowSiteData       = mysql_fetch_array($resultSiteData);
        $sql               = "SELECT ROUND((SELECT AVG(tf) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval 30 day))), 2) AS 'tfavg';";
        $resultSiteDataAvg = mysql_query($sql);
        echo "		
			  </div>
			  <div style='clear:both'>
				<div title='tfCont' style='width:16%; height:20px; float:left;'>Room Temperature</div>
				<div title='tfReading' style='width:12%; height:20px; float:left; text-align:center'>" . $rowSiteData['tf'] . " Deg F</div>
				<div title='tfAvg' style='width:22%; height:20px; float:right'>30 Day Avg = ";
        if (mysql_result($resultSiteDataAvg, 0) != NULL) {
            echo mysql_result($resultSiteDataAvg, 0);
        } else {
            echo "NA ";
        }
        echo " F</div>
				<div title='Equipment Room Temperature' style='width:48%; height:20px; float:left; margin-bottom:10px'>
				  <div class='meter";
        if ($rowSites['Storage'] != "Y") {
            if ($rowSiteData['tf'] <= $settings->RoomTempLowThreshhold or $rowSiteData['tf'] >= $settings->RoomTempHighThreshhold) {
                echo " red";
                $sitevals["tf"] = 1;
            }
        }
        echo "'><span style='width: " . round($rowSiteData['tf']) . "%'></span></div>
				</div>
			  ";
        
        
        
        
        $sql               = "SELECT ROUND((SELECT AVG(rh) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval $gauge_hour hour))), 2) AS 'rh';";
        $resultSiteData    = mysql_query($sql);
        $rowSiteData       = mysql_fetch_array($resultSiteData);
        $sql               = "SELECT ROUND((SELECT AVG(rh) FROM `" . strtolower($rowSites['mac']) . "` where `insertDate` - interval 4 hour > timestamp(date_sub(date_sub(now(), interval $mysql_tz_offset hour), interval 30 day))), 2) AS 'rhavg';";
        $resultSiteDataAvg = mysql_query($sql);
        echo "		
			  </div>
			  <div style='clear:both'>
				<div title='rhCont' style='width:16%; height:20px; float:left;'>Room Humidity</div>
				<div title='rhReading' style='width:12%; height:20px; float:left; text-align:center'>" . $rowSiteData['rh'] . " %RH</div>
				<div title='rhAvg' style='width:22%; height:20px; float:right'>30 Day Avg = ";
        if (mysql_result($resultSiteDataAvg, 0) != NULL) {
            echo mysql_result($resultSiteDataAvg, 0);
        } else {
            echo "NA ";
        }
        echo " %RH</div>
				<div title='Equipment Room Humidity' style='width:48%; height:20px; float:left; margin-bottom:10px'>
				  <div class='meter";
        if ($rowSites['Storage'] != "Y") {
            if ($rowSiteData['rh'] <= $settings->RoomHumLowThreshhold or $rowSiteData['rh'] >= $settings->RoomHumHighThreshhold) {
                echo " red";
                $sitevals["rh"] = 1;
            }
        }
        echo "'><span style='width: " . round($rowSiteData['rh']) . "%'></span></div>
				</div>
				";
        
        
        
        
        //$sql="SELECT AVG(`FieldOn`) AS fo FROM `" . strtolower($rowSites['mac']) . "` where date(`date`) > (DATE(DATE_SUB(CURDATE(), INTERVAL $mysql_tz_offset HOUR)) - interval 1 day);";
        $sql            = "SELECT `FieldOn` FROM `" . strtolower($rowSites['mac']) . "` ORDER BY `index` DESC LIMIT 1";
        $resultSiteData = mysql_query($sql);
        $rowSiteData    = mysql_fetch_array($resultSiteData);
        echo "		
			  </div>
			  <div style='clear:both'>
				<div title='FieldOnCont' style='width:16%; height:20px; float:left;'>Magnet Field</div>
				<div title='FieldOnReading' style='width:12%; height:20px; float:left; text-align:center'>";
        echo $rowSiteData['FieldOn'];
        /*if($rowSiteData['fo'] < 1 ){echo "0";}else{echo "1";}*/
        echo "</div>
				<div title='FieldOnAvg' style='width:22%; height:20px; float:right'></div>
				<div title='Field On' style='width:48%; height:20px; float:left; margin-bottom:10px'>
				  <div class='meter";
        if ($rowSites['Storage'] != "Y") {
            if ($rowSiteData['FieldOn'] == 0) {
                echo " red";
                $sitevals["fieldon"] = 1;
            }
        }
        /*if($rowSiteData['fo'] < 1 ){echo " red"; $sitevals["fieldon"] = 1;}*/
        echo "'><span style='width: 100%'></span></div>
				</div>
				";
        
        
        
        
        //$sql="SELECT AVG(`CompOn`) AS c FROM `" . strtolower($rowSites['mac']) . "` where date(`date`) > (DATE(DATE_SUB(CURDATE(), INTERVAL $mysql_tz_offset HOUR)) - interval 1 day);";
        $sql            = "SELECT `CompOn` FROM `" . strtolower($rowSites['mac']) . "` ORDER BY `index` DESC LIMIT 1";
        $resultSiteData = mysql_query($sql);
        $rowSiteData    = mysql_fetch_array($resultSiteData);
        echo "		
			  </div>
			  <div style='clear:both'>
				<div title='CCont' style='width:16%; height:20px; float:left;'>Compressor On</div>
				<div title='CReading' style='width:12%; height:20px; float:left; text-align:center'>";
        echo $rowSiteData['CompOn'];
        /*if($rowSiteData['c'] >= 1 ){echo "1";}else{echo "0";}*/
        echo "</div>
				<div title='CAvg' style='width:22%; height:20px; float:right'></div>
				<div title='Compressor On' style='width:48%; height:20px; float:left; margin-bottom:10px'>
				  <div class='meter";
        if ($rowSites['Storage'] != "Y") {
            if ($rowSiteData['CompOn'] == 0) {
                echo " red";
                $sitevals["compon"] = 1;
            }
        }
        /*if($rowSiteData['c'] < 1 ){echo " red"; $sitevals["compon"] = 1;}*/
        echo "'><span style='width: 100%'></span></div>
				</div></div></div></div></div>";
        
        
        
        
        // Add here
        $alarm  = false;
        //print_r(array_count_values($sitevals));
        $arrcnt = array_count_values($sitevals);
        //print_r($sitevals);
        foreach ($sitevals as &$value) {
            if ($value == 1) {
                $alarm = true;
                //echo $value;
            }
            $value = 0;
        }
	 
            if ($alarm == true && $notreporting == false) {
                $alarmsites[] = array(
                    0 => $rowSites['site_name'],
                    1 => $rowSites['site_id']
                );
             }
   
    }
    ;
}
;
?>
      </p>
    </div>
    <?php
if (count($alarmsites) != 0 || count($nodatasites) != 0) {
    if (count($alarmsites) != 0) {
        echo "
		<div id='top_div'>
		  <h1 align='center'> Sites With Alarms </h1>
		  <div title='line' class='line'></div>\n";
        
        foreach ($alarmsites as &$value) {
            echo "<a href='#' onClick=\"goToByScroll('x" . $value[1] . "')\">" . $value[0] . "</a>";
            //echo "<a href='#" . $value[1] . "'>" . $value[0]. "</a>";
            echo "<br />\n";
        }
    } else {
        echo "
		<div id='top_div'>";
    }
    if (count($nodatasites) != 0) {
        echo "
      <br />
      <h1 align='center'> Sites Not Reporting </h1>
      <div title='line' class='line'></div>\n";
        
        foreach ($nodatasites as &$value) {
            echo "<a href='#' onClick=\"goToByScroll('y" . $value[1] . "')\">" . $value[0] . "</a>";
            //echo "<a href='#" . $value[1] . "'>" . $value[0]. "</a>";
            echo "<br />\n";
        }
    }
    echo "
      <br />
    </div>
    <script type='text/javascript'>   
		$('#top_div').removeAttr('style');
		$('#bottom_div').removeAttr('style');
		var tmp = $('#top_div').html();
		$('#top_div').empty().append($('#bottom_div').html());
		$('#bottom_div').empty().append(tmp); 
    </script>";
}
?> 
  </div>
  <div id="footer"></div>
  Site Updated: 9-17-12 1530
</div>
</body>
</html>
