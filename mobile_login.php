<?php

 //  require_once('mobile_device_detect.php');
//   mobile_device_detect(true,true,true,true,true,true,true,'../mobile/index.php',false);

//This section redirect to ssl page if not already

//if ($_SERVER['HTTPS'] != "on") {
//    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//    header("Location: http://www.google.com");
//    exit;
//} 
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="INDEX">
<link rel="icon" type="image/png" href="/images/OIIcon.png" />

<link rel="stylesheet" type="text/css" media="screen" href="css/main_mobile.css" />
<script type='text/javascript' src='js/jsmd5.js'></script>
<script type='text/javascript' src='js/php.default.min.js'></script>
<script type="text/javascript">
function highlight(){
	document.getElementById("submitbtn").focus();
}
function submitcheck(){
	$key = "platinum";
	document.getElementById("uname").value = base64_encode(document.getElementById("myusername").value);
	document.getElementById("pword").value = base64_encode(document.getElementById("mypassword").value);	
	//document.getElementById("mypassword").value = "";
	//document.getElementById("myusername").value = "";
	//alert(document.getElementById("uname").value);
	document.forms["form"].submit();
}

</script>

</head>
<body onLoad="highlight()">
<div id="center-x">

<div id="header"></div>
<br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
<div id="login">
  <table id="loginTableOuter" width="600" border="0"  cellpadding="0" cellspacing="1" >
<tr>

<form id="form" name="form" method="post" action="<?php if (isset($_GET['download'])){echo "checklogin.php?download";}else{echo "checklogin.php";} ?>">
<td>
<table id="loginTableInner" width="100%" border="0" cellpadding="3" cellspacing="1" >
<tr>
<td colspan="3"><div align="center">
  <h2>Oi Vision Monitor Login </h2>
</div></td>
</tr>
<tr>
<td >Username</td>
<td >:</td>
<td ><input name="myusername"  type="text" class="textField"  id="myusername"></td>
</tr>
<tr>
<td>Password</td>
<td>:</td>
<td><input name="mypassword" type="password" class="textField"  id="mypassword"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><input name="Submit" id="submitbtn" type="button" value="Login" class="button" onclick="submitcheck()" /></td>
</tr>
</table></td>
<input type="hidden" id="uname" name="uname" value="none" />
<input type="hidden" id="pword" name="pword" value="none" />
</form>
</tr>
</table>
</div>
</div>
<br>
<div id="footer"></div>

</div>

</body>
</html>
