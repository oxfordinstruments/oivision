<?php
/*
Check select tables sizes and truncates if needed.

*/

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

echo "Clean Tables Begin",EOL,EOL;

$settings = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'].'/settings.xml', null, true);

ob_start();//Start output buffering

require($_SERVER['DOCUMENT_ROOT'].'/mysqlInfo.php');

$mysqli = new mysqli("$host", "$username", "$password", "$db_name");

if ($mysqli->connect_errno) {
    header("location:/error.php?e=".$mysqli->connect_error."&p=session_ctrl_mysqli.php.php");
}

$sql="SELECT TABLE_NAME AS 'tables',
	round(((data_length + index_length) / 1024 / 1024), 2) AS size
	FROM information_schema.TABLES 
	WHERE table_schema = 'oivisi5_mm'";
if(!$result = $mysqli->query($sql)){
	echo $mysqli->error;
	exit;	
}

echo "Getting table sizes in MB...",EOL;
$tables = array();
while($row = $result->fetch_assoc()){
	$tables[$row['tables']] = $row['size'];	
	echo "  ".$row['tables']." - ".$row['size'],EOL;
}

//TRUNCATE `hellos`;
echo EOL,"Checking HELLOS table...",EOL;
if(floatval($tables['hellos']) >= floatval($settings->hellos_table)){
	echo "Truncating hellos",EOL;
	$sql="TRUNCATE `hellos`;";
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;	
	}
}else{
	echo "Table is good",EOL;	
}

echo EOL,"Checking ALARMS table...",EOL;
if(floatval($tables['alarms']) >= floatval($settings->alarms_table)){
	echo "Truncating alarms",EOL;
	$sql="TRUNCATE `alarms`;";	
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;	
	}
}else{
	echo "Table is good",EOL;	
}

echo EOL,"Checking LOGINS table...",EOL;
if(floatval($tables['logins']) >= floatval($settings->logins_table)){
	echo "Truncating logins",EOL;
	$sql="TRUNCATE `logins`;";	
	if(!$result = $mysqli->query($sql)){
		echo $mysqli->error;	
	}
}else{
	echo "Table is good",EOL;	
}

echo EOL,"Done.",EOL;

?>