<?php
if (isset($_COOKIE["mm1"])){
	if (isset($_COOKIE["mm3"]) == false){
		header("location:error.php?e='You do not have permission to access this page!'");
	}
}else{
	header("location:index.php");
}
	
if (isset($_GET['site_id'])){
		$site_id = $_GET['site_id'];
	}else{
		header("location:error.php?e='Invalid site id'");
	}
if (isset($_GET['m'])){$mgt="true";}else{$mgt="false";}
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password"); //or die("cannot connect");
mysql_select_db("$db_name"); //or die("cannot select DB");

$sql        = "SELECT * FROM sites WHERE site_id='$site_id' LIMIT 1";
$resultSite = mysql_query($sql);
$rowSite    = mysql_fetch_array($resultSite);


if (strtolower($rowSite['use_global_alarm']) == "y")
{
	$use_global = "checked";	
}
else
{
	$use_global = "";
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />

<script src="js/smartspin/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="js/smartspin/smartspinner.js" type="text/javascript"></script>
<link href="js/smartspin/smartspinner.css" type="text/css" rel="stylesheet"/>

<script type="text/javascript">
<?php
ob_start();
$settings = new SimpleXMLElement('settings.xml', null, true);
 
 echo "
        $(document).ready(function() {
            $('#he').spinit({min:0,max:100,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_he']." }); //he
			$('#hec').spinit({min:0,max:100,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_hec']." }); //he critical
			$('#heb').spinit({min:0,max:100,stepInc:0.01,pageInc:0.1, height: 22, width: 140, initValue: ".$rowSite['alarm_heb']." }); //heburn
			$('#hephc').spinit({min:0,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$rowSite['alarm_vphc']." }); //hep h critical
			$('#heph').spinit({min:0,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$rowSite['alarm_vph']." }); //hep h
			$('#hepl').spinit({min:0,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$rowSite['alarm_vpl']." }); //hep l
			$('#heplc').spinit({min:0,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$rowSite['alarm_vplc']." }); //hep l critical
			$('#wfhc').spinit({min:0,max:100,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_wfhc']." }); //wf h Critical
			$('#wfh').spinit({min:0,max:100,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_wfh']." }); //wf h
			$('#wfl').spinit({min:0,max:100,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_wfl']." }); //wf l
			$('#wflc').spinit({min:0,max:100,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_wflc']." }); //wf l Critical
			$('#wthc').spinit({min:0,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_wthc']." }); //wt h Critical
			$('#wth').spinit({min:0,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_wth']." }); //wt h
			$('#wtl').spinit({min:-20,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_wtl']." }); //wt l
			$('#wtlc').spinit({min:-20,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_wtlc']." }); //wt l Critical
			$('#rthc').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_rthc']." }); //rt h Critical
			$('#rth').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_rth']." }); //rt h
			$('#rtl').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_rtl']." }); //rt l
			$('#rtlc').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_rtlc']." }); //rt l Critical
			$('#rhhc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_rhhc']." }); // rh h Critical
			$('#rhh').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_rhh']." }); // rh h
			$('#rhl').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_rhl']." }); // rh l
			$('#rhlc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_rhlc']." }); // rh l Critical
			$('#she').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_she']." }); // Storage He 
			$('#shec').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_shec']." }); // Storage He Critical
			$('#shephc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_svphc']." }); // Storage Hep h Critical
			$('#sheph').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_svph']." }); // Storage Hep h Critical
			$('#shepl').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_svpl']." }); // Storage Hep l Critical
			$('#sheplc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$rowSite['alarm_svplc']." }); // Storage Hep l Critical
	";
?> 
        });
</script>

<script type="text/javascript">
function checkGlobal()
{
	if(document.getElementById('ga').checked)
	{
		document.getElementById('alarmsettings').style.display = 'none';
		document.getElementById('use_global').value = 'Y';
	}else{
		document.getElementById('alarmsettings').style.display = '';	
		document.getElementById('use_global').value = 'N';
	}
}

function restoreDefaults()
{
	if (confirm("Are you sure you want to restore the default values?"))
	{
		$settings = new SimpleXMLElement('settings.xml', null, true);
 

			$('#chk1').spinit({min:0,max:23,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->AlarmCheckOne." }); //he
			$('#chk2').spinit({min:0,max:23,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->AlarmCheckTwo." }); //he
            $('#he').spinit({min:0,max:100,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->HeLevelThreshhold." }); //he
			$('#hec').spinit({min:0,max:100,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->HeLevelCriticalThreshhold." }); //he critical
			$('#heb').spinit({min:0,max:100,stepInc:0.01,pageInc:0.1, height: 22, width: 140, initValue: ".$settings->HeBurnThreshhold." }); //heburn
			$('#hephc').spinit({min:0,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$settings->HePressHighCriticalThreshhold." }); //hep h critical
			$('#heph').spinit({min:0,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$settings->HePressHighThreshhold." }); //hep h
			$('#hepl').spinit({min:0,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$settings->HePressLowThreshhold." }); //hep l
			$('#heplc').spinit({min:0,max:20,stepInc:0.1,pageInc:1, height: 22, width: 140, initValue: ".$settings->HePressLowCriticalThreshhold." }); //hep l critical
			$('#wfhc').spinit({min:0,max:100,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterFlowHighCriticalThreshhold." }); //wf h Critical
			$('#wfh').spinit({min:0,max:100,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterFlowHighThreshhold." }); //wf h
			$('#wfl').spinit({min:0,max:100,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterFlowLowThreshhold." }); //wf l
			$('#wflc').spinit({min:0,max:100,stepInc:0.25,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterFlowLowCriticalThreshhold." }); //wf l Critical
			$('#wthc').spinit({min:0,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterTempHighCriticalThreshhold." }); //wt h Critical
			$('#wth').spinit({min:0,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterTempHighThreshhold." }); //wt h
			$('#wtl').spinit({min:-20,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterTempLowThreshhold." }); //wt l
			$('#wtlc').spinit({min:-20,max:180,stepInc:0.5,pageInc:10, height: 22, width: 140, initValue: ".$settings->WaterTempLowCriticalThreshhold." }); //wt l Critical
			$('#rthc').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomTempHighCriticalThreshhold." }); //rt h Critical
			$('#rth').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomTempHighThreshhold." }); //rt h
			$('#rtl').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomTempLowThreshhold." }); //rt l
			$('#rtlc').spinit({min:0,max:200,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomTempLowCriticalThreshhold." }); //rt l Critical
			$('#rhhc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomHumHighCriticalThreshhold." }); // rh h Critical
			$('#rhh').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomHumHighThreshhold." }); // rh h
			$('#rhl').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomHumLowThreshhold." }); // rh l
			$('#rhlc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->RoomHumLowCriticalThreshhold." }); // rh l Critical
			$('#she').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHeLevelThreshhold." }); // Storage He 
			$('#shec').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHeLevelCriticalThreshhold." }); // Storage He Critical
			$('#shephc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHePressHighCriticalThreshhold." }); // Storage Hep h Critical
			$('#sheph').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHePressHighThreshhold." }); // Storage Hep h Critical
			$('#shepl').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHePressLowThreshhold." }); // Storage Hep l Critical
			$('#sheplc').spinit({min:0,max:100,stepInc:1,pageInc:10, height: 22, width: 140, initValue: ".$settings->StoreHePressLowCriticalThreshhold." }); // Storage Hep l Critical
	}
	
}

</script>

</head>
<body onLoad="checkGlobal()">
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php if(isset($_COOKIE["mm2"])){include("menu/manageMenu.php");}else{include("menu/menu.php");}?></div>
<br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>
        <div id="aform">
          <form id="alarms" method="post" action="siteAlarmsDo.php?site_id=<?php echo $site_id; ?>">
          <div id="field" class="afield" style="height:auto; width:100%">
           	    <div id="txt" class="atxt">Use Global Alarm Settings</div>
                <div id="inp" class="ainpcb"><input name="ga" type="checkbox" class="smartcb" id="ga" onChange="checkGlobal()" <?php echo $use_global ?> /></div>
                <input type="hidden" id="use_global" name="use_global" value="N" />
            </div>
            <div id="alarmsettings">
            <br /><div class="line"></div> <br />
          <?php if(strtolower($rowSite['Storage'])=="y") echo"<div style='display:none'>"; ?>
          	<div id="field" class="afield" style="height:auto; width:100%">
           	    <div id="txt" class="atxt">He Level Low</div>
                <div id="inp" class="ainp"><input type="text" id="he" name="he" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
           	    <div id="txt" class="atxt">He Level Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="hec" name="hec" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Burn</div>
                <div id="inp" class="ainp"><input type="text" id="heb" name="heb" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Pressure High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="hephc" name="hephc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Pressure High</div>
                <div id="inp" class="ainp"><input type="text" id="heph" name="heph" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Pressure Low</div>
                <div id="inp" class="ainp"><input type="text" id="hepl" name="hepl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">He Pressure Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="heplc" name="heplc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Flow High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="wfhc" name="wfhc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Flow High</div>
                <div id="inp" class="ainp"><input type="text" id="wfh" name="wfh" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Flow Low</div>
                <div id="inp" class="ainp"><input type="text" id="wfl" name="wfl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Flow Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="wflc" name="wflc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Temp High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="wthc" name="wthc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Temp High</div>
                <div id="inp" class="ainp"><input type="text" id="wth" name="wth" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Temp Low</div>
                <div id="inp" class="ainp"><input type="text" id="wtl" name="wtl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Water Temp Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="wtlc" name="wtlc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Temp High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="rthc" name="rthc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Temp High</div>
                <div id="inp" class="ainp"><input type="text" id="rth" name="rth" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Temp Low</div>
                <div id="inp" class="ainp"><input type="text" id="rtl" name="rtl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Temp Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="rtlc" name="rtlc" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Humidity High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="rhhc" name="rhhc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Humidity High</div>
                <div id="inp" class="ainp"><input type="text" id="rhh" name="rhh" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Humidity Low</div>
                <div id="inp" class="ainp"><input type="text" id="rhl" name="rhl" class="smartspinner" /></div>
            </div>
             <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Room Humidity Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="rhlc" name="rhlc" class="smartspinner" /></div>
            </div>
            <?php if(strtolower($rowSite['Storage'])=="y") echo"</div>"; ?>
            <?php if(strtolower($rowSite['Storage'])=="n") echo"<div style='display:none'>"; ?>
            
            <!--<br /><div class="line"></div> <br />-->
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Level Low</div>
                <div id="inp" class="ainp"><input type="text" id="she" name="she" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Level Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="shec" name="shec" class="smartspinner" /></div>
            </div>
            <br /><div class="line"></div> <br />
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Press High Critical</div>
                <div id="inp" class="ainp"><input type="text" id="shephc" name="shephc" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Press High</div>
                <div id="inp" class="ainp"><input type="text" id="sheph" name="sheph" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Press Low</div>
                <div id="inp" class="ainp"><input type="text" id="shepl" name="shepl" class="smartspinner" /></div>
            </div>
            <div id="field" class="afield" style="height:auto; width:100%">
            	<div id="txt" class="atxt">Storage He Press Low Critical</div>
                <div id="inp" class="ainp"><input type="text" id="sheplc" name="sheplc" class="smartspinner" /></div>
            </div>
            <?php if(strtolower($rowSite['Storage'])=="n") echo"</div>"; ?>
            </div>
            <div id="alarmsbuttons">
            <div id="asavediv">
            <input type="submit" name="Save" id="asave" value="Save" />	  
            </div>
            <div id="arestorediv">
            <input type="button" name="Restore" id="arestore" value="Restore Defaults" onClick="restoreDefaults()" />
            </div>
            </div>
            <input type="hidden" name="mgt" id="mgt" value="<?php echo $mgt ?>" />  
          </form>
        </div>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>
