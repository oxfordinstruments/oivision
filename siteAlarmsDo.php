<?php
if (isset($_GET['site_id']))
	{
		$site_id = $_GET['site_id'];
	}
else
	{
		header("location:error.php?e='Invalid site id'");
	}
$mgt=strtolower($_POST['mgt']);
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password"); //or die("cannot connect");
mysql_select_db("$db_name"); //or die("cannot select DB");

$sql        = "SELECT * FROM sites WHERE site_id='$site_id' LIMIT 1";
$resultSite = mysql_query($sql);
$rowSite    = mysql_fetch_array($resultSite);
mysql_close();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />


<script type="text/javascript">
<!--
function delayer(){
    window.location = "<?php if($mgt == "true"){echo "manage.php";}else{echo "mm_front.php";} ?>"
}
//-->
</script>
</head>
<body onLoad="setTimeout('delayer()', 3000)">
<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/manageMenu.php");?></div><br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>

<?php

ob_start();
$use_global=strtoupper($_POST['use_global']);
$ahe=round((double)$_POST['he'],2);
$ahec=round((double)$_POST['hec'],2);
$aheb=round((double)$_POST['heb'],2);
$avphc=round((double)$_POST['hephc'],2);
$avph=round((double)$_POST['heph'],2);
$avpl=round((double)$_POST['hepl'],2);
$avplc=round((double)$_POST['heplc'],2);
$awfhc=round((double)$_POST['wfhc'],2);
$awfh=round((double)$_POST['wfh'],2);
$awfl=round((double)$_POST['wfl'],2);
$awflc=round((double)$_POST['wflc'],2);
$awthc=round((double)$_POST['wthc'],2);
$awth=round((double)$_POST['wth'],2);
$awtl=round((double)$_POST['wtl'],2);
$awtlc=round((double)$_POST['wtlc'],2);
$arthc=round((double)$_POST['rthc'],2);
$arth=round((double)$_POST['rth'],2);
$artl=round((double)$_POST['rtl'],2);
$artlc=round((double)$_POST['rtlc'],2);
$arhhc=round((double)$_POST['rhhc'],2);
$arhh=round((double)$_POST['rhh'],2);
$arhl=round((double)$_POST['rhl'],2);
$arhlc=round((double)$_POST['rhlc'],2);
$ashe=round((double)$_POST['she'],2);
$ashec=round((double)$_POST['shec'],2);
$asvphc=round((double)$_POST['shephc'],2);
$asvph=round((double)$_POST['sheph'],2);
$asvpl=round((double)$_POST['shepl'],2);
$asvplc=round((double)$_POST['sheplc'],2);

mysql_connect("$host", "$username", "$password"); //or die("cannot connect");
mysql_select_db("$db_name"); //or die("cannot select DB");

$sql="UPDATE `sites` SET `use_global_alarm`='$use_global', `alarm_heb`='$aheb', `alarm_he`='$ahe', `alarm_hec`='$ahec', `alarm_vphc`='$avphc', `alarm_vph`='$avph', `alarm_vpl`='$avpl', `alarm_vplc`='$avplc', `alarm_wthc`='$awthc', `alarm_wth`='$awth', `alarm_wtl`='$awtl', `alarm_wtlc`='$awtlc', `alarm_wfhc`='$awfhc', `alarm_wfh`='$awfh', `alarm_wfl`='$awfl', `alarm_wflc`='$awflc', `alarm_rthc`='$arthc', `alarm_rth`='$arth', `alarm_rtl`='$artl', `alarm_rtlc`='$artlc', `alarm_rhhc`='$arhhc', `alarm_rhh`='$arhh', `alarm_rhl`='$arhl', `alarm_rhlc`='$arhlc', `alarm_she`='$ashe', `alarm_shec`='$ashec', `alarm_svphc`='$asvphc', `alarm_svph`='$asvph', `alarm_svpl`='$asvpl', `alarm_svplc`='$asvplc' WHERE  `site_id`='$site_id' LIMIT 1;";
$resultAlarms = mysql_query($sql);
mysql_close();

echo "<h1>".$rowSite['site_name']." alarm settings have been saved.</h1>";
?>
<br />
      <span class="red">Page will return to <?php if($mgt == "true"){echo "management page";}else{echo "main page";} ?> in 3 seconds</span></h1>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>

