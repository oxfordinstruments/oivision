<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/26/2018
 * Time: 12:28 PM
 */

namespace OiVision;

use mysqli;
use mysqli_sql_exception;

class Functions
{
	private $logger;
	/**
	 * @var array
	 */
	private $mysql;

	/**
	 * functions constructor.
	 * @param \Monolog\Logger $logger
	 */
	public function __construct(\Monolog\Logger $logger)
	{
		require_once '../mysqlInfo.php';
		$this->logger = $logger;
		if (!empty($host)) {
			if (!empty($username)) {
				if (!empty($password)) {
					if (!empty($db_name)) {
						$this->mysql = ['host' => $host, 'username' => $username, 'password' => $password, 'db_name' => $db_name];
					}
				}
			}
		}
	}

	/**
	 * @return array
	 */
	public function getSites()
	{
		$mysql = new mysqli($this->mysql['host'], $this->mysql['username'], $this->mysql['password'], $this->mysql['db_name']);
		$result = $mysql->query("SELECT * FROM sites;");
		$rows = array();
		while ($row = $result->fetch_assoc()){
			array_push($rows, $row);
		}
		$mysql->close();
		return $rows;
	}

	/**
	 * @return array
	 */
	public function getSitesMacs()
	{
		$mysql = new mysqli($this->mysql['host'], $this->mysql['username'], $this->mysql['password'], $this->mysql['db_name']);
		$result = $mysql->query("SELECT mac FROM sites;");
		$rows = array();
		while ($row = $result->fetch_assoc()){

			array_push($rows, strtolower($row['mac']));
		}
		$mysql->close();
		return $rows;
	}

	/**
	 * @param bool/string $mac
	 * @param bool/int $limit
	 * @return array
	 */
	public function getData($mac = false, $limit = false)
	{
		if (!$limit){
			$limitStr = '';
		}else{
			$limitStr = "LIMIT ".$limit;
		}

		$mysql = new mysqli($this->mysql['host'], $this->mysql['username'], $this->mysql['password'], $this->mysql['db_name']);

		$result = $mysql->query("SELECT mac FROM sites WHERE enabled = 'Y';");
		$macs = array();
		while ($row = $result->fetch_assoc()){
			array_push($macs, $row['mac']);
		}
		$result->close();

		$data = [];

		if($mac !== false){
			if(!in_array($mac, $macs)){
				return ['error' => 'mac does not exist'];
			}
			$macs = [$mac];

		}
		foreach ($macs as $mac){
			$tmp = [];
			$sql = sprintf(/** @lang text */ "SELECT * FROM `%s` WHERE new = 0 %s;", $mac, $limitStr);
			$result = $mysql->query($sql);
			if($result->num_rows == 0){
				$result->close();
				continue;
			}

			while ($row = $result->fetch_assoc()){
				array_push($tmp, $row);
			}
			$result->close();
			$data[$mac] = $tmp;
		}


		$mysql->close();
		return $data;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	public function markData(array $data){
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$mysql = new mysqli($this->mysql['host'], $this->mysql['username'], $this->mysql['password'], $this->mysql['db_name']);
		$macErrors = array();
		$macGood = array('updated' => array());

		foreach ($data as $key=>$datum) {
			$af = 0;
			$ids = implode(",", $datum);
			$sql = sprintf(/** @lang text */"UPDATE `%s` SET `new` = 1 WHERE `index` IN(%s);", strtolower($key), $ids);
			try{
				$mysql->query($sql);
				$af = $mysql->affected_rows;
				if($af > 0){
					array_push($macGood['updated'], $key);
				}
			}catch (mysqli_sql_exception $sql_exception){
				$macErrors['errors'][$key] = ['error' => $sql_exception->getMessage(), 'sql' => $sql, 'code' => $sql_exception->getCode() ];
			}

		}

		$mysql->close();

		if(!empty($macErrors)){
			return $macErrors;
		}

		return $macGood;
	}


}