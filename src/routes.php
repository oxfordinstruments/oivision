<?php

use Slim\Http\Request;
use Slim\Http\Response;
use OiVision\Functions;


/**
 * API Usage
 *
 * Required headders:
 * Content-Type: application/json
 * Authorization: {api token}
 *
 * Request type:
 * Json
 *
 * Optional request parameters:
 * limit: {query limit}   Works on /getData and /getData/{mac}
 */


// Routes
/**
 * @route /
 * @api get
 * @return string
 * Default route
 */
$app->get('/', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

/**
 * @route /getSites
 * @api post
 * @return string/json
 * Get all sites and info about them
 */
$app->post('/getSites', function (Request $request, Response $response, array $args) {
	$this->logger->debug("getSites Route");
	$headerContentType = $request->getHeader('HTTP_CONTENT_TYPE');
	$headerAuth = $request->getHeader('HTTP_TOKEN');

	if(!in_array('application/json', $headerContentType)){
		$this->logger->error("getSites: non-json request sent");
		$data = ['error' => "HTTP_CONTENT_TYPE application/json required", 'code' => 400];
		return $response->withJson($data, 400);
	}
	if(!in_array($this->get('settings')['apiKey'], $headerAuth)){
		$this->logger->error(sprintf("getSites: Invalid api key %s", $headerAuth ));
		$data = ['error' => "Api token invalid", 'code' => 401];
		return $response->withJson($data, 401);
	}

	$f = new Functions($this->logger);

	$data = [
		'code' => 200,
		'dataCount' => 0,
		'data' => $f->getSites()

	];
	$data['dataCount'] = count($data['data']);
	return $response->withJson($data, $data['code']);

});

/**
 * @route /getData
 * @api post
 * @parameter (optional) limit: {int}
 * @return string/json
 * Get all data for all mac addresses
 */
$app->post('/getData', function (Request $request, Response $response, array $args) {
	$this->logger->debug("getData Route");
	$headerContentType = $request->getHeader('HTTP_CONTENT_TYPE');
	$headerAuth = $request->getHeader('HTTP_TOKEN');
	if(!in_array('application/json', $headerContentType)){
		$this->logger->error("getData: non-json request sent");
		$data = ['error' => "HTTP_CONTENT_TYPE application/json required", 'code' => 400];
		return $response->withJson($data, 400);
	}
	if(!in_array($this->get('settings')['apiKey'], $headerAuth)){
		$this->logger->error("getData: Invalid api key");
		$data = ['error' => "Api token invalid", 'code' => 401];
		return $response->withJson($data, 401);
	}

	$limit = $request->getParam('limit');
	if(!is_null($limit)){
		$limit = $request->getParam('limit');
	}else{
		$limit = false;
	}

	$f = new Functions($this->logger);
	$data = [
		'code' => 200,
		'dataCount' => 0,
		'data' => $f->getData(false, $limit)

	];
	$data['dataCount'] = count($data['data']);
	return $response->withJson($data, $data['code']);


});

/**
 * @route: /getData/{mac}
 * @api post
 * @parameter (optional) limit: {int}
 * @return string/json
 * Get data for single mac address
 */
$app->post('/getData/{mac}', function (Request $request, Response $response, array $args) {
	$this->logger->debug("getData/".$args['mac']." Route");
	$headerContentType = $request->getHeader('HTTP_CONTENT_TYPE');
	$headerAuth = $request->getHeader('HTTP_TOKEN');
	if(!in_array('application/json', $headerContentType)){
		$this->logger->error(sprintf("getData/%s: non-json request sent", $args['mac']));
		$data = ['error' => "HTTP_CONTENT_TYPE application/json required", 'code' => 400];
		return $response->withJson($data, 400);
	}
	if(!in_array($this->get('settings')['apiKey'], $headerAuth)){
		$this->logger->error("getData: Invalid api key");
		$this->logger->error(sprintf("getData/%s: Invalid api key", $args['mac']));
		$data = ['error' => "Api token invalid", 'code' => 401];
		return $response->withJson($data, 401);
	}

	$limit = $request->getParam('limit');
	if(!is_null($limit)){
		$limit = $request->getParam('limit');
	}else{
		$limit = false;
	}

	$f = new Functions($this->logger);

	$data = [
		'code' => 200,
		'data' => $f->getData($args['mac'], $limit)
	];
	if(key_exists('error',$data['data'])){
		$data['error'] = $data['data']['error'];
		unset($data['data']);
	}
	return $response->withJson($data, $data['code']);


});

/**
 * @route /markData
 * @api post
 * @return string/json
 * Marks data for as consumed.
 * Body must contain a json array of macs. Each mac must have a list of indexes to be consumed
 * { "5c-86-4a-00-c1-3d" : [ 30840, 30841 ] }
 */
$app->post('/markData', function (Request $request, Response $response, array $args) {
	$this->logger->debug("markData Route");
	$headerContentType = $request->getHeader('HTTP_CONTENT_TYPE');
	$headerAuth = $request->getHeader('HTTP_TOKEN');
	if(!in_array('application/json', $headerContentType)){
		$this->logger->error("markData: non-json request sent");
		$data = ['error' => "HTTP_CONTENT_TYPE application/json required", 'code' => 400];
		return $response->withJson($data, 400);
	}
	if(!in_array($this->get('settings')['apiKey'], $headerAuth)){
		$this->logger->error("markData: Invalid api key");
		$data = ['error' => "Api token invalid", 'code' => 401];
		return $response->withJson($data, 401);
	}

	$bc = $request->getBody()->getContents();
	$jsonData = json_decode($bc, true);
	if(is_null($jsonData)){
		$this->logger->error("markData: non-json request sent");
		$data = ['error' => "body does not contain valid json", 'code' => 400];
		return $response->withJson($data, 400);
	}

	$f = new Functions($this->logger);
	$macs = $f->getSitesMacs();

	$invalidMacs = array();
	foreach ($jsonData as $jsonKey=>$jsonDatum){
		if(!in_array(strtolower($jsonKey), $macs) or empty($jsonDatum)){
			array_push($invalidMacs, $jsonKey);
		}
	}
	if(!empty($invalidMacs)){
		$im = implode(", ", $invalidMacs);
		$this->logger->error("markData: invalid mac(s) sent");
		$data = ['error' => sprintf("Invalid mac(s) found. No data marked consumed. Macs: %s", $im), 'code' => 400];
		return $response->withJson($data, 400);
	}

	$rtn = $f->markData($jsonData);
	if(key_exists('errors', $rtn)){
		$this->logger->error(sprintf("markData: Sql exception occurred: %s ",json_encode($rtn) ));
		$data = ['error' => "Sql exception(s) occurred.", "exceptions" => $rtn['errors'], 'code' => 400];
		return $response->withJson($data, 400);
	}

	$data = [
		'code' => 200,
		'data' => $rtn,
	];
	return $response->withJson($data, $data['code']);


});