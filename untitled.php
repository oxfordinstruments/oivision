<table>
    <tr>
        <td>Alabama</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.alabamainternationaldragway.com" target="_blank"> Alabama International Dragway</a> 1/4 Mile 
            IHRA 
            Div 2 </td>
        <td> Jason 
            Collins
            
            P.O. Box 426 
            Steele. AL 35987 </td>
        <td> Phone: 256-538-7223 
            Fax: 256-354-5014 </td>
        <td> RACED(Best: Split @ 7 cars) 
            Covered the IHRA Div. 2 Bracket Finals here once, and raced once. 
            Former national event track. Large swooping downhill-uphill right 
            after 1/4-mile. Decent amount of paved parking. Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.atmoredragway.com" target="_blank"> Atmore Dragway </a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td> Karen 
            Allen
            Atmore Dragway 
            PO Box 912 
            Flomaton, AL 36441 <a href="mailto:karen@atmoredragway.com"> karen@atmoredragway.com </a></td>
        <td> Track:251-583-6797
            Fax: 251-296-3911 </td>
        <td> CLOSED </td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.baileytondragstrip.com/"> Baileyton "Good Time" Drag Strip</a> 1/8 mile
            </p></td>
        <td>Baileyton, 
            Alabama </td>
        <td> 256-796-2892 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.holidayraceway.com"> Holiday Raceway</a> 1/8 mile
            </p></td>
        <td>Woodstock, AL </td>
        <td> 205-938-2123 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.huntsvilledragway.com/" target="_blank"> Huntsville Dragway</a> 1/8 Mile 
                NHRA 
                Div. 2 </p></td>
        <td>Toney, 
            Ala. </td>
        <td> (256) 859-0807 </td>
        <td> RACED
            Raced here three times. Outstanding facility with even better 
            events by the famed George Howard. Tight rollout. Can't see 
            opponent's front wheel on the starting line because of guardrail.24/7 
            Live Webcam. Accutime installed March '05. Sunoco fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.i-22motorsports.net/">I-22 Motorsports Park</a> 1/8 Mile </td>
        <td>Eldridge, 
            AL
            <div sizset="0" sizcache="54" id="site">
                <div sizset="0" sizcache="54" id="container0">
                    <div sizset="0" sizcache="54" id="wrapper">
                        <div id="wrap_right">
                            <div id="contactInfo"> <a href="mailto:info@i-22motorsports.net" class="maillink"> info@i-22motorsports.net</a> </div>
                        </div>
                    </div>
                </div>
            </div></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Lassiter Mountain Dragway
            
            1/8 mile </td>
        <td>Gardendale, AL </td>
        <td> 205-841-8696 </td>
        <td> VISITED
            Visited here on a non-race day. Literally on top of a mountain, 
            5mi north of Birmingham. Small, but neat. Track lies down in 
            between earthen banks. Guardrail just around the scoreboard area. </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.mobiledragway.com/" target="_blank"> Mobile Dragway </a>1/8 Mile 
                NHRA 
                Div. 2 </p></td>
        <td>Daphne, 
            Ala. </td>
        <td> (800) 232-7961 </td>
        <td> TSI RaceNet </td>
    </tr>
    <tr>
        <td><a href="http://www.mmpdragracing.com/" target="_blank">Montgomery 
            Motorsports Park</a> 1/4 Mile 
            IHRA 
            Div. 2 </td>
        <td>Montgomery, Ala. </td>
        <td> (334) 260-9660 </td>
        <td> RACED (Best: WIN in First Visit, both races of a Pro-Am Double Header) 
            Compulink. </td>
    </tr>
    <tr>
        <td>One Way Dragway
            
            1/4 mile
            </p></td>
        <td>Cottonwood, AL </td>
        <td> 334-691-4431 </td>
        <td></td>
    </tr>
    <tr>
        <td>Phenix 
            Drag Strip
            
            1/8 mile </td>
        <td>Phenix City, AL </td>
        <td> 334-291-0494 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.sandmtndragway.com/">Sand Mountain Dragway</a> 1/8 Mile </td>
        <td>Jeff 
            Rucks
            
            846 County Road 419
            Section, AL 35771 </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a name="AK"></a> Alaska</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.akracewaypark.com" target="_blank"> Alaska Raceway Park </a>1/4 Mile 
            IHRA 
            Div. 5 </td>
        <td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Cassie L. Baughman 
                3900 English Bay Dr 
                Wasilla, AK 99654 <a href="mailto:management@akracewaypark.com"> management@akracewaypark.com</a> </p></td>
        <td>Track: 907-746-7223 
            Fax: 907-746-7224 </td>
        <td>TSI. </td>
    </tr>
    <tr>
        <td><a name="AZ"></a> Arizona</td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td><a href="http://www.firebirdraceway.com/" target="_blank">Firebird 
            International Raceway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Chandler, 
            Ariz. </td>
        <td> (602) 268-0200 </td>
        <td> Compulink. 
            Elevation: 1300' </td>
    </tr>
    <tr>
        <td><a href="http://www.sirdragstrip.com/" target="_blank">Southwestern 
            International Raceway</a> 1/4 Mile 
            IHRA 
            Div. 6 </td>
        <td>Bernie 
            Longjohn <a href="mailto:sirtucson@hotmail">sirtucson@hotmail</a> Tucson, Ariz. </td>
        <td> Track: (520) 762-9700 
            Fax: (520) 762-9777 </td>
        <td> Compulink. 
            Elevation: 3100' </td>
    </tr>
    <tr>
        <td><a href="http://www.speedworlddragstrip.com/" target="_blank">Speedworld 
            Dragstrip</a> 1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td><p align="left"> Northwest Phoenix, Ariz. </p></td>
        <td> (623) 388-2424 EVENT INFO LINE </td>
        <td> Chrondek. 
            Elevation: 1500' </td>
    </tr>
    <tr>
        <td><a name="AR"></a> Arkansas</td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td>Centerville Dragway
            
            1000' </td>
        <td>P.O. Box 160
            
            Centerville, AR 72829 </td>
        <td> 501-576-4001 </td>
        <td></td>
    </tr>
    <tr>
        <td>George 
            Ray's Wildcat Hot Rod Dragstrip </td>
        <td>Paragould, AR </td>
        <td> 501-236-8247 </td>
        <td> (heads-up racing only!) </td>
    </tr>
    <tr>
        <td>Newport Raceway
            
            1/8 mile </td>
        <td>Newport, 
            AR </td>
        <td> 501-455-2398 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.prescottraceway.net/">Prescott Raceway</a>
            </p></td>
        <td>5121 
            Highway 53 East
            
            Prescott, AR 71857 
            Jackie and Velvet Lewis <font size="1" face="Arial" color="#b1b1b4"> <a href="mailto:jackie-lewis@live.com">jackie-lewis@live.com </a></td>
        <td> Track:
            870-887-3984 </td>
        <td></td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.two-laneblacktopdragstrip.com/"> Two Lane Blacktop Dragstrip </a></td>
        <td>Daniel 
            Yoder
            <address>
            <span style="FONT-STYLE: normal"><font size="1" face="Arial" style="font-style: normal"> 320 Post Oak Drive </span>
            </address>
            <address>
            <font size="1" face="Arial" style="font-style: normal"> Bee Branch, 
            AR 7201 <a href="mailto:57chevy@artelco.com">57chevy@artelco.com</a>
            </address></td>
        <td> (501) 253-2867 </td>
        <td></td>
    </tr>
    <tr>
        <td><a name="CA"></a> California</td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td><a href="http://www.famosoraceway.com/" target="_blank">Auto Club Famoso 
            Raceway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Bakersfield, Calif. </td>
        <td> (661) 399-2210 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.baronadrags.com/" target="_blank">Barona 1/8 Mile 
            Drag Strip </a>1/8 Mile 
            NHRA 
            Div. 7 </td>
        <td>Lakeside, 
            Calif. </td>
        <td> (760) 787-1403 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.californiaspeedway.com/track/CaliforniaDragway.jsp" target="_blank"> California Dragway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Fontana, 
            Calif. </td>
        <td> (909) 429-5060 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><p><font size="1" face="Arial" color="#808080"> Carlsbad Raceway* 
                1/8 mile </p></td>
        <td><font size="1" face="Arial" color="#808080"> Escondido, CA </td>
        <td><font size="1" face="Arial" color="#808080"> 760-480-8369 </td>
        <td><font size="1" face="Arial" color="#808080"> *defunct </td>
    </tr>
    <tr>
        <td><a href="http://www.infineonraceway.com/" target="_blank">Infineon 
            Raceway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Sonoma, 
            Calif. </td>
        <td> (707) 938-8448 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><font color="#808080"> <a href="http://www.inyokerndragstrip.com/" target="_blank"> Inyokern Dragstrip* </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td><font size="1" face="Arial" color="#808080"> Inyokern, Calif. </td>
        <td><font size="1" face="Arial" color="#808080"> (760) 371-7777 </td>
        <td><font size="1" face="Arial" color="#808080"> *Closed 
            Elevation: 2400' </td>
    </tr>
    <tr>
        <td><a href="http://www.irwindalespeedway.com/" target="_blank">Irwindale 
            Dragstrip </a>1/8 Mile 
            NHRA 
            Div. 7 </td>
        <td>Irwindale, 
            Calif. </td>
        <td> (626) 358-1100 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.lacr.net/" target="_blank">Los Angeles County 
            Raceway </a> <font color="#808080"> 1/4 Mile
            NHRA 
            Div. 7 </td>
        <td><font size="1" face="Arial" color="#808080">Palmdale, Calif. <a href="mailto:lacr2006@yahoo.com">lacr2006@yahoo.com</a></td>
        <td><font size="1" face="Arial" color="#808080">(661) 533-2224 </td>
        <td><font size="1" face="Arial" color="#808080">CLOSED July 29, 2007 
            Chrondek 
            Elevation: 2700' </td>
    </tr>
    <tr>
        <td><a href="http://www.pomonaraceway.com/" target="_blank">Pomona Raceway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Pomona, 
            Calif. </td>
        <td> (909) 593-7010 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td>Rialto 
            Raceway
            
            1/8 mile </td>
        <td>Rialto, 
            CA </td>
        <td> 909-424-3196 </td>
        <td> Juniors only </td>
    </tr>
    <tr>
        <td><a href="http://www.reddingdragstrip.net/" target="_blank">Redding Drag 
            Strip </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Redding, 
            Calif. </td>
        <td> (530) 221-1311 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.sacramentoraceway.com/" target="_blank">Sacramento 
            Raceway Park </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Sacramento, Calif. </td>
        <td> (916) 363-2653 </td>
        <td> Chrondek. </td>
    </tr>
    <tr>
        <td><a href="http://www.samoadragstrip.com/" target="_blank">Samoa Drag 
            Strip </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Eureka, 
            Calif. </td>
        <td> (707) 443-5203 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.teampossibilities.com/" target="_blank">Team 
            Possibilities Jr. Raceway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td><p align="left"> San Diego, Calif. </p></td>
        <td> (619) 390-1332 </td>
        <td> Juniors only </td>
    </tr>
    <tr>
        <td><a name="CO"></a> Colorado </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.bandimere.com/" target="_blank">Bandimere Speedway </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Denver, 
            Colo. </td>
        <td>(303) 697-6001 </td>
        <td>Compulink. 
            Elevation: 5800' </td>
    </tr>
    <tr>
        <td><a href="http://www.julesburgracing.com" target="_blank">Julesburg Drag 
            Strip</a> 1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Julesburg, Colo. </td>
        <td>(970) 474-3779 </td>
        <td>Elevation: 3500' </td>
    </tr>
    <tr>
        <td><a href="http://www.pueblomotorsportspark.com/" target="_blank">Pueblo 
            Motorsports Park </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Pueblo, 
            Colo. </td>
        <td>(719) 583-0907 </td>
        <td>Compulink. 
            Elevation: 4900' </td>
    </tr>
    <tr>
        <td><a href="http://www.tsmraceway.com/" target="_blank">TSM Raceway </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Mead, 
            Colo. </td>
        <td>(970) 689-9055 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.western-colorado-dragway.com/" target="_blank"> Western Colorado Dragway </a>1/4 Mile 
                NHRA 
                Div. 5 </p></td>
        <td>Grand 
            Junction, Colo. </td>
        <td>(970) 243-9022 </td>
        <td></td>
    </tr>
    <tr>
        <td>Delaware</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.delawareracing.com/" target="_blank"> U.S. 13 Dragway Inc.</a></td>
        <td>Charlie Cathell
            
            Delmar, DE </td>
        <td>(302) 846-3968 </td>
        <td>RACED (Best: semi)
            Raced the Bracket Finals and a couple NHRA Div'l/Nat'l Open events 
            here. 30' below sea level, this place is <i>fast</i>. 
            Shutdown narrows to one lane after awhile. Compulink. </td>
    </tr>
    <tr>
        <td>Florida</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.bradentonmotorsports.com/" target="_blank">Bradenton 
            Motorsports Park</a> 1/4 Mile 
            IHRA 
            Div. 2 </td>
        <td>Bradenton, Fla. </td>
        <td> (941) 748-1320 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://cfracingcomplex.com/">Central Florida Racing Complex </a> 1/8 mile 
            IHRA
            Div. 2 </td>
        <td>10694 
            Cosmonaut Blvd 
            
            Lot 103
            Orlando FL 32824 </td>
        <td> 407-812-4004 </td>
        <td> Portatree Timing </td>
    </tr>
    <tr>
        <td><a href="http://www.countylinedragwayinc.com/">Countyline Dragway, Inc.</a> 1/8 Mile 
            IHRA
            Div. 2 </td>
        <td>19999 Okeechobee Road
            
            Miami, FL 33018 <a href="mailto:info@countylinedragwayinc.com"> info@countylinedragwayinc.com</a></td>
        <td><strong>305-303-1098</strong></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.ecdragway.com/" target="_blank"> Emerald 
            Coast Dragway </a> 1/8 Mile 
            NHRA 
            Div. 2 </td>
        <td> Holt, 
            Fla. </td>
        <td> (334) 299-0303 </td>
        <td> CLOSED 
            TSI. </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.gainesvilleraceway.com/" target="_blank">Gainesville 
                Raceway </a>1/4 Mile 
                NHRA 
                Div. 2 </p></td>
        <td>Gainesville, Fla. </td>
        <td> (352) 377-0046 </td>
        <td> Compulink. 
            Track Radio 88.5FM. 
            Elevation: 167' </td>
    </tr>
    <tr>
        <td><a href="http://www.immrace.com/" target="_blank"> Immokalee Regional Raceway</a><a href="http://www.akracewaypark.com" target="_blank"> </a>1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td>Ralph 
            Hester
            
            P.O. Box 2023 
            Immokalee, FL 34142 <a href="mailto:IMMRACE@aol.com">IMMRACE@aol.com</a></td>
        <td> Track:239-657-5954 
            Office:800-826-1947 
            FAX:239-657-7013 </td>
        <td> RACED (Best: WIN - Stock)
            Accutime. </td>
    </tr>
    <tr>
        <td><p align="left"><font size="1" face="Arial" color="#808080"> Jax Raceway 
                1/8 Mile 
                NHRA 
                Div. 2 </p></td>
        <td><font size="1" face="Arial" color="#808080">Jacksonville, Fla. </td>
        <td><font size="1" face="Arial" color="#808080">(904) 757-5425 </td>
        <td><font size="1" face="Arial" color="#808080"> DEFUNCT. Closed 
            2007
            Chrondek. </td>
    </tr>
    <tr>
        <td><a href="http://www.lakelandmotorsportspark.com/">Lakeland Motorsports 
            Park</a> 1/8 mile </td>
        <td>8100 U.S. 
            Hwy 33 North
            
            Lakeland, FL 33809 </td>
        <td> 863-984-1145 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://racepbir.com/">Palm Beach International Raceway</a> (formerly Moroso Motorsports Park) 
            1/4 Mile 
            IHRA 
            Div. 2 <font size="1"></td>
        <td>17047 
            Beeline Hwy. 
            
            Jupiter, FL 33478 </td>
        <td> (561) 622-1400 </td>
        <td> RACED (Best: WIN in First Visit - IHRA Nitro Jam)
            Fiber-Optic
            Compulink. Tight rollout, much like an Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.speedworlddragway.com/" target="_blank">Orlando 
            Speed World Dragway </a>1/4 Mile 
            NHRA 
            Div. 2 </td>
        <td>Orlando, 
            Fla. </td>
        <td> (407) 568-5522 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td>Power House Drag Strip
            
            1/8 mile
            </p></td>
        <td>Panama Cty, FL </td>
        <td> 850-762-8885 </td>
        <td></td>
    </tr>
    <tr>
        <td>Sebring Dragway
            
            1/4 mile </td>
        <td>Sebring, FL </td>
        <td> 941-655-1555 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://showtimedragstrip.us/">Showtime Dragstrip</a> (formerly Sunshine 
                Dragstrip) 
                1/8 Mile 
                NHRA 
                Div. 2 </p></td>
        <td>4550 Ulmerton Rd
            
            Clearwater, Fla. </td>
        <td> (727) 545-3596 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a name="GA"></a> Georgia</td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td><a href="http://www.atlantadragway.com/" target="_blank">Atlanta Dragway </a>1/4 Mile 
            NHRA 
            Div. 2 </td>
        <td>Commerce, Ga. </td>
        <td> (706) 335-2301 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.atlantamotorspeedway.com/">Atlanta Motor Speedway</a> 1/8 Mile </td>
        <td></td>
        <td></td>
        <td> Friday Night heads-up 1/8th mile drags on 
            the pit road! </td>
    </tr>
    <tr>
        <td><p> <a target="_blank" href="http://www.brainerdracing.com/"> Brainerd Optimist Club Drag Strip</a> 1/8 mile </p></td>
        <td>745 Scruggs Rd
            
            Ringgold, GA 30736 <a href="mailto:slongley54@aol.com">slongley54@aol.com</a></td>
        <td> 706-891-9831 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.dmpdragway.com" target="_blank"> Douglas Motorsports Park</a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td><div style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> <font size="1">Billy Raulerson <font size="1" face="Arial" color="#000000"> <span class="575285419-09112004">1560 Ronnie Walker Rd 
                Nichols, GA 31554 </span> <a href="mailto:dmpdragway@windstream.net"> <font size="1">dmpdragway@windstream.net </a></div></td>
        <td><font size="1" face="Arial" color="#000000" narrow=""><span class="575285419-09112004"> Track: 912-384-7733</span></td>
        <td> RACED (Best: Semi)
            Formerly Southern Dragway. New timing, PA, and scoreboards in 2005. 
            Compulink. </td>
    </tr>
    <tr>
        <td>Headhunter Dragway 
            1/8 mile </td>
        <td>Eatonton, GA </td>
        <td> 706-485-2302 </td>
        <td></td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Hinesville Raceway Park* 
            1/8 mile </td>
        <td><font size="1" face="Arial" color="#808080">Hinesville, GA </td>
        <td></td>
        <td><font size="1" face="Arial" color="#808080"> *Now defunct. Once home to 2002 Summit World Champion Chip Horton. </td>
    </tr>
    <tr>
        <td><a href="http://www.lagrangetroupcountydragstrip.com/" target="_blank"> Lagrange Troup County Dragstrip</a></td>
        <td>123 Heard Road 
            
            Lagrange, GA 30240 <a href="mailto:lhmotion@yahoo.com">lhmotion@yahoo.com</a><a href="mailto:lhmotion@yahoo.com">lhmotion@yahoo.com</a></td>
        <td> (706) 881-5411 </td>
        <td> TSI </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.maconnationaldragway.com/" target="_blank">Macon 
                National Dragway </a>1/4 Mile 
                NHRA 
                Div. 2 </p></td>
        <td>Danville, Ga. </td>
        <td> (205) 229-0905 </td>
        <td></td>
    </tr>
    <tr>
        <td>Paradise Dragway 
            1/8 mile </td>
        <td>Calhoun, GA </td>
        <td> 706-629-6161 </td>
        <td></td>
    </tr>
    <tr>
        <td><font color="#808080"> Savannah Dragway 
            1/8 Mile 
            NHRA 
            Div. 2 </td>
        <td><font size="1" face="Arial" color="#808080">Bloomingdale, Ga. </td>
        <td><font size="1" face="Arial" color="#808080">(912) 234-1965 </td>
        <td><font size="1" face="Arial" color="#808080"> DEFUNCT . Closed 
            2006-07 
            Port-A-Tree. </td>
    </tr>
    <tr>
        <td><a href="http://www.savannahriverdragway.com/">Savannah River Dragway</a></td>
        <td>6118 Savannah Hwy
            
            Sylvania, GA USA 30467 <a href="mailto:screvenmotorspeedway@yahoo.com"> screvenmotorspeedway@yahoo.com</a> <span style="display: none;"></span></td>
        <td> (912) 857-4884 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.silverdollarraceway.com/" target="_blank">Silver 
            Dollar Raceway </a>1/4 Mile 
            NHRA 
            Div. 2 </td>
        <td>Reynolds, Ga. </td>
        <td> (478) 847-4414 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.racesgmp.com" target="_blank">South Georgia 
            Motorsports Park</a> 1/4 Mile 
            NHRA 
            Div. 2 </td>
        <td>Cecil, Ga. </td>
        <td> (229) 896-7000 </td>
        <td> RACED (Best: Won S/SS Combo)
            Compulink. Track Radio 88.5FM 
            Loose rollout. Sunoco. </td>
    </tr>
    <tr>
        <td><font color="#808080"> Southeastern International Dragway 
            1/8 Mile 
            NHRA 
            Div. 2 </td>
        <td><font color="#808080"> Dallas, Ga. </td>
        <td></td>
        <td><font size="1" face="Arial" color="#808080"> *DEFUNCT. Closed in 
            2005
            TSI. </td>
    </tr>
    <tr>
        <td><a href="http://www.us19-dragway.com/" target="_blank">U.S. 19 Dragway</a> 1/8 Mile 
            NHRA 
            Div. 2 </td>
        <td>Albany, Ga. </td>
        <td> (229) 431-0077 </td>
        <td> TSI. </td>
    </tr>
    <tr>
        <td><a name="HI"></a> Hawaii</td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td><font color="#808080"><a href="http://www.hawaiiracewaypark.com/drag.html"> Hawaii Raceway Park</a> 1/4 mile </td>
        <td><font size="1" face="Arial" color="#808080"> Waipahu, Oahu </td>
        <td><font size="1" face="Arial" color="#808080"> 808-682-4994 </td>
        <td><font size="1" face="Arial" color="#808080"> Defunct, 2006
            TSI. </td>
    </tr>
    <tr>
        <td><span class="081345813-21012005">Hilo Dragstrip </span>IHRA </td>
        <td><span class="081345813-21012005">Alan Martins &amp; Pat Koga</span> <span class="081345813-21012005"> 2229 Ainakahele St</span> <span class="081345813-21012005"> Hilo, HI 96720</span></td>
        <td> Alan: 808-960-3124 
            Pat: 808-933-6200 </td>
        <td> Chrondek. </td>
    </tr>
    <tr>
        <td><a href="http://www.islandracer.com/kauai.htm" target="_blank">Kauai 
            Raceway Park </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Mana, Kauai, Hawaii </td>
        <td> (808) 823-6749 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.mrp.org/" target="_blank">Maui Raceway Park </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td><p align="left"> Puunene, Maui, Hawaii </p></td>
        <td> (808) 281-1273 </td>
        <td></td>
    </tr>
    <tr>
        <td><a name="ID"></a> Idaho</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Drag City Raceway* 
            1/4 mile </td>
        <td><font size="1" face="Arial" color="#808080"> Pocatello, ID </td>
        <td><font size="1" face="Arial" color="#808080"> 208-766-4515 </td>
        <td><font size="1" face="Arial" color="#808080"> *Closed in 2001. </td>
    </tr>
    <tr>
        <td><a href="http://www.firebirdonline.com/" target="_blank"> Firebird Raceway </a>1/4 Mile 
            NHRA 
            Div. 6 </td>
        <td>Boise, 
            Idaho </td>
        <td>(208) 938-8986 </td>
        <td>Home of the famed Nightfire Nationals. Compulink. 
            Elevation: 2700' </td>
    </tr>
    <tr>
        <td><a href="http://www.highdesertspeedway.com" target="_blank">High Desert 
            Speedway</a> 1/8 Mile </td>
        <td><p align="left"> 1999 South 1600 East 
                Gooding, ID </p></td>
        <td>Mitch 208-431-3821
            Jodie 208-431-9596 </td>
        <td></td>
    </tr>
    <tr>
        <td><a name="IL"></a> Illinois</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.carsforkidsaccelaquarterraceway.com/">Accelaquarter 
            Raceway Park</a> 1/8 Mile </td>
        <td>1515 Hwy. 
            34 N
            
            Harrisburg, IL </td>
        <td> GENE CHURCH 
            618-252-3727 or
            618-499-5459 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.byrondragway.com/" target="_blank"> Byron Dragway</a> 1/4 Mile 
            IHRA 
            Div. 5 </td>
        <td>Ron Leek
            
            1924 23rd Avenue 
            Rockford, IL 61104 </td>
        <td> Track:815-234-8405 
            Office:815-398-1060 
            FAX: 815-398-1146 </td>
        <td> VISITED
            Covered the US Class Nationals here. WIDE track. Plenty of 
            shutdown, but the last return road is not at the very end. Famed 
            promoter Ron Leek runs a great, tight show, and a clean facility. 
            Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.colescountydragwayusa.com/" target="_blank">Coles 
            County Dragway </a>1/8 Mile 
            NHRA 
            Div. 3 </td>
        <td><div> Rod Viehland &amp; Darwin Korson </div>
            <div> Track &amp; Mailing Address </div>
            <div> 4700 W State St </div>
            <div> Charleston IL, 61920 </div>
            <div> <a href="mailto:colescountydragway@gmail.com"> colescountydragway@gmail.com</a> </div></td>
        <td><div> Track: 217-345-7777 </div>
            <div> Office: 217-512-0673 </div></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.cordovadrag.com" target="_blank"> Cordova Dragway Park </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Scott &amp; 
            Laura Gardner
            
            P.O. Box 442 <font size="1" face="Arial" color="#000000" id="role_document1"> (Physical address: 19425 IL Rt 84 N) 
            Cordova, IL 61242 <font size="1" face="Arial" color="#000000" id="role_document0"> <a href="mailto:cordovadragway@aol.com">cordovadragway@aol.com</a></td>
        <td> Track:309-654-2110 
            Office:309-654-2110 
            FAX:309-654-2282 </td>
        <td> Some of the nicest people and best promoters in the business! Compulink. </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.gatewayraceway.com/" target="_blank"> <font color="#808080">Gateway 
                International Raceway </a><font color="#808080">1/4 Mile 
                NHRA 
                Div. 3 </p></td>
        <td><font size="1" face="Arial" color="#808080">Madison, 
            Ill. </td>
        <td><font size="1" face="Arial" color="#808080">CLOSED </td>
        <td><font size="1" face="Arial" color="#808080"> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.i57dragstrip.com/"> I-57 Dragstrip</a> 1/8 Mile </td>
        <td>PO Box 
            633
            
            (Physical Address: 6112 Hill City Road) 
            Benton, IL 62812 <a href="mailto:baileyi57@gmail.com">baileyi57@gmail.com</a></td>
        <td> 618-439-6039 </td>
        <td></td>
    </tr>
    <tr>
        <td>Mason 
            County Dragway
            
            1/8th mile </td>
        <td>Havana, IL </td>
        <td> 309-543-6124 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.midstatedragway.com/" target="_blank">Midstate 
            Dragway </a>1/8 Mile 
            NHRA 
            Div. 3 </td>
        <td>Havana, 
            Ill. </td>
        <td> (217) 245-1776 </td>
        <td> Formerly Mason County Dragway? TSI. </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.route66raceway.com/" target="_blank">Route 66 
                Raceway </a>1/4 Mile 
                NHRA 
                Div. 3 </p></td>
        <td>Joliet, 
            Ill. </td>
        <td> (815) 722-5500 </td>
        <td> Ground by IHRA's Jim Weinert, and then the records fell, one after 
            another! Compulink. </td>
    </tr>
    <tr>
        <td><a name="IN"></a> Indiana</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.bunkerhillrace.com/" target="_blank">Bunker Bill 
            Dragstrip </a>1/8 Mile 
            NHRA 
            Div. 3 </td>
        <td>Bunker 
            Hill, Ind. </td>
        <td> (765) 689-8248 </td>
        <td> TSI. </td>
    </tr>
    <tr>
        <td><a href="http://www.crossroadsdragway.com">Crossroads Dragway</a> <a href="http://www.wabashvalleydragway.com/" target="_blank"> </a>1/8 Mile 
            NHRA 
            Div. 3 </td>
        <td>3701 U.S. 
            41
            
            Terre Haute, IN 47802 </td>
        <td> (812) 229-2167 </td>
        <td> formerly Wabash Valley Dragway or Terre Haute Action Dragstrip </td>
    </tr>
    <tr>
        <td><a href="http://racecmp.com/" target="_blank">Chandler Motorsports Park</a> 1/8 Mile 
            NHRA 
            Div. 3 </td>
        <td>Jonathan 
            Smiddy 
            
            1099 Inderrieden Rd 
            Chandler, IN 47610 </td>
        <td> (812) 925-3685 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.lyonsracewaypark.com/">Lyons Raceway Park</a> (Formerly E.T. Raceway) 
                1/8 Mile 
                NHRA 
                Div. 3 </p></td>
        <td>Lyons Raceway Park
            
            P. O. Box 246 
            Wheatland, IN 47597
            <div> <a href="mailto:mail@lyonsracewaypark.com">mail@lyonsracewaypark.com</a> </div>
            <div> Facebook: <a href="http://www.facebook.com/lyonsracewaypark"> www.facebook.com/lyonsracewaypark</a> </div></td>
        <td> Track: (812) 659-2951 </td>
        <td> New 2009 Version Compulink </td>
    </tr>
    <tr>
        <td><a href="http://www.munciedragway.com" target="_blank">Muncie Dragway</a> 1/4 Mile 
            NHRA </td>
        <td>Kevin &amp; 
            Susan White
            
            7901 E. St. Rd. 28/67 
            Albany, IN 47320 <a href="mailto:munciedragway@yahoo.com">munciedragway@yahoo.com</a></td>
        <td> Track:765-789-8470 
            Cell:765-713-9034 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.nolimitraceway.com/" target="_blank">No Limit 
            Raceway</a> (formerly U.S. 41 Dragway) 
            1/4 Mile </td>
        <td><p style="FONT-SIZE: 11px"> Physical Address: 
                2695 W 50 S 
                Morocco, IN 47963 
                
                Ken Reed 219-808-9474 <a href="mailto:nlrdragracing@hotmail.com">nlrdragracing@hotmail.com</a> </p></td>
        <td> Track/Office: 219-285-2200 </td>
        <td> 2010 - New TSI timing system and new LED 
            scoreboards </td>
    </tr>
    <tr>
        <td><a href="http://www.irponline.com/">O'Reilly Raceway Park</a> 1/4 Mile 
            NHRA 
            Div. 3 </td>
        <td>Clermont, 
            Ind. </td>
        <td> (317) 291-4090 </td>
        <td> RACED
            Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.osceoladragway.com/" target="_blank"> Osceola Dragway</a><a href="http://www.keystoneraceway.net/" target="_blank"> </a>1/4 Mile </td>
        <td>Ruth 
            Chizum
            
            56328 Ash Road 
            Osceola, IN 46561 </td>
        <td> Track:574-674-8400 </td>
        <td> TSI. </td>
    </tr>
    <tr>
        <td><a href="http://www.newhopedragstrip.com/" target="_blank">Speed's New 
            Hope Int'l Dragstrip</a> 1/8 Mile </td>
        <td>7848 
            Villas Rd.
            
            Spencer, IN 47429 <a href="mailto:contact@newhopedragstrip.com"> contact@newhopedragstrip.com</a></td>
        <td> 1-812-828-0800 </td>
        <td></td>
    </tr>
    <tr>
        <td><a name="IA"></a> Iowa</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.cedarfallsraceway.com/" target="_blank"> Cedar Falls Raceway </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Dan 
            Driscoll <font size="1" face="Arial" color="#000000" id="role_document4"> 6200 W Bennington Rd 
            PO Box 442 
            Cedar Falls, IA 50613 <font size="1" face="Arial" color="#000000" id="role_document2"> <a href="mailto:cfrmanager@aol.com">cfrmanager@aol.com</a></td>
        <td> Track:319-987-2537 
            Office: <font size="1" face="Arial" color="#000000" id="role_document3">309-654-2110 </td>
        <td> Completely redone around 2001 or so. <font size="1" face="Arial" color="#000000" id="role_document5">Track is 
            operated by Scott &amp; Laura Gardner 
            Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.eddyvilleraceway.com" target="_blank"> Eddyville Raceway</a> 1/8 Mile 
            NHRA 
            Div. 5 </td>
        <td><font size="1" face="Arial" color="#000000" id="role_document6"> Gerald Kramer
            Track address:
            3260 Merino Ave 
            Oskloosa, IA 52577 
            Office Address:
            2462 Highway 163 
            Pella, IA 50219 <a href="mailto:manager@eddyvilleraceway.com"> manager@eddyvilleraceway.com</a></td>
        <td><font size="1" face="Arial" color="#000000" id="role_document6">
            <div> Track PH: 641-969-5596</div>
            <div> Office Ph: 641-780-3534</div></td>
        <td><font size="1" face="Arial" color="#000000" id="role_document6">
            <div> Track owned by Scott Gardner and Gerald Kramer. Completely 
                redone by Carl Moyer around 1992. </div>
            <div> Lots of grass and pavement. Accutime</div></td>
    </tr>
    <tr>
        <td><a href="http://www.humboldtcountydragway.com/">Humboldt County Dragway</a></td>
        <td><div id="wrap">
                <div id="page">
                    <div role="main" class="narrowcolumn" id="content1">
                        <div class="post" id="post-130">
                            <div class="entry"> 2141 Penn Ave.
                                Humboldt, IA 50548 <a href="mailto:rick@humboldtcountydragway.com"> rick@humboldtcountydragway.com</a> </div>
                        </div>
                    </div>
                </div>
            </div></td>
        <td> (515) 332-2510 
            (515) 573-7833 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.midamericamotorplex.com/" target="_blank">Rolling 
            Thunder Dragstrip at Mid-America Motorplex </a>1/8 Mile 
            NHRA 
            Div. 5 </td>
        <td>Pacific 
            Junction, Iowa </td>
        <td> (712) 622-8122 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.tristateraceway.com/" target="_blank">Tri-State 
                Raceway </a>1/4 Mile 
                NHRA 
                Div. 5 </p></td>
        <td>Earlville, Iowa </td>
        <td> (563) 923-3724 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a name="KS"></a> Kansas </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.hpt.com/" target="_blank">Heartland Park Topeka </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Topeka, 
            Kan. </td>
        <td>(785) 862-4781 </td>
        <td>Compulink. </td>
    </tr>
    <tr>
        <td>Mid-America Dragway
            
            1/4 mile
            </p></td>
        <td>Arkansas City, KS </td>
        <td>316-447-3355 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.srcadragstrip.com/" target="_blank">SRCA Dragstrip </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Great 
            Bend, Kan. </td>
        <td>(620) 792-5079 </td>
        <td>formerly Great Bend Motorplex? Compulink. 
            Elevation: 1900' </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.kidragway.com/">Kansas International Dragway</a> (formerly <a href="http://www.teamwir.com/" target="_blank">Wichita International 
                Raceway) </a>1/4 Mile 
                NHRA 
                Div. 5 </p></td>
        <td>7800 W. 61st N.
            
            Maize, Ks. 67101 </td>
        <td>(316) <font size="1" face="Arial" color="#000000">729-2882 </td>
        <td>Accutime (new in '05) 
            Elevation: 1400' </td>
    </tr>
    <tr>
        <td><a name="KY"></a> Kentucky </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.beechbend.com/" target="_blank">Beech Bend Raceway 
            Park </a>1/4 Mile 
            NHRA 
            Div. 3 </td>
        <td>Bowling 
            Green, Ky. </td>
        <td> (270) 781-7634 </td>
        <td> RACED
            Awesome race track, neat (albeit old) covered stands, some amusement 
            rides. Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.bluegrassracewaypark.com/" target="_blank">Bluegrass 
            Raceway Park </a>1/8 Mile 
            IHRA 
            Div. 3 </td>
        <td>Owingsville, Ky. </td>
        <td> (606) 674-3987 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://lakecumberlanddragway.com/" target="_blank">Lake 
            Cumberland Dragway</a> 1/8 Mile </td>
        <td>Airport 
            Rd
            
            Jamestown, Ky. </td>
        <td> (270) 343-6101 </td>
        <td> RACED (Won $5K No-Box)
            Raced here a few times. May not look like much at first, but quickly 
            became a favorite. SUPER people, excellent PA and radio broadcast. 
            TSI timing, so rollout is about .02 loose. Sunoco fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.londondragway.com/" target="_blank">London Dragway </a>1/8 Mile 
            IHRA 
            Div. 3 </td>
        <td>London, 
            Ky. </td>
        <td> (606) 878-8883 </td>
        <td> Accutime. 
            Elevation: 1200' 
            Track Radio: 101.9FM </td>
    </tr>
    <tr>
        <td><a href="http://www.mpdragway.com" target="_blank"> Mt. Park Dragway</a> 1/4 Mile - 1/8 Mile 
            IHRA 
            Div. 3 </td>
        <td>Jared 
            Kennon &amp; Dallas Clark
            
            718 11th Street 
            Clay City, KY 40312 <a href="mailto:mpdragway@snapp.net">mpdragway@snapp.net</a></td>
        <td> Track:606-663-2344 
            Office:606-663-0369 
            FAX:606-663-0369 </td>
        <td> RACED (Best: R/U)
            Raced IHRA Pro-Am events here. One of the Chrondek timing 
            systems, if I recall. Great people, great working facility. 
            Sunoco fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.ohiovalleydragway.com/" target="_blank">Ohio Valley 
            Dragway </a>1/8 Mile 
            NHRA 
            Div. 3 </td>
        <td>align="left">
            <p> <em style="font-style: normal">Terry Huff 
                632 Katherine Station Road West 
                West Point, KY 40177 </em> </p></td>
        <td><em style="font-style: normal">Track - 
            502-922-4152
            Office - 502-922-0063 </em></td>
        <td> VP Fuel. Nitrous and CO2 available. 
            1650' total shutdown. </td>
    </tr>
    <tr>
        <td><a href="http://www.thornhilldragstrip.com">Thornhill Dragway</a> 1/4 mile </td>
        <td><p class="MsoAddress"><span style=""> 14114 
                Kenton Station Rd 
                Morning View, KY 41063 </span></p></td>
        <td> 859-356-7702 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://us60dragracing.com/">US 60 Dragway</a> 1/8 Mile </td>
        <td><div id="wb-container-outer">
                <div id="wb-container-inner">
                    <div id="wb-layout">
                        <div id="wb-layout-inner">
                            <div id="wb-container-contentwrapper">
                                <div id="wb-layout-main">
                                    <div id="wb-layout-main-inner-1">
                                        <div class="wb_region" id="wb-layout-main-inner-2">
                                            <p align="left" editor_id="mce_editor_0"> <font size="1" face="Arial" editor_id="mce_editor_0"> Hwy. 60 Bypass East 
                                                Hardinsburg, KY 40143 <a href="mailto:jamessnead@yahoo.com" editor_id="mce_editor_0"> <font size="1" face="Arial" editor_id="mce_editor_0"> jamessnead@yahoo.com </a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></td>
        <td><font size="1" face="Arial" editor_id="mce_editor_0">(270) 756-2214 </td>
        <td> TSI RaceNet </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.windyhollowdragway.com/" target="_blank">Windy 
                Hollow Raceway Park </a>1/8 Mile 
                NHRA 
                Div. 3 </p></td>
        <td>Owensboro, Ky. </td>
        <td> (270) 785-4300 </td>
        <td></td>
    </tr>
    <tr>
        <td><a name="LA"></a> Louisiana </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.noproblemraceway.com/" target="_blank">No Problem 
            Raceway </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td>Belle 
            Rose, La. </td>
        <td>(985) 369-3692 </td>
        <td>RACED (Best: WIN in First Visit) 
            Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.thunderroadracewaypark.com/">Thunder Road Raceway 
            Park</a> (Formerly Red River Raceway) 
            1/4 Mile 
            Div. 4 </td>
        <td>13500 
            Gilliam Scott Slough Road
            
            Gilliam, LA 71029 <a href="mailto:info@thunderroadracewaypark.com"> info@thunderroadracewaypark.com</a></td>
        <td>(318) 296-4466 </td>
        <td>VISITED
            Covered the IHRA Div. 4 Bracket Finals here. Former national 
            event track, top of the line. Lots of paved pit area. 
            Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.statecapitoldragway.com/" target="_blank">State 
            Capitol Dragway </a>1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td>Baton 
            Rouge, La. </td>
        <td>(225) 627-4574 </td>
        <td>Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.twincitydragway.com/" target="_blank">Twin City 
            Dragway </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td>Monroe, 
            La. </td>
        <td>(318) 387-8563 </td>
        <td></td>
    </tr>
    <tr>
        <td><a name="ME"></a> Maine</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.oxforddragway.com" target="_blank">Oxford Plains Dragway</a> 1/8 Mile 
            IHRA 
            Div. 3 </td>
        <td>29 Oxford 
            Homes Lane
            
            Oxford, ME 04270 
            Business Address: <font size="1" face="Arial" color="#000000" id="role_document7"> <strong style="font-weight: 400">P.O. Box 234 
            Lisbon Falls, ME 04252</strong><font size="1" face="Arial" color="#000000" id="role_document8">
            <div> <strong style="font-weight: 400">Rich Wilcox </strong></div>
            <div> <strong style="font-weight: 400"> <a href="mailto:richwilcox01@aol.com">richwilcox01@aol.com</a></strong></div></td>
        <td>align="left"> Track:207-689-5201 </td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td><a href="http://www.winterportdragway.com/">Winterport Dragway</a> 1/8th mile
            </p></td>
        <td>Winterport, ME </td>
        <td>align="left"> Track (Race Days Only) ~ (207)223-3998 
            Bob Reynolds, President ~ (207)223-5592
            Alan Jolicoeur, VP~ (207) 453-2348 </td>
        <td>align="left"> Port-A-Tree. </td>
    </tr>
    <tr>
        <td><a name="MD"></a> Maryland </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Aquasco Speedway </td>
        <td><font size="1" face="Arial" color="#808080"> Aquasco, MD <font size="1" face="Arial" color="#808080">Contact: Julio Marra 
            2040 Mazatlan Road 
            Punta Gorda, Florida 33983 <a href="mailto:rcetrak@comcast.net">rcetrak@comcast.net</a></td>
        <td><font size="1" face="Arial" color="#808080"> (301) 351-8990 </td>
        <td><font size="1" face="Arial" color="#808080"> Defunct (Operated: 
            1956-1978)
            Promoter Julio Marra. Track closed in 1978 due to 
            environmental reasons and NHRA sanctions. This track was 
            located in Aquasco, Maryland. Was best known for 
            President Gold Cup Championship. </td>
    </tr>
    <tr>
        <td><a href="http://www.capitolraceway.com/" target="_blank"> Capitol Raceway</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Crofton, 
            MD </td>
        <td> (410) 793-0661 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.cecilcountydragway.com" target="_blank"> Cecil County Dragway</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Rising 
            Sun, MD </td>
        <td> (410) 287-5486 
            (410) 287-6280 </td>
        <td> RACED (Best: R/U)
            Ran NHRA Div. 1 race here in '99. Great facility, slightly 
            downhill, so it's fast. Interesting horseshoe and crossover 
            staging lanes. Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.mirdrag.com" target="_blank"> Maryland International Raceway </a>1/4 Mile 
            IHRA 
            Div. 1 </td>
        <td>Royce &amp; 
            Linda Miller
            
            27861 Budds Creek Rd. 
            Mechanicsville, MD 20659 <a href="mailto:info@mirdrag.com">info@mirdrag.com</a></td>
        <td> Track:301-884-9833 
            Office:301-884-9833 
            FAX:301-884-9878 </td>
        <td> RACED (Best: R/U Nat'l)
            Raced IHRA Nat'l &amp; Div'l events here. One of the best facilities 
            in the country! Hooks like mad. Tight rollout. 
            Compulink. Sunoco fuel </td>
    </tr>
    <tr>
        <td><a href="http://www.masondixondragway.com/" target="_blank"> Mason-Dixon Dragway</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Elmer 
            Wachter
            
            Hagerstown, MD </td>
        <td> (301) 791-5193 </td>
        <td> RACED
            Raced a few bracket races here over the years. Rollout is crazy 
            tight! Scoreboards are at the 1/8th mile. Be careful 
            checking your dial in the left lane, as light poles may block a digit 
            depending on where you are! Downhill, so this place is fast! 
            Sun sets behind you. Compulink. </td>
    </tr>
    <tr>
        <td><font color="#808080"> <a href="http://www.75-80dragway.com/" target="_blank">75-80 Dragway</a> 1/4 Mile 
            IHRA </td>
        <td><font face="Arial"> <font size="1"> 11210 Fingerboard Road 
            Monrovia, Maryland 21770 <a href="mailto:info@75-80dragway.com"> <font size="1">info@75-80dragway.com </a></td>
        <td><span class="text"> 301-253-4670 
            301-253-6535 (FAX) </span></td>
        <td> Re-opened 2009! <font color="#808080">Closed - Oct 2005 </td>
    </tr>
    <tr>
        <td><a name="MI"></a> Michigan</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.lapeerdragway.com/"> Lapeer International Dragway</a></td>
        <td>Lapeer, MI </td>
        <td> 810-664-4772 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.midmichmotorplex.com/" target="_blank"> Mid-Michigan Motorplex </a>Div. 3 
            1/4 Mile 
            NHRA </td>
        <td>Stanton, 
            Mich. </td>
        <td> (989) 762-5043 </td>
        <td> RACED (Best: $2K semi)
            Ran the $15K No-Box here in '05. Beautiful facility. 
            Smooth grass pits, some paved pits, long staging lanes. 
            Compulink. Sunoco fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.milandragway.com/" target="_blank"> Milan Dragway </a>1/4 Mile 
            NHRA 
            Div. 3 </td>
        <td>Bill 
            Kapolka
            
            10860 Plank Road 
            Milan, MI 48160 <a href="mailto:info@milandragway.com">info@milandragway.com</a></td>
        <td> Track:734-439-7368 
            Office:734-439-7368 
            FAX:734-439-2617 </td>
        <td> RACED (Best: R/U Nat'l)
            IHRA National event facility. Includes a GM test track. 
            TSI timing. Slight difference lane to lane. Sunoco fuel </td>
    </tr>
    <tr>
        <td><a href="http://www.ublydragway.com/"> Northern Mich. Dragway</a> 1/8 mile </td>
        <td>Kaleva, 
            MI </td>
        <td> 616-362-3439 </td>
        <td> Chrondek. </td>
    </tr>
    <tr>
        <td>West 
            Michigan Sand Dragway 
            
            (formerly Silver Lake Sanddragway ) </td>
        <td>Owned by 
            Jim &amp; Judi Briggs.
            
            Located in Silver Lake Sand Dunes Area. </td>
        <td></td>
        <td> Modern Restrooms, Full concessions, 
            Sanctioned 300 ft. sand track. Home of the Summer U.S. Sand 
            nationals. </td>
    </tr>
    <tr>
        <td><a href="http://www.ublydragway.com/">Ubly 
            Dragway</a> 1/8 mile
            </p></td>
        <td>Ubly, MI </td>
        <td> 517-658-2331 </td>
        <td> TSI RaceNet. </td>
    </tr>
    <tr>
        <td><a href="http://www.us131motorsportspark.com/" target="_blank"> US 131 Motorsports Park </a>1/4 Mile 
            IHRA 
            Div. 3 </td>
        <td>Jason 
            Peterson
            
            1249 12th St. 
            Martin, MI 49070 <a href="mailto:jason@us131motorsportspark.com"> jason@us131motorsportspark.com</a></td>
        <td> Track:269-672-7800 
            Office:269-672-7800 
            FAX:269-672-9194 </td>
        <td> RACED (Best: Won IHRA Nitro Jam)
            Table-top smooth, and one of the best-hooking tracks I've been to! 
            Nice bright digital scoreboards. Large, modern restrooms. TSI 
            RaceNet </td>
    </tr>
    <tr>
        <td><a name="MN"></a> Minnesota </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.brainerdracewayandresort.com/" target="_blank"> Brainerd International Raceway </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Brainerd, 
            Minn. </td>
        <td>(218) 824-7220 </td>
        <td>Compulink. 
            Elevation: 1300' </td>
    </tr>
    <tr>
        <td><a href="http://www.grovecreek.com/" target="_blank"> Grove Creek Raceway </a>1/8 Mile 
            NHRA 
            Div. 5 </td>
        <td>Grove 
            City, Minn. </td>
        <td>(320) 857-2152 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.topenddragways.com/">Top End Dragways</a> (formerly Interstate 
                Dragways) 
                1/4 Mile 
                NHRA 
                Div. 5 </p></td>
        <td><div id="outerWrapper">
                <div id="gradient">
                    <div id="contentWrapper">
                        <div style="zoom: 1" id="content0"> 4147 70th St SW 
                            Glyndon MN, 56547 <a href="mailto:charlie@topendperformanceinc.com"> charlie@topendperformanceinc.com</a> </div>
                    </div>
                </div>
            </div></td>
        <td>(218) 236-9461 </td>
        <td></td>
    </tr>
    <tr>
        <td><p><font size="1" face="Arial" color="#808080"> Iron Range Raceway * </p></td>
        <td><font size="1" face="Arial" color="#808080"> Keewatin, MN </td>
        <td></td>
        <td><font size="1" face="Arial" color="#808080"> * Defunct </td>
    </tr>
    <tr>
        <td><a name="MS"></a> Mississippi</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.gulfportdragway.com/">Gulfport Dragway</a></td>
        <td>17085 
            Race Track Rd. <span lang="en-us"> Gulfport, 
            Mississippi 39503 </span></td>
        <td> (228) 863-4408 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.hollyspringsdragstrip.com/">Holly Springs Dragstrip</a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td>159 Old 
            Hwy 7 South
            
            Holly Springs, MS 38635 </td>
        <td> Jabbo:(901) 830-0179 
            Track: (662) 252-5600 </td>
        <td> All concrete racing surface. 20 acres paved 
            parking. Some electric hookups. 40 miles from Memphis, Exit 30 on US Hwy 
            78 </td>
    </tr>
    <tr>
        <td><a href="http://www.hubcitydragway.com" target="_blank"> Hub City Dragway </a>1/8 Mile 
            IHRA 
            Div. 4 </td>
        <td>Ralph 
            Abraham
            
            331 Eatonville Rd. 
            Hattiesburg, MS 39401 <a href="mailto:hubdrag@aol.com">rabraham@comcast.net</a></td>
        <td> Track:601-307-3724 
            Office:601-268-0133 
            FAX:601-268-1588 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.jxndragway.com/" target="_blank">Jackson Dragway 
                Park </a>1/4 Mile 
                NHRA 
                Div. 4 </p></td>
        <td>Jackson, 
            Miss. </td>
        <td> (601) 372-5506 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.mississippimotorsportspark.com/">Mississippi 
            Motorsports Park</a> (Formerly
            Tuscola Motorsports) 
            1/4 Mile </td>
        <td>Jerry 
            McDaniel <font size="1" face="Arial" color="#000000"> 624 Hwy 487 W 
            Lena, MS 39094 </td>
        <td><font size="1" style="font-family: Arial; font-weight: normal"> 601-622-0124 </td>
        <td> Also known as Lake Slipaway. Compulink. </td>
    </tr>
    <tr>
        <td><a name="MO"></a> Missouri</td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td><a href="http://www.kcironline.com/" target="_blank"> Kansas City International Raceway </a> 1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td> Kansas 
            City, Mo. </td>
        <td> (816) 358-6700 </td>
        <td> CLOSED 
            Compulink. <a href="http://race2thefuture.org/">Race To The Future Project</a></td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Interstate 55 Dragway 
            1/8 mile </td>
        <td><font size="1" face="Arial" color="#808080"> Pevely, MO </td>
        <td><font size="1" face="Arial" color="#808080"> 314-479-3219 </td>
        <td><font size="1" face="Arial" color="#808080">DEFUNCT (Dirt track 
            and go-kart tracks remain open) </td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Mid-America Raceways </td>
        <td><font size="1" face="Arial" color="#808080"> Wentzville, MO </td>
        <td><font size="1" face="Arial" color="#808080"> 314-673-2434 
            314-639-6465 </td>
        <td><font size="1" face="Arial" color="#808080">DEFUNCT </td>
    </tr>
    <tr>
        <td><a href="http://www.mokandragway.com/"> Mo-Kan Dragway</a></td>
        <td>Asbury, Mo </td>
        <td> 417-642-5599 track 
            918-836-7032 weekdays </td>
        <td> Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.ozarkracewaypark.com/">Ozark Raceway Park</a> (Formerly Ozark International Raceway) 
            IHRA 
            Div. 4 </td>
        <td>8529 
            State Hwy U
            
            Rogersville, MO 65742 </td>
        <td> Phone: 417-738-2222 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://sikestondragstrip.us/">Sikeston Dragstrip</a></td>
        <td>389 
            Sandywood Ln.
            
            Sikeston,MO63801 <a href="mailto:info@sikestondragstrip.us" style="color: #0000FF"> info@sikestondragstrip.us</a></td>
        <td> 573-471-9099 
            609-879-1511 </td>
        <td></td>
    </tr>
    <tr>
        <td>height="47"<a href="http://www.us36raceway.com/">US 36 Raceway</a> 1/8 Mile </td>
        <td>height="47" 9850 S. 
            Hwy 33 
            Osborn, MO 64474 <a href="mailto:us36race@us36raceway.com">us36race@us36raceway.com</a></td>
        <td>height="47" bgcolor="#ffffff" align="left"> (816) 675-2279 </td>
        <td>height="47" bgcolor="#ffffff" align="left"><font size="1" face="Arial, Arial, Helvetica">
            <p align="left"><font size="1" face="Arial, Arial, Helvetica">concrete launch pad and dual score boards. Sunoco 
                Racing Fuel and Alcohol available </p></td>
    </tr>
    <tr>
        <td><a name="MT"></a> Montana</td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td><a href="http://www.pcmotorsports.org"> Philips County Motorsports Dragstrip </a> 1/8 Mile 
            IHRA 
            Div. 6 </td>
        <td><font size="1" face="Arial" color="#000000">HWY 191 South of Malta 
            West of National Guard 1320 Power Lane 
            
            P.O. Box 31 
            Malta, MT 59538 <a href="mailto:greg.k@pcmotorsports.org">greg.k@pcmotorsports.org</a></td>
        <td> John Carnahan, Manager 
            (406) 654-1755 </td>
        <td> Lowest elevation track in MT at 2200 ft. 
            480 feet of concrete after the starting line. </td>
    </tr>
    <tr>
        <td><font color="#808080"> <a href="http://www.lewistowndragracing.com" target="_blank">Lewistown 
            Raceway</a> 1/4 Mile 
            NHRA 
            Div. 6 </td>
        <td>Lewistown, Mont. </td>
        <td> (406) 350-0733 </td>
        <td> Montana's oldest dragstrip, celebrating its 
            50th anniversary in 2007. </td>
    </tr>
    <tr>
        <td><a href="http://www.lostcreek-raceway.com/" target="_blank">Lost Creek 
            Raceway </a>1/8 Mile 
            NHRA 
            Div. 6 </td>
        <td>Anaconda, 
            Mont. </td>
        <td> (406) 949-3724 </td>
        <td> Chrondek. 
            Elevation: 5100' </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.rockymountaindragway.com/">Rocky Mountain Dragway</a> (formerly Yellowstone Dragstrip) 
                1/4 Mile 
                NHRA 
                Div. 6 </p></td>
        <td>Track 
            Location:
            
            8405 Raceway Ln 
            Acton, MT 59002
            <p> Mailing Location: 
                P.O. Box 30784 
                Billings, MT 59107 </p></td>
        <td> (406) 530-9737 </td>
        <td> formerly Intermountain Motorsports. Compulink. </td>
    </tr>
    <tr>
        <td><a name="NE"></a> Nebraska</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.krpi.com/" target="_blank"> Kearney Raceway Park </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Al Simmons
            
            Kearney, Neb. <a href="mailto:asimmons@krpi.com">asimmons@krpi.com</a></td>
        <td>308-750-2049 </td>
        <td>New Compulink 2008 new lighting down 
            track(submitted by Carl Peterson) 
            Track Radio: <strong style="font-weight: 400"><span class="style7">91.5 
            FM</span></strong></td>
    </tr>
    <tr>
        <td><p align="left"><font size="1" face="Arial" color="#808080"> Nebraska 
                Motorplex 
                1/4 Mile 
                NHRA 
                Div. 5 </p></td>
        <td><font size="1" face="Arial" color="#808080">Scribner, Neb. </td>
        <td><font size="1" face="Arial" color="#808080">(402) 664-3364 </td>
        <td><font size="1" face="Arial" color="#808080"> DEFUNCT
            Compulink. 
            Elevation: 1400' </td>
    </tr>
    <tr>
        <td><a name="NV"></a> Nevada </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.lvms.com/" target="_blank">The Strip at Las Vegas 
            Motor Speedway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Las 
            Vegas, Nev. </td>
        <td>(702) 623-8459 </td>
        <td>Compulink. 
            Elevation: 2100' 
            Track Radio: 103.9FM </td>
    </tr>
    <tr  >
        <td><a href="http://www.topgunraceway.com/" target="_blank">Top Gun Raceway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td><p align="left"> Fallon, Nev. </p></td>
        <td> (800) 325-7448 </td>
        <td> Crondek. 
            Elevation: 4000' </td>
    </tr>
    <tr  >
        <td><a name="NH"></a> New Hampshire </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.newenglanddragway.com" target="_blank"> New England Dragway </a>1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Larry 
            Bean
            
            P.O. Box 1320 
            Epping, NH 03042 <a href="mailto:info@newenglanddragway.com">info@newenglanddragway.com</a></td>
        <td> Track:603-679-8001 
            Office:603-679-8001 
            FAX:603-679-1955 </td>
        <td> RACED  
            Beautiful facility. and *fast*. Watch the sun late in the day. 
            Compulink. </td>
    </tr>
    <tr  >
        <td><a name="NM"></a> New Mexico </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr  >
        <td><a href="http://www.abqdragway.com/" target="_blank"> Albuquerque National Dragway </a>1/4 Mile 
            NHRA 
            Div. 7 </td>
        <td>Albuquerque, N.M. </td>
        <td> (505) 873-2684 </td>
        <td></td>
    </tr>
    <tr  >
        <td><a href="http://www.area51dragway.com/" target="_blank">Area 51 Dragway </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td>Roswell, 
            N.M. </td>
        <td> (505) 627-3968 </td>
        <td> TSI Timing System, Infareds w/ plans to go to an LED tree with 
            crosstalk. Track elevation is 3500’. The pit area, shut down and 
            return road has been recently repaved with “slurry” seal. Full 
            facilities with concession stand. (contributed by Johnny Teel, 10/06) </td>
    </tr>
    <tr  >
        <td>Carlsbad Dragway 
            
            1/8 Mile </td>
        <td>Keith 
            Lewis
            
            Carlsbad, N.M. <a href="mailto:carlsbaddragway@hotmail.com"> carlsbaddragway@hotmail.com</a></td>
        <td> 505-887-8602 </td>
        <td> Chrondek C-33 (with custom upgrades) 
            Upgrades scheduled: 1/4 Mile, restrooms, concessions </td>
    </tr>
    <tr  >
        <td><p align="left"> <a href="http://www.hobbsmotorsportspark.com/" target="_blank">Hobbs 
                Motorsports Park </a>1/4 Mile 
                NHRA 
                Div. 4 </p></td>
        <td>Hobbs, 
            N.M. </td>
        <td> (505) 391-8283 </td>
        <td></td>
    </tr>
    <tr  >
        <td><a name="NJ"></a> New Jersey </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr  >
        <td><a href="http://www.atcorace.com/" target="_blank"> Atco Raceway</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Joe Sway
            
            Atco, NJ </td>
        <td> (856) 768-0900 
            (856) 768-2167 </td>
        <td> RACED  
            Lots of paved pit, but also some sandy areas, so make sure your wheel 
            wells are clean. Unique drive-through waterbox walls, with 'garage 
            doors'. Good working track with great programs. Fiber-Optic Compulink. </td>
    </tr>
    <tr  >
        <td><a href="http://www.islanddragway.com" target="_blank"> Island Dragway</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Tony 
            Demarky
            
            Great Meadows, NJ </td>
        <td> (908) 637-6060 </td>
        <td> RACED (Best: semi)  
            Neat little home-grown track. 60' clocks are probably a little 
            short, as is the shutdown. Home to tons of hitters.Compulink. </td>
    </tr>
    <tr  >
        <td><a href="http://www.etownraceway.com/" target="_blank"> Old Bridge Township Raceway Park</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Englishtown, NJ </td>
        <td> (732) 446-7800 </td>
        <td> RACED  
            Nothing but pavement! Also has an 1/8th mile track running 
            parallel to 1/4-mile shutdown area for T&amp;T, Trophy, and Licensing. 
            Compulink. </td>
    </tr>
    <tr  >
        <td><a name="NY"></a> New York </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.dunntireracewaypark.com/">Dunn Tire Raceway Park</a> (Formerly Lancaster Motorsports Park)<a href="http://lakecumberlanddragway.virtualave.net/" target="_blank"> </a>1/8 Mile<a href="http://www.lancasterracing.com/" target="_blank"> </a>IHRA </td>
        <td><div> Mailing: 
                Tony Galluzzi  
                PO Box 708 
                Cheektowaga, NY 14225 
                
                Physical Location: </div>
            <div> 57 Gunnville Road  
                Lancaster, NY 14086 </div></td>
        <td> Track:716-759-6818 
            Office:716-759-6818 
            FAX:716-759-6008 </td>
        <td> VISITED  
            Been past it... sits right off I-90 south of Buffalo. </td>
    </tr>
    <tr>
        <td><a href="http://www.estadrags.com/" target="_blank"> ESTA Safety Park</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Cicero, 
            NY </td>
        <td> (315) 622-4348 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.dragway.com/" target="_blank"> Lebanon Valley Dragway</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>West 
            Lebanon, NY </td>
        <td> (518) 794-7130 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.spencerdrags.freewebspace.com/" target="_blank"> Spencer Speedway</a> 1/8 Mile 
            NHRA 
            Div. 1 </td>
        <td>Williamson, NY </td>
        <td> (315) 589-3018 </td>
        <td> formerly Williamson Int'l Speedway </td>
    </tr>
    <tr>
        <td><p><font size="1" face="Arial" color="#808080"> Long Island Motorsports 
                Park* 
                1/4 mile </p></td>
        <td><font size="1" face="Arial" color="#808080"> West Hampton, NY </td>
        <td></td>
        <td><font size="1" face="Arial" color="#808080"> Defunct. Famous for bringing the "Dream Team" to the NHRA Div. 1 
            Bracket Finals, stacked with the likes of Biondo, Harrington, Morehead, 
            et.al. </td>
    </tr>
    <tr>
        <td><a href="http://www.nyirp.com" target="_blank"> New York Int'l Raceway Park</a><a href="http://www.akracewaypark.com" target="_blank"> </a>1/4 Mile 
            IHRA 
            Div. 3 </td>
        <td>Bob &amp; 
            Donna Metcalfe
            
            P.O. Box 296 
            Leicester, NY 14481 <a href="mailto:nyirp@aol.com">nyirp@aol.com</a></td>
        <td> Track:585-382-3030 
            Office:585-382-9061 
            FAX:585-382-9061 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.skyviewdrags.com" target="_blank"> Skyview Drags</a> 1/8 Mile 
            IHRA 
            Div. 3 </td>
        <td>George 
            Swansborgh
            
            114 Skyline Dr 
            Tiogo Center, NY 13845 </td>
        <td> Track: 607-687-3744 
            Fax: 607-687-2505 </td>
        <td> RACED  
            Neat little family-friendly facility built on top of a mountain, just 
            west of Binghamton. TSI. </td>
    </tr>
    <tr  >
        <td><a name="NC"></a> North Carolina </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.race710dragstrip.com/">710 Dragstrip</a> 1/8 Mile </td>
        <td><font face="Arial"><font size="1"> 3118 NC Highway 710 S 
            Rowland, NC <font size="1"> <a href="mailto:sascott@aol.com"> uneek@race710dragstrip.com</a></td>
        <td> (910) 422-3236 </td>
        <td></td>
    </tr>
    <tr>
        <td>Brewer's Speedway
            
            1/8th mile </td>
        <td>Sharpsburg, NC </td>
        <td> 919-446-2631 </td>
        <td></td>
    </tr>
    <tr>
        <td>Canton 
            Motorsports Park
            
            IHRA </td>
        <td>Steve 
            Michael </td>
        <td> Phone: (828)235-8725 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.coastalplainsraceway.com/" target="_blank">Coastal 
                Plains Dragway </a>1/4 Mile 
                IHRA 
                Div. 9 </p></td>
        <td>Jacksonville, N.C. </td>
        <td> (910) 347-2200 </td>
        <td> RACED 
            Great working track with great staff. Pits on the left, return 
            road on the right. Get timeslip at base of the tower and back to the 
            pits. Incandescent bulbs, loose rollout. </td>
    </tr>
    <tr>
        <td><a href="http://www.dunnbensondragstripinc.com" target="_blank">Dunn-Benson Dragstrip</a> 1/8 Mile 
            IHRA 
            Div. 9 </td>
        <td>Track 
            (Physical) Address: 
            
            555 Dragstrip Road 
            Benson, NC 27504 
            Mailing Address:  
            6275 Hwy. 50 North suite 117 
            Benson, NC 27504 </td>
        <td> Track:919-894-1662 
            Office:919-894-4870 or 919-894-8502 
            FAX:910-893-3244 </td>
        <td> RACED (Best: R/U) 
            Accutime (new in '05). Track ground, shutdown lengthened, 
            wide turnoff, built scale house and many more improvements in 2010. </td>
    </tr>
    <tr>
        <td><a href="http://www.farmingtonmotorsportspark.com" target="_blank"> Farmington Motorsports Park</a> 1/8 Mile 
            IHRA 
            SWC 1 
            Pro-Am 9 </td>
        <td>Everette 
            Justice
            
            2962 Hwy. 801N. 
            Farmington, NC 27028 <a href="mailto:info@farmingtonmotorsportspark.com"> info@farmingtonmotorsportspark.com</a></td>
        <td> Track:336-998-3443 
            Office:336-399-1978 
            FAX:336-998-3443 </td>
        <td> RACED (Best: WIN) 
            Nice layout, great spectator track. Tons of pit area. Drainage 
            system for waterbox. Chrondek. Sunoco fuels (including 100 
            unleaded) </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.fayettevillemotorsportspark.com/" target="_blank"> Fayetteville Motorsports Park </a>1/4 Mile 
                IHRA 
                Div. 9 </p></td>
        <td>Fayetteville, N.C. </td>
        <td> (910) 484-3677 </td>
        <td> RACED (Best: Semi) 
            Formerly Cumberland Dragway. Accutime. </td>
    </tr>
    <tr>
        <td><div style="font-size: 12pt; font-family: 'times new roman', 'new york', times, serif; margin: 0px">
                <div style="margin: 0px"> <a href="http://www.harrellsraceway.net/">The New Harrells 
                    Raceway</a> </div>
                <div style="margin: 0px"> 1/8 mile </div>
            </div></td>
        <td><div style="margin: 0px"> Eddie Yarborough </div>
            <div style="margin: 0px"> 3030 Buckhorn Rd </div>
            <div style="margin: 0px"> Harrells NC 28444 </div>
            <div style="margin: 0px"> <a href="mailto:mail@harrellsraceway.net">mail@harrellsraceway.net</a> </div></td>
        <td> (910) 532-2363 </td>
        <td> Chrondek </td>
    </tr>
    <tr>
        <td><a href="http://www.kdsmotorsports.com/" target="_blank"> Kinston Drag Strip</a><a href="http://www.keystoneraceway.net/" target="_blank"> </a>1/8 Mile 
            IHRA 
            Div 9 </td>
        <td>Bobby 
            Smith
            
            2869 Hull Road 
            Kinston, NC 28501 <a href="mailto:info@kdsmotorsports.com">info@kdsmotorsports.com</a></td>
        <td> Track:252-527-4337 
            Office:252-522-9551 
            FAX:252-522-4829 </td>
        <td> RACED (Best: WIN)  
            Narrow track &amp; groove, but worked great when I was there for the IHRA 
            Pro-Am in '06. Nice folks. Chrondek. </td>
    </tr>
    <tr>
        <td><a href="http://www.mooresvilledragway.com/" target="_blank"> Mooresville Dragway </a>1/8 Mile 
            IHRA 
            Div 9 </td>
        <td>8415 Hwy. 152 W.
            
            Mooresville, NC 28115<a href="mailto:dragway@mooresvilledragway.com"> dragway@mooresvilledragway.com</a></td>
        <td> Track:704-663-4685 
            Office:704-857-9364 </td>
        <td> RACED (Best: WIN)  
            Narrow track, but one that really works great. Left lane is just 
            a bit tighter and slower. Chrondek. </td>
    </tr>
    <tr>
        <td><a href="http://www.newbernmotorsports.com">New Bern MotorSports</a> 1/8 Mile </td>
        <td>300 
            Dragstrip Rd
            
            New Bern, NC 28560 </td>
        <td> Track/Office: 252-637-7701 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.northeastdragway.com" target="_blank"> Northeast Dragway</a> 1/8 Mile 
            IHRA 
            Div. 9 </td>
        <td>Tony 
            Trueblood
            
            Rt. 2 Box 1099 Lake Rd. 
            Hertford, NC 27944 </td>
        <td> Track:252-264-2066 
            Office:252-264-2066 </td>
        <td></td>
    </tr>
    <tr>
        <td><span class="905441119-28092004"><a href="http://www.outerbanksspeedway.com" target="_blank"> Outer Banks Speedway</a> 1/8 Mile </span>IHRA <span class="905441119-28092004"> Div. 9</span></td>
        <td><div style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Andy Hambright 
                426 Benson Road 
                Creswell, NC 27928<a href="mailto:info@outerbanksspeedway.com" title="mailto:info@outerbanksspeedway.com"> info@outerbanksspeedway.com</a> </div></td>
        <td><span class="905441119-28092004"> Track: 252-797-7223</span></td>
        <td> TSI. </td>
    </tr>
    <tr>
        <td><a href="http://www.piedmontdragway.com" target="_blank"> Piedmont Dragway </a>1/8 Mile 
            IHRA 
            Div. 9 </td>
        <td>Gilmer 
            Hinshaw
            
            6750 Holt Store Rd. 
            Greensboro, NC 27283 <a href="mailto:info@piedmontdragway.com">info@piedmontdragway.com</a></td>
        <td> Track:336-449-7411 
            Office:336-449-7411 
            FAX:366-449-7074 </td>
        <td> RACED (Best: Win)  
            All concrete 1/8th mile. Home to the Big Dog Shootout. 
            Compulink. Sunoco fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.rockinghamdragway.com" target="_blank"> Rockingham Dragway</a><a href="http://lakecumberlanddragway.virtualave.net/" target="_blank"> </a>1/4 Mile 
            IHRA 
            Div. 9 </td>
        <td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Steve Earwood 
                2153 Highway U.S. 1 North 
                Rockingham, NC 28379 <a href="mailto:info@rockinghamdragway.com">info@rockinghamdragway.com</a> </p></td>
        <td> Track:910-582-3400 
            Office:910-582-3400 
            FAX:910-582-8667 </td>
        <td> RACED (Best: Win, '07 Track Champ, '09 Spring Nationals Champ. '10 
            Spring Nitro Jam Champ)  
            National event facility. When it's good, it's really good. 
            Be careful, though. Watch for sand spurs in the 'back nine', and 
            try not to drag sand to the starting line. Compulink. VP 
            fuel </td>
    </tr>
    <tr>
        <td><a href="http://www.roxboromotorsports.com/" target="_blank"> Roxboro Motorsports Dragway</a> 1/8 Mile 
            IHRA 
            Div. 1 </td>
        <td>AJ Ware
            
            Roxboro, NC <a href="mailto:info@roxboromotorsports.com"> info@roxboromotorsports.com</a></td>
        <td> Track:336-364-3724 
            Office:336-599-7459 
            FAX:336-599-0620 </td>
        <td> RACED (Best: Win)  
            Great working facility and well managed, by Alan Carpenter. 
            Surprisingly, this track is just as wide as Rockingham! 
            Chrondek. Sunoco Fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.shadysidedragway.com/"> ShadySide Dragway</a> 1/8th mile </td>
        <td>Boiling Springs, NC </td>
        <td> 704-434-7313 </td>
        <td></td>
    </tr>
    <tr>
        <td>Thunder Valley Dragway
            
            1/8 mile </td>
        <td>1505 Old 
            Lowery Rd
            
            Red Springs, NC </td>
        <td> 910-843-2934 Track 
            910-369-3132 Office </td>
        <td> RACED 
            This independent track was built in 1983. Surface is good. 
            Famous for fried chicken sandwiches. Chrondek. </td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.wcsdragracing.com/"> Wayne County Dragstrip</a> 1/8 mile
            </p></td>
        <td>Pikeville, NC </td>
        <td> 919-242-8100 </td>
        <td> Chrondek C44 </td>
    </tr>
    <tr>
        <td><a href="http://www.wilkesbororacewaypark.com" target="_blank"> Wilkesboro Raceway Park </a>1/8 Mile 
            IHRA 
            Div. 1 </td>
        <td>Danny 
            Dunn
            
            774 Dragway Road 
            Wilkesboro, NC 28697 <a href="mailto:ddunn@wilkesbororacewaypark.com"> ddunn@wilkesbororacewaypark.com</a></td>
        <td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Phone: 336-973-7223 
                Fax: 336-973-7250 </p></td>
        <td> TSI. </td>
    </tr>
    <tr>
        <td><a href="http://www.lowesmotorspeedway.com/dragway/" target="_blank"> zMax Dragway</a><a href="http://lakecumberlanddragway.virtualave.net/" target="_blank"> </a>1/4 Mile 
            NHRA 
            Div. 2 </td>
        <td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> 5555 Concord Pkwy South 
                Concord, NC 28027</p></td>
        <td> Track: 704-455-3200 
            Office: 704-455-3200 </td>
        <td> RACED 
            4-lane capable. Fiber-Optic Compulink. </td>
    </tr>
    <tr  >
        <td><a name="ND"></a> North Dakota </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="mailto:aerodrag@ndak.net">Aero 
            Drag Racing</a> 1/8 mile
            </p></td>
        <td>Harvey, ND </td>
        <td> 701-324-4481 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.magiccityinternationaldragway.com/">Magic City 
            International Dragway</a> (Formerly Dakota Flat Track) 
            1/8 Mile 
            IHRA 
            Div. 6 </td>
        <td><div class="Section1">
                <p class="MsoNormal"> <span style="font-family: Arial"> Physical Location: </span>North Dakota State Fairgrounds  
                    2005 Burdick Expressway East  
                    Minot, ND 58701 <span style="font-family: Arial"> Mailing Address:  
                    P.O Box 3263 
                    Minot, ND 58702 <a href="mailto:MCIDdragway@yahoo.com">MCIDdragway@yahoo.com</a></span> </p>
            </div></td>
        <td> Track:701-833-8465 </td>
        <td> New Compulink system. 
            VP Fuel. </td>
    </tr>
    <tr  >
        <td><a name="OH"></a> Ohio </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Brush Run </td>
        <td><font size="1" face="Arial" color="#808080"> Brush Run, OH </td>
        <td></td>
        <td><font size="1" face="Arial" color="#808080"> Defunct </td>
    </tr>
    <tr>
        <td><a href="http://www.dragway42.com" target="_blank"> Dragway 42 </a>1/4 Mile 
            IHRA 
            Div. 3 </td>
        <td>Jack &amp; 
            Toby Ehrmantraut
            
            P.O. Box 816 
            West Salem, OH 44287-0816 <a href="mailto:support@dragway42.com">support@dragway42.com</a></td>
        <td> Track:419-853-4242 
            Office:419-886-4284 </td>
        <td> RACED (Best: WIN)  
            Back-in waterbox is nice. Rollout is a bit loose: TSI RaceNet timing. Citgo fuel. 
            Elevation: 1100' 
            Track Radio: 90.1FM </td>
    </tr>
    <tr>
        <td><a href="http://www.edgewaterrace.com/" target="_blank">Edgewater Sports 
            Park </a>1/4 Mile 
            NHRA 
            Div. 3 </td>
        <td>Cleves, 
            Ohio </td>
        <td> (513) 353-4666 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.kilkareraceway.com/" target="_blank">Kil-Kare 
            Dragway </a>1/4 Mile 
            NHRA 
            Div. 3 </td>
        <td>Xenia, 
            Ohio </td>
        <td> (937) 426-2764 </td>
        <td> TSI RaceNet. </td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.kddragway.com/"> KD Dragway</a> 1/8 Mile </td>
        <td>11902 
            State Route 140
            
            South Webster.Oh 45682<a href="mailto:kddragway@direcway.com"> kddragway@direcway.com</a></td>
        <td> 740-778-2594 </td>
        <td> Repaved in 2004. Straight up "The Hill" after the finish line. </td>
    </tr>
    <tr>
        <td><a href="http://www.mcir.com/" target="_blank">Marion County Int'l 
            Raceway</a> 1/4 Mile 
            IHRA 
            Div. 3 </td>
        <td>Mark 
            Primavera
            
            2454 Richwood-LaRue Rd. 
            LaRue, OH 43332 <a href="mailto:mark@mcir.com">mark@mcir.com</a></td>
        <td> Track:740-499-3666 
            Office:740-499-3666 
            FAX:740-499-2185 </td>
        <td></td>
    </tr>
    <tr>
        <td><font color="#808080"> <a target="_blank" href="http://www.magnoliadragstrip.com/"> Magnolia Dragstrip</a> 1/8 mile </td>
        <td>Bruce &amp; 
            Dixie McCreery
            
            5910 Westbrook St 
            Magnolia, Ohio 44643 <font size="1" face="Arial" color="#808080"> <a href="mailto:mccreery3113@aol.com">mccreery3113@aol.com</a></td>
        <td> Ph. 330-866-1500 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.nationaltrailraceway.com/" target="_blank">National 
            Trail Raceway </a>1/4 Mile 
            NHRA 
            Div. 3 </td>
        <td>Columbus, 
            Ohio </td>
        <td> (740) 928-5706 </td>
        <td> RACED (Best: Rnd 5) 
            Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.pacemakersdragway.com/" target="_blank">Pacemakers 
            Dragway Park </a>1/8 Mile 
            NHRA 
            Div. 3 </td>
        <td>Mount 
            Vernon, Ohio </td>
        <td> (740) 397-2720 </td>
        <td> RACED (Best: SEMI) 
            TSI timing. Sunoco fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.quakercitymotorsportspark.com/" target="_blank"> Quaker City Motorsports Park</a> 1/4 Mile 
            IHRA 
            Div. 3 </td>
        <td>Norm Fox
            
            10359 West South Range Rd. 
            Salem, OH 44460 </td>
        <td> Track:330-324-7188 </td>
        <td> RACED (Best: WIN)  
            Great facility, with over 300 electric hookups available. 
            On-site stores for parts, tires, chassis setup. Paint/Body shop 
            and Chassis shop on-site. Unique uphill burnout, and uphill from 
            1000' on. Compulink, but rollout is .02-.03 loose. Union 76 
            fuel. Elevation: 1280' </td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.steelvalleydragway.com/"> Steel Valley Dragway</a> 1/8 Mile </td>
        <td>Smithfield, OH
            
            330-673-6953 </td>
        <td></td>
        <td> RACED  
            Formerly Friendship Park Raceway. Mom &amp; Pop place. Small 
            staff, pits are rough in areas, and the left lane crowns to the left, 
            BUT all in all, a nice place. Good people, and they really take 
            care of the starting line. No lights, so will curfew at dark. 
            Have not raced here under the new ownership, which took over in 2004. </td>
    </tr>
    <tr>
        <td><a href="http://www.norwalkraceway.com" target="_blank"> Summit Racing Equipment Motorsports Park (Norwalk) </a>1/4 Mile 
            NHRA 
            Div. 3 </td>
        <td>Bill 
            Bader, Jr.
            
            1300 St. Rt. 18 
            Norwalk, OH 44857-9548 <a href="mailto:comments@norwalkraceway.com"> comments@norwalkraceway.com</a></td>
        <td> Track:419-668-5555 
            Office:419-668-5555 
            FAX:419-663-0502 </td>
        <td> RACED (Won IHRA Div. 3 Bracket Finals Gambler's)  
            Formerly Norwalk Raceway Park. One of the nicest facilities in 
            the world. Lots of electric hookups, showers, large modern 
            restrooms, and a pound of ice cream for $1! On-site stores for 
            parts, wheels and tires. Sunoco fuel. </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.thompsonracewaypark.com/" target="_blank">Thompson 
                Raceway Park</a> 1/4 Mile 
                IHRA 
                Div. 3 </p></td>
        <td>Thompson, 
            Ohio </td>
        <td> (440) 298-1350 </td>
        <td> RACED (Best: WIN)  
            Some paved pit. Unique crossover bridge with tower centered over 
            the lanes. TSI RaceNet timing, Downhill shutdown. 
            Sunoco fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.tristatedragway.com" target="_blank"> Tri-State Dragway </a>1/4 Mile 
            IHRA 
            Div. 3 </td>
        <td>Bob &amp; Pat 
            Louden
            
            2362 Hamilton Cleves Rd. 
            Hamilton, OH 45013 <a href="mailto:webmaster@tristatedragway.com"> webmaster@tristatedragway.com</a></td>
        <td> Track:513-863-0562 
            Office:513-863-0562 </td>
        <td> RACED  
            Unique layout with separate lanes and a grass median. Box class 
            runs 1/8th mile. Sunoco fuel. </td>
    </tr>
    <tr  >
        <td><a name="OK"></a> Oklahoma </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.ardmoreraceway.com/" target="_blank">Ardmore Dragway</a></td>
        <td>Track Location: 
            
            15793 US Hwy 77 
            Springer, OK 73458 
            
            Mailing Address:  
            3801 Springdale Road 
            Ardmore, OK 73401 </td>
        <td> Track: (580)653-2711 
            Office: (580)226-7811 </td>
        <td></td>
    </tr>
    <tr>
        <td>LA 
            Dragway
            
            1/4 mile </td>
        <td>Geronimo, OK </td>
        <td> 405-357-1948 </td>
        <td></td>
    </tr>
    <tr>
        <td><div> <a href="http://outlawdragway.com"> Outlaw Dragway</a> </div>
            (Formerly Wood Bros. 
            Raceway) 
            1/8 Mile </td>
        <td>6 miles S. of Sallisaw on Hwy 59 south. 
            
            Sallisaw, OK <a href="mailto:firststreettires@yahoo.com"> firststreettires@yahoo.com</a></td>
        <td><p align="left"> 918-650-5778 </p></td>
        <td></td>
    </tr>
    <tr>
        <td>Sooner 
            Motorplex
            
            1/8 Mile 
            IHRA 
            Div. 4 </td>
        <td><p style="FONT-SIZE: 11px" class="MsoNormal"> Terry Zimmerman 
                PHYSICAL ADDRESS: 
                Sooner Motorplex 
                11042 SW Sheridan Road 
                Lawton, OK 73505 
                MAILING ADDRESS: 
                Sooner Motorplex 
                338 SW Qual Springs 
                Cache, OK 73505 </p></td>
        <td> Track phone: 580-695-9648 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.okthunder.com/" target="_blank"> Thunder Valley Raceway Park </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td>Noble, Okla. </td>
        <td> (405) 872-3429 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.tulsaracewaypark.com/" target="_blank"> Tulsa Raceway Park </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Dan Guterman 
                4157 S. Harvard 
                Tulsa, OK 74135 </p></td>
        <td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Track:918-437-7223 
                Office: 918-742-6201 
                Fax: 918-742-6280 </p></td>
        <td> RACED 
            Beautiful, smooth National event facility. Compulink. </td>
    </tr>
    <tr  >
        <td><a name="OR"></a> Oregon </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr  >
        <td><a href="http://www.championraceway.com/" target="_blank"> Champion Raceway</a> (formerly So. Oregon Dragway)<a href="http://www.championraceway.com/" target="_blank"> </a>1/4 Mile 
            NHRA 
            Div. 6 </td>
        <td>Eagle 
            Point, Ore. </td>
        <td> (541) 830-3724 </td>
        <td></td>
    </tr>
    <tr  >
        <td><a href="http://www.scdra.com/" target="_blank"> Coos Bay International Speedway </a>1/8 Mile 
            NHRA 
            Div. 6 </td>
        <td>Coos Bay, 
            Ore. </td>
        <td> (541) 269-2474 </td>
        <td> Compulink. </td>
    </tr>
    <tr  >
        <td><a href="http://communities.msn.com/LakeviewDragstrip" target="_blank"> Lakeview Drag Strip </a>1/8 Mile 
            NHRA 
            Div. 6 </td>
        <td>Lakeview, 
            Ore. </td>
        <td> (541) 947-3293 </td>
        <td> formerly High Desert Motorsports Park </td>
    </tr>
    <tr  >
        <td><a href="http://www.madrasdragstrip.com/" target="_blank"> Madras Dragstrip </a>1/8 Mile 
            NHRA 
            Div. 6 </td>
        <td>Madras, 
            Ore. </td>
        <td> (541) 815-2107 </td>
        <td></td>
    </tr>
    <tr  >
        <td><a href="http://www.supercarsunlimited.com/pirbrackets/" target="_blank"> Portland International Raceway </a>1/8 Mile 
            NHRA 
            Div. 6 </td>
        <td>Portland, 
            Ore. </td>
        <td> (503) 823-7223 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.sospeedway.com/dragstrip.htm"> Southern Oregon Dragway</a> White City, OR 
            1/4 mile
            </p></td>
        <td>White City, OR </td>
        <td> 541-826-7966 </td>
        <td> formerly Jackson County Dragway. TSI. 
            Elevation: 1400' </td>
    </tr>
    <tr  >
        <td><p align="left"> <a href="http://www.woodburndragstrip.com/" target="_blank">Woodburn 
                Dragstrip </a>1/4 Mile 
                NHRA 
                Div. 6 </p></td>
        <td>Woodburn, 
            Ore. </td>
        <td> (503) 982-4461 </td>
        <td> Compulink. </td>
    </tr>
    <tr  >
        <td><a name="PA"></a> Pennsylvania </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.beaversprings.com" target="_blank"> Beaver Springs Dragway</a> 1/4 Mile 
            IHRA 
            Div. 3 </td>
        <td>Bob 
            McCardle
            
            109 Racetrack Lane 
            Beaver Springs, PA 17812<a href="mailto:beaverbob@beaversprings.com"> beaverbob@beaversprings.com</a></td>
        <td> Track:570-658-9601 
            Office:570-658-9601 
            FAX:570-658-9601 </td>
        <td> RACED (Multi-time champ)  
            My original home track. Ran points at BSD '90-'99. If the 
            mountain disappears (humidity), dial up! Great new lighting in '05. 
            New scoreboards in '09. Clean, friendly, nostalgic feel. No 
            Buybacks! Great food at great prices. Compulink. Sunoco 
            fuel. Elevation: 640' (but track is on a 1% uphill grade)   2010 upgrades:  Track ground (right lane transition now gone), 
            full Compulink freshen &amp; alignment of sensors, new tower, more lighting, 
            and more. </td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.luckydragcity.com/"> Lucky Drag City </a></td>
        <td>I-90 Exit 
            11
            
            southeast on SR8 11 miles <span style="color: windowtext; mso-color-alt: windowtext; mso-fareast-font-family: Times New Roman; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA"> Half-way between Cleveland and Buffalo <a href="mailto:dragraceerie@verizon.net"><span style="mso-fareast-font-family: Times New Roman; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA"> dragraceerie@verizon.net</span></a></span></td>
        <td> 814-739-2735 </td>
        <td> formerly Wattsburg Dragway, Lowville Drag Raceway. <span style="color: windowtext; mso-color-alt: windowtext; mso-fareast-font-family: Times New Roman; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA"> Bi-Monthly “Super Showdown Series"&copy;</span></td>
    </tr>
    <tr>
        <td><a href="http://www.maplegroveraceway.com/" target="_blank"> Maple Grove Raceway</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Reading, 
            PA </td>
        <td> (610) 856-781 </td>
        <td> RACED (Won '94 NHRA Div. 1 Bracket Finals)  
            One of the nicest facilities in the country, top to bottom. 
            Typically very quick, and very tricky to dial due to ever swirling 
            winds, and intermittent dead-spots of air. Compulink. Sunoco 
            fuel. Elevation: 330' </td>
    </tr>
    <tr>
        <td><a href="http://www.numidiadragway.net/" target="_blank"> Numidia Dragway</a> 1/4 Mile 
            NHRA 
            Div. 1 </td>
        <td>Vinny 
            Dimino
            
            10 Dragstrip Road 
            Numidia, PA 17858 <a href="mailto:announcer@numidiadragway.net"> announcer@numidiadragway.net</a></td>
        <td> (570) 799-5090 
            (570) 799-5091 </td>
        <td> RACED (Won Box class, footbraking)  
            Great spectator track. Track is cut into the hill as it goes 
            downtrack. Uphill shutdown. Smooth surface. Watch the 
            wind, as it can funnel between the hills and multiply its effect! 
            Sunoco fuel. Portatree. Elevation: 970' 
            Track radio: 104.7FM </td>
    </tr>
    <tr>
        <td><a href="http://www.pittsburghracewaypark.com" target="_blank"> Pittsburgh Raceway Park </a>1/4 Mile 
            IHRA 
            Div. 3 </td>
        <td>Greg 
            Miller
            
            538 Stone Jug Road 
            New Alexandria, PA 15670 <a href="mailto:miller@pittsburghracewaypark.com"> miller@pittsburghracewaypark.com</a></td>
        <td> Track:724-668-7600 
            FAX:724-668-2296 </td>
        <td> RACED (Won $5K No-Box, Multiple Pro-Am wins)  
            Great job by the Tedesco Bros. rebuilding this track. Repaved, 
            new lights, great food, shutdown lengthened, new pit area added. 
            Slight hump early in right lane. Bump right after finish in left 
            lane. Constant improvements. This place HOOKS! New Accutime in 
            '08. VP Fuel. </td>
    </tr>
    <tr>
        <td><a href="http://www.southmountaindragway.us">Quarter Aces Drag-O-Way</a> (Formerly South Mountain Dragway)  
            1/8 Mile 
            NHRA 
            Div. 1 </td>
        <td>Mike/ 
            Kathy Jones
            
            1107 Petersburg Rd. 
            Boiling Springs, PA 17007 <a href="mailto:nhrap133@comcast.net" class="Email_Link"> nhrap133@comcast.net</a></td>
        <td> (717) 258-6287 </td>
        <td> RACED (Best: semi)  
            Nostalgic feel. Spectator side on the hill. Tons of trees. 
            Staging lanes run <i>right</i> beside the track. TSI RaceNet timing. </td>
    </tr>
    <tr>
        <td><a href="http://www.sunsetdragstrip.com/">Sunset Dragstrip</a> 1/8 Mile </td>
        <td>Located equal distance between Mercer 
            Pa., and Hermitage Pa., 1 mile off of Rt 62 on Charleston Road </td>
        <td> ALEX (727-278-1194) 
            FRANK (724-866-2519) 
            BILL BENSON (724-813-9164) </td>
        <td> Independent 1/8th mile track. 2000' total length. </td>
    </tr>
    <tr  >
        <td><a name="SC"></a> South Carolina </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.bowmandragway.com/" target="_blank">Bowman Dragway</a> (Also "Dwayne's World Fast Track") 
            1/8 mile </td>
        <td>384 
            Berrywood Rd
            
            Orangeburg, SC 29115 </td>
        <td> (803) 536-3822 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.darlingtondragway.com/"> Darlington Dragway</a> 1/4 mile 
            IHRA 
            Div. 2
            </p></td>
        <td>Bill 
            Wilson
            
            2056 E. Bobo Newsome Hwy 
            Hartsville, SC 29550 <a href="mailto:info@darlingtondragway.com">info@darlingtondragway.com</a></td>
        <td> Track: 843-383-0008 
            Fax: 843-383-9933 </td>
        <td> RACED     (Best: Won, '09 IHRA Div. 2 Bracket Finals)  
            Covered IHRA Div. 1 Bracket Finals here twice. Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.carolinadragway.com" target="_blank"> Carolina Dragway, Inc. </a>1/4 Mile 
            IHRA 
            Div. 2 </td>
        <td>Jeff 
            Miles Jr. &amp; Angel Miles
            
            P.O. Box 70 
            Jackson, SC 29831 <a href="mailto:angel@carolinadragway.com">angel@carolinadragway.com</a></td>
        <td> Track:803-471-2285 
            Office:803-471-3688 
            FAX:803-266-3715 </td>
        <td> RACED (Best: Won Stock)  
            Covered the IHRA Div. 2 Bracket Finals here twice. Superb 
            facility. New Compulink with all new wiring, new tower. 
            1/4-mile track, but steep incline at finish, usually run 1/8th. 
            Track Radio: 90.1FM </td>
    </tr>
    <tr>
        <td><a href="http://www.dorchesterdrag.com/" target="_blank"> Dorchester Dragway</a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td>Elijah 
            DeLee
            
            P.O. Box 166 
            Dorchester, SC 29437 </td>
        <td> Track:843-563-5412 
            Office:843-563-5539 
            FAX: 843-563-7677 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.greerdragway.com" target="_blank"> Greer Dragway</a><a href="http://www.grandbendmotorplex.com" target="_blank"> </a>1/8 Mile 
            IHRA 
            Div. 1 </td>
        <td>Mike 
            Greer
            
            1792 Dragway Road 
            Greer, SC 29651<a href="mailto:Greerdrag@aol.com"> Greerdrag@aol.com</a></td>
        <td> Track:864-879-4634 
            Office:864-877-0457 
            FAX:864-989-0624 </td>
        <td> TSI RaceNet. New surface in 2010. </td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Lowcountry Dragway 
            1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td><font size="1" face="Arial" color="#808080">Frank Lambert 
            P.O. Box 2021 
            Moncks Corner, SC 29461 </td>
        <td><font size="1" face="Arial" color="#808080">Track:843-761-2566 </td>
        <td><font size="1" face="Arial" color="#808080">DEFUNCT 
            formerly Cooper River Dragway. New timing and wiring in 2005. 
            Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.midwaydragracing.net/">Midway Dragstrip</a> 1/8 mile </td>
        <td><font face="Arial"> <strong style="font-weight: 400"><font size="2" class="df"> <font size="1">Track Manager - Jeremiah Bryant </strong> <font size="2" class="df"> <font face="Arial"><font size="1">2948 South Williamsburg Hwy 
            Greeleyville, SC 29056 <strong style="font-weight: 400"><font size="1"> <a href="mailto:jeremiah@midwaydragracing.net"> jeremiah@midwaydragracing.net</a> </strong></td>
        <td> 843-426-4419 </td>
        <td> VISITED  
            Visited here on a non-race day, and walked the track when it first 
            opened. </td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.orangeburgdragstrip.com"> Orangeburg Dragstrip</a> 1/8 mile </td>
        <td>Buddy 
            Boozer
            
            194 Dragstrip Road 
            Orangeburg, SC </td>
        <td> 803-534-3428 
            803-534-8999 </td>
        <td> This track has a new owner, and is being completely rebuilt, modeled 
            after Carolina Dragway, with new wider concrete track and walls. New 
            bathrooms, TSI timing system, lights, new concessions. Re-opened 11/06 </td>
    </tr>
    <tr>
        <td><a href="http://www.jpdragway.com" target="_blank">Pageland Dragway</a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td>Mike 
            Bradley - part owner
            
            Doug Mayhew - bracket program manager 
            3167 Peach Orchard Rd. 
            Jefferson, SC 29718 <a href="mailto:info@jpdragway.com">info@jpdragway.com</a></td>
        <td> Phone: 843-658-3556 
            Fax: 704-821-7069 </td>
        <td> RACED (Best: WIN)  
            Formerly Jefferson-Pageland Dragway. Track ground early 2007. Track 
            record 4.00 set by Kelly Martin in 2007. TSI. Sunoco fuel. 
            FM 100.1 </td>
    </tr>
    <tr>
        <td><a href="http://www.unioncountydragway.info">Union County Dragway</a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td><font size="1" face="Arial" color=""> Tony Brown 
            178 Dragway Dr. 
            Union, SC 29379 
            (For MapQuest, use 2102 SANTUC-CARLISLE HW  Y) </td>
        <td></td>
        <td> RACED (Best: WIN) 
            New facility, opened June '09. 
            Ran the first IHRA Pro-Am here Aug '10. Pits still under 
            construction, but the track itself was perfect. FM 107.1 </td>
    </tr>
    <tr>
        <td><a href="http://www.wareshoalsdragway.com/" target="_blank"> Ware Shoals Dragway</a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td>Rayford 
            Gambrell
            
            17052 Hwy. 25 
            Ware Shoals, SC 29692 <a href="http://www.ihra.com/tracks/tinker@emeraldis.com"> tinker@emeraldis.com</a></td>
        <td> Track:864-861-2467 
            Office:864-456-4502 </td>
        <td> Chrondek. </td>
    </tr>
    <tr  >
        <td><font size="3" face="Arial" color="#FFFF00"><a name="SD"></a>South 
            Dakota </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Dakota 
            Intermountain Dragway </td>
        <td>Bell Fourche, SD </td>
        <td> 605-892-4472 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.oahespeedway.com/" target="_blank">Oahe Speedway </a>1/4 Mile 
            NHRA 
            Div. 5 </td>
        <td>Pierre, 
            S.D. </td>
        <td> (605) 223-9885 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.sturgisdrags.com/" target="_blank">Sturgis Dragway </a>1/8 Mile 
                NHRA 
                Div. 5 </p></td>
        <td>Sturgis, 
            S.D. </td>
        <td> (605) 347-3724 </td>
        <td> Chrondek. </td>
    </tr>
    <tr>
        <td><a href="http://www.thundervalleydragways.com" target="_blank"> Thunder Valley Dragway </a>1/4 Mile 
            IHRA 
            Div. 5 </td>
        <td>Glen Rapp
            
            SD Hyway #44 
            Marion, SD </td>
        <td> Phone:605-648-3604 
            FAX:605-498-1298 </td>
        <td> Elevation: 1500'. </td>
    </tr>
    <tr  >
        <td><a name="TN"></a> Tennessee </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.411dragway.net/">411 
            Dragway</a></td>
        <td>Donnie 
            Phillips
            
            632 Maryville Hwy 
            Seymour, TN </td>
        <td> 865-577-8551 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.bristolmotorspeedway.com/" target="_blank">Bristol 
                Dragway </a>1/4 Mile 
                NHRA 
                Div. 2 </p></td>
        <td>Bristol, 
            Tenn. </td>
        <td> (423) 764-3724 </td>
        <td> RACED (Best: Win <i>and</i> R/U at 2008 
            World Footbrake Challenge, Sun $10K) 
            Compulink. Sunoco fuel </td>
    </tr>
    <tr>
        <td><a href="http://www.cherokeemotorsportspark.com" target="_blank"> Cherokee Raceway Park</a> 1/8 Mile 
            Div. 2 </td>
        <td><p align="left"><strong style="font-weight: 400"> <font size="1" face="Arial, Helvetica, sans-serif" color="#000000">154 
                Racetrack Road  
                Rogersville, TN 37857 <font size="1" face="Arial, Helvetica, sans-serif">email: <a href="mailto:roger@lucky13racing.net">roger@lucky13racing.net</a> </strong></p></td>
        <td><strong style="font-weight: 400"> <font size="1" face="Arial, Helvetica, sans-serif" color="#000000">Track 
            Phone: (423) 272-2555 <font size="1" face="Arial, Helvetica, sans-serif">Track Operator 
            - Roger Ayers-  
            276- 870-5423 or 423.754.4550 
            Owner - Dixee Dragz, llc - 954-655-7787 </strong></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.clarksvillespeedway.com">Clarksville Speedway</a></td>
        <td><address>
            <span style="font-style: normal"> 1600 
            Needmore Road </span>
            </address>
            <address>
            <span style="font-style: normal"> Clarksville, TN 37040 </span>
            </address>
            <address>
            <span style="font-style: normal"> <a href="mailto:wiltamracing@aol.com">wiltamracing@aol.com</a> </span>
            </address></td>
        <td><address>
            <span style="font-style: normal"> (931) 
            645-2523 </span>
            </address>
            <address>
            <span style="font-style: normal"> Fax: 
            (931) 645-5171 </span>
            </address></td>
        <td> TSI RaceNet </td>
    </tr>
    <tr>
        <td><a href="http://englishmountaindragway.jimdo.com/" target="_blank"> English Mountain Dragway</a> 1/8 Mile </td>
        <td>David 
            Clevenger
            
            1323 Lewis Rd.  
            Newport, TN 37821 </td>
        <td> Track:423-625-8375 
            Office:423-748-7277 </td>
        <td></td>
    </tr>
    <tr>
        <td> Great 
            River Road Raceway 
            1/8 Mile </td>
        <td><span style="font-family: Arial" class="Apple-style-span"> <font size="1">Great River Road Raceway 
            165 Little Levee Rd 
            Dyersburg, TN 38024 </span></td>
        <td><span style="font-family: Arial" class="Apple-style-span"> <font size="1">731-285-1856 </span></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.i40dragway.com/" target="_blank">I-40 Dragway</a><a href="http://www.akracewaypark.com" target="_blank"> </a>1/4 Mile 
            IHRA 
            Div. 2 </td>
        <td>Bunny 
            Howe
            
            1650 Creston Rd. 
            Crossville, TN 38555 <a href="mailto:i40drag@multipro.com">i40drag@multipro.com</a></td>
        <td> Track:931-484-0049 
            Office:931-456-1584 
            FAX:931-456-1582 </td>
        <td></td>
    </tr>
    <tr>
        <td>Jackson Dragway
            
            1/8 mile </td>
        <td>Jackson, TN </td>
        <td> 901-423-9784 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.knoxdragway.com" target="_blank"> Knoxville Dragway</a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td>Shay 
            Loveday
            
            802 C Sevier 
            Knoxville, TN 37912 </td>
        <td> Phone: 865-577-3112 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.londondragway.com">London Dragway</a> 1/8 Mile 
            IHRA 
            Div. 2 </td>
        <td>London 
            Dragway 
            
            3835 White Oak Road  
            London, KY 40741 <a href="mailto:boone72@windstream.net">Email Craig Boone</a></td>
        <td> (606) 878-8883 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.mtdragway.com/">Middle Tennessee Dragway</a> IHRA 
            1/8 mile 
            Div. 2
            </p></td>
        <td>Gary Huddleston
            
            Buffalo Valley, TN </td>
        <td> (931) 858-2422 </td>
        <td> former IHRA Div. 2 Champs </td>
    </tr>
    <tr>
        <td><a href="http://www.memphismotorsports.com/" target="_blank"> <font color="#808080"> Memphis Motorsports Park </a> <font color="#808080"> 1/4 Mile </td>
        <td><font size="1" face="Arial" color="#808080">Memphis, Tenn. </td>
        <td></td>
        <td><font size="1" face="Arial" color="#808080">CLOSED. </td>
    </tr>
    <tr>
        <td><a href="http://www.musiccityraceway.com/" target="_blank">Music City 
            Raceway </a>1/8 Mile 
            NHRA 
            Div. 2 </td>
        <td>Goodlettsville, Tenn. </td>
        <td> (615) 876-0981 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.northwesttennesseemotorsports.com" target="_blank"> Northwest Tennesee Motorsports Park </a>1/8 Mile 
                NHRA 
                Div. 2 </p></td>
        <td>Gleason, 
            Tenn. </td>
        <td> (731) 648-9567 </td>
        <td></td>
    </tr>
    <tr>
        <td><span class="081345813-21012005"> U.S. 43 Dragway</span></td>
        <td><div style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> <span class="081345813-21012005"> Ed 
                Brooks 
                29 Dooley Rd 
                Ethridge, TN 38456 </span> </div></td>
        <td> Phone: 931-381-3583 </td>
        <td></td>
    </tr>
    <tr  >
        <td><a name="TX"></a> Texas </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Alamo Dragway 
            1/4 mile </td>
        <td><font size="1" face="Arial" color="#808080">San Antonio, TX </td>
        <td><font size="1" face="Arial" color="#808080"> 210-923-8801 
            210-628-1371 </td>
        <td><font size="1" face="Arial" color="#808080">DEFUNCT </td>
    </tr>
    <tr>
        <td><a href="http://www.abilenedragstrip.com" target="_blank"> Abilene Dragstrip </a> 1/8 Mile 
            IHRA 
            Div. 4 </td>
        <td> 1134 South San Jose 
            Abilene, TX 79605 </td>
        <td>bgcolor="#ffffff"  Phone: 325-721-2482 
            Fax: 325-676-7225 </td>
        <td>bgcolor="#ffffff"  CLOSED 
            Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.amarillodragway.com" target="_blank"> Amarillo Dragway</a> 1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td>Norm &amp; 
            Kat Henson
            
            12955 Burlington Rd. 
            Amarillo, TX 79118 <a href="mailto:64dodger@amarillodragway.com"> 64dodger@amarillodragway.com</a></td>
        <td> Phone: 806-622-2010 
            Fax: 806-622-2010 
            Cell: 806-231-0359 </td>
        <td> Elevation: 3700'. </td>
    </tr>
    <tr>
        <td> Angleton Dragway 
            1/8 Mile 
            IHRA 
            Div. 4 </td>
        <td> Mike 
            Hurta             &amp; Jim Naylor 
            27250 FM 2004 
            Angleton, TX 77515 <a href="mailto:mhurta@sbcglobal.net"> mhurta@sbcglobal.net </a></td>
        <td>bgcolor="#ffffff"  Track: 979-549-0311 
            Office: 979-848-0243 
            Fax: 979-481-4448 </td>
        <td>bgcolor="#ffffff"  CLOSED </td>
    </tr>
    <tr>
        <td><a href="http://www.evadaleraceway.com" target="_blank"> Ben Bruce Memorial Airpark Raceway</a> 1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td>Mike 
            Bruce
            
            1# Gunslough Rd 
            Evadale, TX 77615 </td>
        <td> Track: 409-276-1910 
            Office:409-276-2288 
            Fax: 409-276-1905 </td>
        <td></td>
    </tr>
    <tr>
        <td>Cedar 
            Creek Dragway
            
            1/8 mile </td>
        <td>Kemp, TX </td>
        <td> 903-498-8643 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.cherokeecountymotorsportspark.com/" target="_blank"> Cherokee County Motorsports Park </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td>Rusk, 
            Texas </td>
        <td> (903) 683-4711 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><font size="1" face="Arial" color="#808080"> Corpus Christi Dragway 
            1/4 mile </td>
        <td><font size="1" face="Arial" color="#808080"> Corpus Christi, TX </td>
        <td><font size="1" face="Arial" color="#808080"> (956)689-9415 or 
            (512)937-1012 </td>
        <td><font size="1" face="Arial" color="#808080">DEFUNCT </td>
    </tr>
    <tr>
        <td><a href="http://www.dentondragway.com/"> Dallas Raceway </a> 1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td> Crandall, TX <a href="mailto:Sammy@DallasRaceway.com" target="_blank"> Sammy@DallasRaceway.com </a></td>
        <td><p align="left" Phone: 972-474-2600 
                Fax: 972-474-2601 
            
            </p></td>
        <td>bgcolor="#ffffff"  CLOSED 
            Brand new all-concrete IHRA National Event 
            facility completed in May '09. </td>
    </tr>
    <tr>
        <td><a href="http://www.desertthunderraceway.com" target="_blank"> Desert Thunder Raceway</a> 1/8 Mile 
            IHRA 
            Div. 4 </td>
        <td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Mike Waldrop 
                1 Hialeah Dr. 
                Midland, TX 79705 </p></td>
        <td> Phone: 432-685-1877 
            Cell: 432-664-3949 </td>
        <td> Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.edinburgracetrack.com" target="_blank"> Edinburg International Racetrack </a>1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td>Julio 
            Cantu
            
            15920 Expressway North 
            Edinburg, TX 76205 </td>
        <td> Track:956-318-0355 
            Office:956-239-0470 
            FAX:956-318-0382 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.elpasomotorplex.com">El Paso Motorplex</a> 1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td>16101 
            Gateway West
            
            Clint, TX 79836 
            elpasomotorplex.inc@gmail.com </td>
        <td> 915-887-3318 
            915-525-9645 </td>
        <td></td>
    </tr>
    <tr>
        <td>Hallsville Raceway
            
            1/4 mile </td>
        <td>Hallsville, TX </td>
        <td> 903-668-2858 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.houstonmotorsportspark.com/" target="_blank">Houston 
            Motorsports Park </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td>Houston, 
            Texas </td>
        <td> (281) 458-1972 </td>
        <td> Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.houstonraceway.com/" target="_blank">Houston Raceway 
            Park </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td>Houston, 
            Texas </td>
        <td> (281) 383-7223 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.lonestarraceway.com" target="_blank"> Lone Star Raceway Park </a>1/8 Mile 
            IHRA 
            Div. 4 </td>
        <td>Michael 
            Wear
            
            120 Old Columbus Rd. 
            Sealy, TX 77474 </td>
        <td> Track:979-885-0731 
            Office:979-885-0731 
            FAX:979-885-0750 </td>
        <td></td>
    </tr>
    <tr>
        <td><p align="left"> <a href="http://www.lubbockdragway.com/" target="_blank">Lubbock Dragway </a>1/4 Mile 
                NHRA 
                Div. 4 </p></td>
        <td>Lubbock, 
            Texas </td>
        <td> (806) 842-3842 </td>
        <td> formerly Idalou Motorsports Park? Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.navasotaraceway.com">Navasota Raceway</a> 1/8 mile </td>
        <td><p align="left"> Navasota Raceway 
                10847 FM 1227 
                Navasota, TX 77868 
                
                Mailing address: 
                29195 Kyle Road, Waller, TX 77484 <a href="mailto:navasotaraceway@yahoo.com">navasotaraceway@yahoo.com</a> </p></td>
        <td> Track: (936) 825-8725 
            Larry Magnus (936) 372-5119 
            or (281) 642-3520 
            Richard Magnus (713) 705-7380 </td>
        <td> New ownership. 
            Concession stand, restrooms, race fuel available. New TSI RaceNet system 
            in 2009. </td>
    </tr>
    <tr>
        <td><a href="http://www.northstar-dragway.com/">North Star Dragway</a> (formerly Denton Dragway) 
            1/8 Mile 
            NHRA 
            Div. 4 </td>
        <td>3236 
            Memory Lane
            
            Denton, TX </td>
        <td> (940)-482-9998 </td>
        <td> formerly I-35 North Texas Dragway. 
            Chrondek. </td>
    </tr>
    <tr>
        <td><a href="http://www.parisdragstrip.com/" target="_blank">Paris Dragstrip </a>1/8 Mile 
            NHRA 
            Div. 4 </td>
        <td>Paris, 
            Texas </td>
        <td> (903) 982-5358 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.pinevalleyracing.com" target="_blank"> Pine Valley Raceway Park</a> 1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td>Bill 
            Bailey
            
            3427 FM 2497 
            Lufkin, TX 75904 <a href="mailto:billbaileyjr@netzero.com">billbaileyjr@netzero.com</a></td>
        <td> Track: 936-699-3227 
            Office: 936-699-3227 
            Fax: 936-637-3492 </td>
        <td> Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.redlineraceway.com/" target="_blank"> Redline Raceway</a> 1/8 Mile<a href="http://www.redlineraceway.com/" target="_blank"> </a>IHRA 
            Div. 4 </td>
        <td>Jerry Lee 
            Goss
            
            5326 FM 1565 
            Caddo Mills, TX 75135 <a href="mailto:info@redlineraceway.com">info@redlineraceway.com</a></td>
        <td> Track:903-527-5911 
            Office:903-527-5911 
            FAX:903-527-5818 </td>
        <td> VISITED  
            Well, seen it from the Interstate, which overlooks the entire 
            facility. Looks like a great place! Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.sanantonioraceway.com" target="_blank"> San Antonio Raceway</a> 1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td>Todd 
            Zampese
            
            3641 Santa Clara 
            Marion, TX 78124 <a href="mailto:sar@sanantonioraceway.com?subject=contact" target="main"> sar@sanantonioraceway.com</a></td>
        <td> Track:830-914-4646 
            Office:210-698-2310 
            FAX:830-420-2200 </td>
        <td> RACED (Best: R/U Super Stock)  
            Raced a few IHRA Nationals here. Great facility from top to 
            bottom! Good amount of paved pits. VP Fuel. Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.littleriverdragway.net/">Little River Dragway</a> (Formerly Temple Academy Dragway) 
            1/4 Mile 
            IHRA 
            Div. 4 </td>
        <td>Hutch
            
            2600 Moores RD. 
            Temple, TX 76504 <a href="mailto:hutch@littleriverdragway.net"> hutch@littleriverdragway.net</a></td>
        <td><p style="FONT-SIZE: 11px" class="MsoNormal"> <font size="1" face="Arial" color="#000000"> Track Phone Number - 254-982-9188 
                Track Owner - Hutch - 817-965-0535 </p></td>
        <td> Sunoco </td>
    </tr>
    <tr>
        <td><a href="http://www.texasmotorplex.com/" target="_blank">Texas Motorplex </a>1/4 Mile 
            NHRA 
            Div. 4 </td>
        <td>Ennis, 
            Texas </td>
        <td> (972) 878-2641 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.texasraceway.com/" target="_blank">Texas Raceway </a>1/8 Mile 
            NHRA 
            Div. 4 </td>
        <td>3830 S. 
            Kennedale New Hope Rd
            
            (MAILING: <font size="1" face="Tahoma"> PO BOX 690)          
            Kennedale, TX 76060 </td>
        <td> 817-483-0356 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.thunderalley.com/" target="_blank"> Thunder Alley Dragway </a> <font color="#808080"> 1/4 Mile          
            IHRA 
            Div. 4 </td>
        <td><font size="1" face="Arial" color="#808080">Joe Vilchis 
            15701 Montana Ave. 
            El Paso, TX 79996 </td>
        <td><font size="1" face="Arial" color="#808080">Track:915-849-0888 
            Office:915-490-1777 </td>
        <td><font size="1" face="Arial" color="#808080">DEFUNCT </td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.yellobellydragstrip.com/"> Yello Belly Drag Strip</a> 1/8th mile </td>
        <td>Hwy 180
            
            West Dallas, TX </td>
        <td> (972) 986-9200 </td>
        <td> No rules, heads up, run what you brung racing. 1/8 mile track with 1/4 
            mile shutdown. Covered trackside spectator areas. </td>
    </tr>
    <tr>
        <td><p><font size="1" face="Arial" color="#CCCCCC"> Wall 
                Dragway 
                1/4 mile </p></td>
        <td><font size="1" face="Arial" color="#CCCCCC"> San Angelo, TX </td>
        <td><font size="1" face="Arial" color="#CCCCCC"> 915-949-6149 </td>
        <td><font size="1" face="Arial" color="#CCCCCC"> Compulink. </td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.wtracewaypark.com/"> West Texas Raceway Park</a> 1/4 Mile </td>
        <td><p align="left"> W.T.R.P. 
                2713 Cumberland 
                Odessa TX 79762 </p></td>
        <td> 915 362-4242 </td>
        <td> Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.wichitaracewaypark.com/" target="_blank">Wichita 
            Raceway Park </a>1/8 Mile 
            IHRA 
            Div. 4 </td>
        <td>Iowa 
            Park, Texas <a href="mailto:wichtaraceway@aol.com">wichitaraceway@aol.com</a></td>
        <td> (940) 592-1069 </td>
        <td> Accutime. </td>
    </tr>
    <tr  >
        <td><a name="UT"></a> Utah </td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr  >
        <td><a href="http://www.rmrracing.com/" target="_blank"> Rocky Mountain Raceways </a>1/4 Mile 
            IHRA 
            Div. 5 </td>
        <td>West 
            Valley City, Utah </td>
        <td> (801) 252-9557 </td>
        <td> Compulink. 
            Elevation: 4400' </td>
    </tr>
    <tr  >
        <td><a name="VA"></a> Virginia </td>
        <td></td>
        <td>align="left"></td>
        <td>align="left"></td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.cbdragway.com/"> Colonial Beach Dragway</a> 1/8th mile 
            IHRA </td>
        <td>Colonial 
            Beach, VA </td>
        <td> Phone:804-224-7455 </td>
        <td></td>
    </tr>
    <tr>
        <td> Eastside Speedway 
            1/8 Mile </td>
        <td>Waynesboro, VA </td>
        <td> 540-943-9336 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.elkcreekdragway.com/" target="_blank"> Elk Creek Dragway, Inc. </a>1/8 Mile 
            IHRA 
            Div. 1 </td>
        <td>Mike 
            Walters
            
            Elk Creek Dragway 
            P.O. Box 1351 
            Wytheville,VA 24382 <a href="mailto:hjones@valink.com">hjones@valink.com</a></td>
        <td> Phone:276-655-7223 </td>
        <td> Compulink. </td>
    </tr>
    <tr  
        
    <td><a href="http://www.motormilespeedway.com" target="_blank"> Motor Mile Speedway</a> IHRA 
            Div. 1 </td>
        <td>Steve 
            Groseclose
            
            6832 Lee Highway 
            Radford, VA 24141 </td>
        <td> Phone: 540-639-1700 </td>
        <td> RACED 
            New track in '05! All 
            concrete. Accutime. </td>
    </tr>
    <tr>
        <td><a href="http://www.naturalbridgedragstrip.com/" target="_blank">Natural Bridge Dragstrip</a> 1/8 Mile 
            IHRA 
            Div. 1 </td>
        <td>Dan Weis, 
            Jr.
            
            12305 Shore View Dr 
            Richmond, VA 23233 <a href="mailto:nbdragstrip@hotmail.com">nbdragstrip@hotmail.com</a></td>
        <td> Track:540-291-3724 
            Office:804-364-5655 
            Fax: 804-364-3572 </td>
        <td></td>
    </tr>
    <tr>
        <td><a href="http://www.newlondondragracing.com/">New London Dragstrip</a> 1/8 Mile 
            IHRA </td>
        <td><p align="left"> New London Dragstrip Association 
                LLC 
                PO Box 12032 
                Lynchburg, VA 24506 
                
                Physical address for the track is: 
                1261 Wheels Dr 
                Forest, VA 24551 <a href="mailto:staff@newlondondragracing.com"> staff@newlondondragracing.com</a> </p></td>
        <td> (434) 525-8916 </td>
        <td><p align="left" style="margin-left: 5px;"> 40 
                foot wide asphalt track with a concrete launching pad stretching past 
                60'. Total track length: 3200 feet, Track elevation: 849'. Wireless 
                internet zone. Portatree timing. VP 110 race fuel. </p></td>
    </tr>
    <tr>
        <td><a target="_blank" href="http://www.olddominionspeedway.com/OD_Dragstrip_Main.html"> Old Dominion Speedway</a> 1/8 mile
            </p></td>
        <td>10611 Dumfries Road
            
            Manassas, VA </td>
        <td> 703-361-7753 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.richmonddragway.com" target="_blank"> Richmond Dragway </a>1/4 Mile 
            IHRA 
            Pro-Am Div. 1 
            SWC Div. 9 </td>
        <td>Johnny 
            Davis
            
            9029 Peaks Road 
            Ashland, VA 23005 <a href="mailto:jdavis@richmonddragway.com">jdavis@richmonddragway.com</a></td>
        <td> Phone: 804-737-1193 
            Fax: 804-730-1305 </td>
        <td> RACED (Best: SEMI) 
            Ground and polished in '03 by 
            IHRA's Jim Weinert. Accutime. Racing since 1961! 
            Elevation: 167' 
            Track Radio: 88.3FM </td>
    </tr>
    <tr>
        <td><a href="http://www.sumerduckdragway.com" target="_blank"> Sumerduck Dragway</a> 1/8 Mile 
            IHRA 
            Div. 1 </td>
        <td>Joy &amp; 
            Mike Anderson
            
            12214 Pemwood Lane 
            Fredericksburg, VA 22407 <a href="mailto:jcamca@adelphia.net">jcamca@adelphia.net</a></td>
        <td> Track: 540-439-8080 
            Office: 540-845-1656 </td>
        <td> Compulink. </td>
    </tr>
    <tr>
        <td><a href="http://www.virginiamotorsportspk.com/" target="_blank"> Virginia Motorsports Park</a><a href="http://www.keystoneraceway.net/" target="_blank"> </a>1/4 Mile 
            IHRA 
            Div. 1 </td>
        <td>Bryan 
            Pierce
            
            8018 Boydton Plank Road 
            Petersburg, VA 23803<a href="mailto:bpierce@virginiamotorsportspk.com"> bpierce@virginiamotorsportspk.com</a></td>
        <td> Track:804-862-3174 
            Office:804-862-3174 
            FAX:804-862-3301 </td>
        <td> RACED (Won Pro-Am)  
            Raced NHRA Div. 2 race here in '99, and IHRA Nat'l/Div'l races last 
            few years. State of the art facility. Track is cut in a 
            bit, so watch for funnel effect of wind. Can get slippery at 
            night. Rollout about .01 tight. Compulink. Sunoco 
fuel. </td>
</tr>
<tr>
<td><a name="WA"></a> Washington </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><a href="http://www.bonanzaraceway.com/" target="_blank">Bonanza Raceway </a>1/4 Mile 
NHRA 
Div. 6 </td>
<td>Walla 
Walla, Wash. </td>
<td> (509) 522-4804 </td>
<td></td>
</tr>
<tr>
<td><a href="http://www.bremertonraceway.com/" target="_blank">Bremerton 
Raceway </a>1/4 Mile 
NHRA 
Div. 6 </td>
<td>Bremerton, Wash. </td>
<td> (360) 674-2280 </td>
<td> Compulink. </td>
</tr>
<tr>
<td><a href="http://www.pacificraceways.com/" target="_blank">Pacific 
Raceways </a>1/4 Mile 
NHRA 
Div. 6 </td>
<td>Kent, 
Wash. </td>
<td> (253) 639-5927 </td>
<td> formerly Seattle Int'l Raceway. 
Compulink. 
Elevation: 325' 
Track Radio: 540AM </td>
</tr>
<tr>
<td><a href="http://www.renegaderaceway.com/" target="_blank">Renegade 
Raceway </a>1/4 Mile 
NHRA 
Div. 6 </td>
<td><p align="left"> Yakima, Wash. </p></td>
<td> (509) 877-4621 </td>
<td> Compulink. 
Elevation: 900' </td>
</tr>
<tr>
<td><a href="http://spokanecountyraceway.com/">Spokane County Raceway</a> 1/4 mile 
NHRA
</p></td>
<td>750 N. Hayford Rd.

Airway Heights, WA 99001 </td>
<td> (509) 244-3333 </td>
<td> Chrondek (old). </td>
</tr>
<tr>
<td><a name="WV"></a> West Virginia </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><font size="1" face="Arial" color="#808080"> Fairmont Dragway* 
1/8th mile </td>
<td><font size="1" face="Arial" color="#808080"> Fairmont, WV </td>
<td></td>
<td><font size="1" face="Arial" color="#808080"> * defunct </td>
</tr>
<tr>
<td><a href="http://www.kanrace.com" target="_blank">Kanawha Valley Dragway </a>1/8 Mile 
IHRA 
Div. 3 </td>
<td>Physical Address 

2492 US RT 35 
Southside, WV 25187
<p> Mailing 
Address
335 Simms Street 
St. Albans, WV 25177 </p></td>
<td> Track:304-675-6760 
Office:304-766-6126 
FAX:304-766-6127 </td>
<td> RACED (Best: Runner-Up)
Ran a couple IHRA Pro-Ams here. Beautiful facility, nice folks. 
TSI timing. Short shutdown. Sunoco fuel. </td>
</tr>
<tr>
<td><a href="http://www.mountaineerkartway.net/">Mountaineer Dragway</a> 1/8 mile </td>
<td>Beckley, 
WV </td>
<td> 304-763-3105 </td>
<td> formerly Raleigh County Dragway; Grandview Dragway. Reopening 2009 
for ATV drags </td>
</tr>
<tr>
<td><a href="http://www.tririverdragway.com/">Tri-River Dragway</a> 300' </td>
<td><div> Owners: Randall &amp; Kay Sartin </div>
<div> Rt. 2 Box 230 </div>
<div> Fort Gay, WV 25514 </div>
<div> <a href="mailto:kay@tririverdragway.com">kay@tririverdragway.com</a> </div></td>
<td><div> Track: (304) 648-8004 </div>
<div> Home:(304) 648-5115 </div></td>
<td> A unique 300' asphalt track, with 590' of 
shutdown plus 100' sand trap. TSI RaceNet Timing. </td>
</tr>
<tr>
<td><a name="WI"></a> Wisconsin </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><a href="http://www.greatlakesdragaway.com/" target="_blank"> Great Lakes Dragaway</a> 1/4 Mile 
NHRA 
Div. 3 </td>
<td>Ray Drew

P.O. Box 7 
Union Grove, WI 53182 <a href="mailto:gld@execpc.com">gld@execpc.com</a></td>
<td> Track:414-878-3783 
Office:414-878-3783 
FAX:262-878-4462 </td>
<td> RACED
Ran one IHRA Pro-Am here in '03. Outstanding people and a great 
hookin' facility! Some paved pit. Tower centered between 
burnout boxes. Compulink. </td>
</tr>
<tr>
<td>Lake Geneva Jr. Dragway

1/8 mile
</p></td>
<td>Lake Geneva, WI </td>
<td> 608-676-4801 </td>
<td> Junior Dragsters Only </td>
</tr>
<tr>
<td><a href="http://www.rockfallsraceway.com/" target="_blank"> Rock Falls Raceway </a>1/4 Mile 
NHRA 
Div. 5 </td>
<td>Al Corda

Eau Claire, Wis. </td>
<td> (715) 879-5089 </td>
<td> Compulink. </td>
</tr>
<tr>
<td><a href="http://www.wirmotorsports.com" target="_blank">Wisconsin International Raceway</a> 1/4 Mile 
IHRA 
Div. 5 </td>
<td>Roger Van 
Daalwyk

W 1460 County Rd KK 
Kaukauna, WI 54130 <a href="mailto:rogerwir@aol.com"><font size="1"> rogerwir@aol.com </a></td>
<td> Track:920-766-5577 
Office:920-766-5575 
FAX:920-766-5738 </td>
<td> TSI RaceNet </td>
</tr>
<tr>
<td><a name="WY"></a> Wyoming </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><a href="http://www.douglasmotorsportspark.com/" target="_blank"> Douglas Motorsports Park </a>1/4 Mile 
NHRA 
Div. 5 </td>
<td>Douglas, 
Wyo. </td>
<td> (307) 359-8376 </td>
<td> Accutime. 
Elevation: 4900' </td>
</tr>
<tr>
<td><a name="Aruba"></a> Aruba </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><a href="http://www.arubadrag.com" target="_blank"> International Raceway Park Aruba</a> 1/4 Mile 
IHRA </td>
<td>Juan Kock

Tomatenstraat 21 
Oranjestad, Aruba </td>
<td> Track: 0011297855300 </td>
<td></td>
</tr>
<tr>
<td><a name="PR"></a> Puerto Rico </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><p align="left"> Carolina Speedway 
1/4 Mile 
NHRA 
Div. 2 </p></td>
<td>Rafael Rodríguez-Collazo

Carolina, Puerto Rico </td>
<td> (787) 768-3424 </td>
<td> Formerly Caribbean Raceway Park </td>
</tr>
<tr>
<td>Manatí Race Track

1/8 Mile </td>
<td>Angel Santiago-Rivera

Road 687 
Barrio Boquillas 
Manatí, Puerto Rico </td>
<td> Track: 787-884-3736 
Office: 939-640-4948 </td>
<td></td>
</tr>
<tr>
<td><a href="http://ww.eltuque.com" target="_blank"> Ponce International Speedway Park</a> 1/4 Mile 
IHRA 
Div. 2 </td>
<td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Edison Lluch 
3330 Ponce Bypass 
Ponce, PR 000728-1510 </p></td>
<td> Track: 787-290-6006 
Office: 787-290-2000 
Fax: 787-290- 2002 </td>
<td></td>
</tr>
<tr>
<td><p align="left"> Puerto Rico 
International Speedway 
1/4 Mile 
NHRA 
Div. 2 </p></td>
<td>Puerto Rico Int'l Speedway Corp

Salinas, Puerto Rico </td>
<td> (787) 747-0319 </td>
<td></td>
</tr>
<tr>
<td><a name="Mexico"></a> Mexico </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><font id="role_document"> <a href="http://http://www.autodromoacc.com.mx" target="_blank"> Autodromo del Centro 
de Coahuila S.A de C.V. </a> IHRA 
Div. 4 </td>
<td><font size="1" face="Arial" id="role_document"> Tomas Tobias 
Promsa Carretera 57 km. 9.5 Nte., Estancias de Santa Ana C.P. 25840 
Monclova, Coahuila ,Mexico <a href="mailto:comiteorganizador@autodromoacc.com.mx"> comiteorganizador@autodromoacc.com.mx</a></td>
<td><font size="1" face="Arial" id="role_document"> Phone: 011 52 866 639 0449 </td>
<td></td>
</tr>
<tr>
<td><div> Autodromo DISA </div>
<div> 1/4 mile </div>
<div> </div></td>
<td>Cd. Acuña, 
Mexico <a href="mailto:autodromodisa@yahoo.com">autodromodisa@yahoo.com</a></td>
<td> (877) 772-6847 </td>
<td><div> ( Submitted by Ricardo Garcia:
<div> "60' concrete, asphalt and 400 
meters stopping length. suitable for 9 seconds or slower cars. 
Located across the border from Del Rio, TX.") </div>
</div></td>
</tr>
<tr>
<td>Autodromo Monterrey

1/4 mile </td>
<td>Monterrey, Mexico </td>
<td> 011-52-837-93706 </td>
<td> Elevation: 1500'. </td>
</tr>
<tr>
<td><a href="http://www.bajaraceway.com/" target="_blank"> Baja Raceway </a>1/4 Mile 
NHRA 
Div. 7 </td>
<td>Mexicali, 
B.C. ;Mexico </td>
<td> 01152 686 838 5084 </td>
<td></td>
</tr>
<tr>
<td><a name="Alberta"></a> Alberta, Canada </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><a href="http://www.castrolraceway.com/">Castrol Raceway</a> 1/4 Mile 
IHRA 
Div. 6 </td>
<td>Rob 
Reeves

7003 Girard Rd 
Edmonton, Alberta, CAN T6B 2C4
<div id="container">
<div id="wrapper_single">
<div id="content">
<div class="post" id="post-48">
<div class="entry"> <a href="mailto:info@castrolraceway.com"> info@castrolraceway.com</a> </div>
</div>
</div>
</div>
</div></td>
<td> Track:780-461-5801 
Office:780-461-5801 
FAX:780-461-5827 </td>
<td> Compulink. 
Elevation: 2200' 
Formerly Bud Park </td>
</tr>
<tr>
<td><a href="http://www.mhdra.com/" target="_blank"> MHDRA Dragstrip </a>1/4 Mile 
NHRA 
Div. 6 </td>
<td>Medicine 
Hat, Alta. ;Canada </td>
<td> (403) 548-7061 </td>
<td> Port-A-Tree. 
Elevation: 2300' </td>
</tr>
<tr>
<td><a href="http://www.racecity.com" target="_blank"> Race City Motorsport Park</a> 1/4 Mile 
IHRA 
Div. 6 </td>
<td><div align="left" style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif">
<p> 11550 - 68 St. S.E. 
Calgary, Alberta, Canada 
T2Z 3E8 <a href="mailto:thetrack@racecity.com">thetrack@racecity.com</a> </p>
</div></td>
<td><div align="left" style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> Track: 403-272-7223 
Office: 403-272-7223 
Fax: 403-236-7223 </div></td>
<td> Compulink. 
Elevation: 3500' </td>
</tr>
<tr>
<td><a href="http://tailwindraceway.net">Tailwind Raceway</a></td>
<td>PO Box 247 Fort Macleod

Alberta Canada T0L0Z0 <a href="mailto:contact@tailwindraceway.net"> contact@tailwindraceway.net</a></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="BC"></a> <font size="3" face="Arial" color="#FFFF00">British Columbia, CAN </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><a href="http://www.missionraceway.com/" target="_blank">Mission Raceway 
Park </a>1/4 Mile 
NHRA 
Div. 6 </td>
<td>Mission, 
B.C. ;Canada </td>
<td> (604) 826-6315 </td>
<td> Compulink. 
Sea-level </td>
</tr>
<tr>
<td><a href="http://www.eaglemotorplex.com/" target="_blank">Nl'akapxm Eagle 
Motorplex </a>1/4 Mile 
IHRA 
Div. 6 </td>
<td>Ashcroft, 
B.C. ;Canada </td>
<td> (250) 453-9310 </td>
<td> Compulink. 
Elevation: 1700' </td>
</tr>
<tr>
<td><p align="left"> <a href="http://www.ncmp.ca/" target="_blank">North Central Motorsports 
Park </a>1/4 Mile 
NHRA 
Div. 6 </p></td>
<td>Prince 
George, B.C. ;Canada </td>
<td> (250) 967-4130 </td>
<td> Chrondek. </td>
</tr>
<tr>
<td><a href="http://www.pgmotorsportspark.ca"> Prince George Motorsports Park</a> 1/8 
IHRA 
Div. 6 </td>
<td><strong> <span style="FONT-WEIGHT: normal">
<div style="TEXT-ALIGN: left"> 9285 Raceway Rd 
Prince George 
British Columbia 
V2K5K2 <a href="mailto:info@pgmotorsportspark.ca" class="ApplyClass"> info@pgmotorsportspark.ca</a> </div>
</span></strong>
<p></p></td>
<td><strong>
<div style="TEXT-ALIGN: left"> <span style="FONT-WEIGHT: normal">(250) 
614-3051</span> </div>
</strong>
<p></p></td>
<td></td>
</tr>
<tr>
<td><a href="http://www.thundermountainraceway.com/"> Thunder Mountain Raceway</a> 1/8 mile 
IHRA 
Div. 6
</p></td>
<td>Kelowna, 
B.C.; Canada </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="New_Brunswick"></a> New Brunswick, Canada </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a href="http://www.miramichidragway.com"> Miramichi Dragway Park</a></td>
<td>Napan, 
New Brunswick; Canada </td>
<td> 506-773-4871 
506-623-8052 </td>
<td></td>
</tr>
<tr>
<td><a name="Newfoundland"></a> Newfoundland, Canada </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.5liter.nf.net/"> Clarenville Dragway </a></td>
<td>Morley's 
Siding, NF; Canada </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Nova_Scotia"></a> Nova Scotia </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><a target="_blank" href="http://members.tripod.com/dragsite/id13.htm"> Cape Breton Dragway</a></td>
<td>NS; 
Canada <a href="mailto:dragsite@yahoo.com"> dragsite@yahoo.com</a></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a href="http://www.nsdra.ca/" target="_blank">Greenfield Dragway</a></td>
<td>Middlefield, NS; Canada </td>
<td></td>
<td></td>
</tr>
<tr>
<td><font size="1" face="Arial" color="#808080"> Maitland Dragway * 
1/4 mile </td>
<td><font size="1" face="Arial" color="#808080"> Maitland, N.S. </td>
<td></td>
<td><font size="1" face="Arial" color="#808080"> (* defunct) </td>
</tr>
<tr>
<td><a name="Manitoba"></a> Manitoba, Canada </td>
<td></td>
<td>align="left"></td>
<td>align="left"></td>
</tr>
<tr>
<td><a href="http://www.wscc.mb.ca" target="_blank"><span class="081345813-21012005"> Gimli Motorsports Park </span> </a>IHRA </td>
<td><div style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> <span class="081345813-21012005"> Steve 
Maillard 
51 N Lake St </span> </div>
<div style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> <span class="081345813-21012005"> Gimli, 
Manitoba R0C - 1B0 </span> </div></td>
<td> Phone: 204-642-6697 </td>
<td> formerly Viking Int'l Raceway. 
TSI </td>
</tr>
<tr>
<td><a name="Prince_Edward"></a> Prince Edward Isl., CAN </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>Raceway Park </td>
<td>Oyster Bed Bridge </td>
<td></td>
<td></td>
</tr>
<tr>
<td><font size="3" face="Arial" color="#FFFF00"><a name="Ontario"></a> Ontario, Canada </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a href="http://www.grandbendmotorplex.com" target="_blank"> Grand Bend Motorplex </a>1/4 Mile 
IHRA 
Div. 3 </td>
<td>Ron Biekx

P.O. Box 668 
Grand Bend, ONT N0M1T0 <a href="mailto:motorplx@hay.net">motorplx@hay.net</a></td>
<td> Track:519-238-7223 
Office:519-238-7223 
FAX:519-238-5299 </td>
<td> RACED (Won Nat'l)
Won my second national event here. Awesome hookin' track! 
Watch the wind, and take your weather station with a grain of salt. 
Tough to dial, as it doesn't always do what the weather indicates. 
Compulink. </td>
</tr>
<tr>
<td><a href="http://www.stthomasracewaypark.com/" target="_blank">St. Thomas 
Raceway Park</a> 1/4 Mile 
IHRA 
Div. 3 </td>
<td><p style="FONT-SIZE: 11px; FONT-FAMILY: Arial, Helvetica, sans-serif"> 45633 Sparta Line 
Sparta, ONT NOL 2HO <strong style="font-weight: 400"> <a href="mailto:info@stthomasracewaypark.com"> info@stthomasracewaypark.com</a></strong></p></td>
<td> Track: <span class="style18"><strong>519-775-0001</strong></span></td>
<td> Chrondek. </td>
</tr>
<tr>
<td><a href="http://www.torontomotorsportspark.com/" target="_blank"> Toronto Motorsports Park</a> 1/4 Mile 
IHRA 
Div. 3 </td>
<td>Lorraine 
Bergstrand

1040 Kohler Road 
Cayuga, ONT NOA IEO <a href="mailto:lorraine@torontomotorsportspark.com"> lorraine@torontomotorsportspark.com</a></td>
<td> Track:905-772-0303 
Office:905-772-0303 
FAX:905-772-1380 </td>
<td> RACED (Won Nat'l)
Won my first national event here in '03, and backed it up in '04! 
Completely redone just a few years ago. Repaved, concrete 
guardwall, new tower. Rollout may be a little loose. Slight dip in 
the left lane. Modern restrooms, with showers. 
Includes a road course! Compulink. Turbo Blue fuel. </td>
</tr>
<tr>
<td><a name="Quebec"></a> Quebec, Canada </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a href="http://www.luskvilledragway.com" target="_blank"> Luskville Dragway</a> 1/4 Mile 
NHRA 
Div. 1 </td>
<td>Luskville, Que. </td>
<td> (613) 224-1162 </td>
<td> Chrondek. </td>
</tr>
<tr>
<td><a href="http://www.napiervilledragway.com/" target="_blank"> Napierville Dragway</a> 1/4 Mile 
NHRA 
Div. 1 </td>
<td>Napierville, Que. </td>
<td> (450) 245-7656 </td>
<td></td>
</tr>
<tr>
<td>Sanair 
Int'l Dragstrip

1/4 mile </td>
<td>St. Pie, Que. </td>
<td> 514-772-6400 </td>
<td></td>
</tr>
<tr>
<td><a name="Saskatchewan"></a> Saskatchewan, CAN </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a href="http://www.racesir.com/" target="_blank"> Saskatchewan International Raceway </a>1/4 Mile 
NHRA 
Div. 6 </td>
<td>Saskatoon, Sask. ;Canada </td>
<td> (306) 955-3724 </td>
<td> Chrondek. 
Elevation: 1600' </td>
</tr>
<tr>
<td><a name="UK"> <font size="3" face="Arial" color="#FFFF00">United Kingdom </a></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a href="http://www.crailraceway.co.uk/"> Crail Raceway</a></td>
<td><font class="bodytext"> Crail Airfield 
Promotions
Crail Airfield 
Balcomie Road 
Crail 
Scotland 
KY10 3XL <font color="#cccccc" class="bodytext"> <a href="mailto:enquiries@crailthrash.co.uk"> enquiries@crailthrash.co.uk</a></td>
<td><font size="1" face="Arial" class="bodytext">Tel: 01333 451 836 
Fax: 01333 451 842 </td>
<td></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.northwealdairfield.org/Campaign/Latest_News/Events/events.html"> North Weald Airfield </a></td>
<td>North Weald Airfield Users Group <font color="#000000"> 34 Lancaster Road
North Weald 
Essex, CM16 6JA <a href="mailto:info@northwealdairfield.org"> info@northwealdairfield.org </a></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.shakespearecountyraceway.com/"> Shakespere County Raceway </a></td>
<td><p align="left"> Long Marston Airfield 
Long Marston 
Stratford upon Avon 
Warwickshire 
CV37 8LL <a href="mailto:enquiries@shakespearecountyraceway.co.uk" class="bodylink"> enquiries@shakespearecountyraceway.co.uk </a> </p></td>
<td> Tel: +44(0) 1789 414119 </td>
<td></td>
</tr>
<tr>
<td><a target="_blank" href="http://www.santapod.co.uk/"> Santa Pod Raceway </a></td>
<td><span class="copy"> Santa Pod Raceway 
Airfield Road 
Podington 
Wellingborough 
Northants 
NN29 7XA 
England </span></td>
<td><span class="copy_bold"> Tel:</span><span class="copy"> 01234 782828 </span><span class="copy_bold">Fax:</span><span class="copy"> 01234 
782818 

International Numbers: 
Tel. +44 1234 782828 
Fax: +44 1234 782818</span></td>
<td> TSI RaceNet </td>
</tr>
<tr>
<td><a target="_blank" href="http://www.york-raceway.co.uk/"> York Raceway </a></td>
<td>Melbourne Airfield

YO42 4SS </td>
<td> T: 
01422 843651 
F: 01422 844171 
E: <a href="mailto:yorkdragway@btinternet.com"> yorkdragway@btinternet.com</a></td>
<td></td>
</tr>
<tr>
<td><a name="AUS"> <font size="3" face="Arial" color="#FFFF00">Australia </a></td>
<td> (Thanks to <a target="_blank" href="http://dragsdownunder.tripod.com"> Ralph Smith </a> ) </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Adelaide" href="http://www.geocities.com/MotorCity/Garage/8034/"> Adelaide International Raceway </a></td>
<td>Port Wakefield Rd, Virginia, South 
Australia, Australia

PO Box 521, Virginia, SA, 5120 </td>
<td> Phone (08) 8280 8154 
Fax (08) 8280 6903 
Info Line 1900 937 445 </td>
<td></td>
</tr>
<tr>
<td><a name="Aerodrome"> 7 Mile Aerodrome 
Raceway </a> 1/8th mile </td>
<td>Alice Springs airport

Old South Rd, Alice Springs, Northern Territory, Australia 
Central Australian Drag Racing Association 
PO Box 434, Alice Springs, NT, 0871 </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Bairnsdale"> Bairnsdale Dragway </a> 1/8th mile </td>
<td>Bairnsdale Aerodrome, Aerodrome Rd, 
Bairnsdale, Victoria, Australia

Bairnsdale Motorsports Club 
PO Box 107, Bairnsdale, VIC, 3875 </td>
<td> Phone John Fletcher 
(A/H) (03) 5156 8441 </td>
<td></td>
</tr>
<tr>
<td><a name="Ballarat" href="http://www.ballaratdrags.com"> Ballarat Airport </a> 1/8th mile </td>
<td>Ballarat Airport, Ballarat, Victoria, 
Australia

Ballarat Drag Racing Club<a href="mailto:leswaight@bigpond.com.au"> leswaight@bigpond.com.au</a></td>
<td> Phone 0418 518 164 </td>
<td></td>
</tr>
<tr>
<td><a name="Benaraby" href="http://www.benarabyraceway.com"> Benaraby Raceway </a></td>
<td>Marawing Rd, Gladstone,

Queensland, Australia 
PO Box 1348, Gladstone, QLD, 4680 </td>
<td> Phone 
(07) 4978 1613 
(07) 4972 2407 
0419 703 710(track) </td>
<td></td>
</tr>
<tr>
<td><a name="Bodangora"> Bodangora Airport </a></td>
<td>Bodangora Airport, Bodangora, New South 
Wales, Australia

Dubbo City Car Club 
PO Box 1054, Dubbo, NSW, 2830 <a href="mailto:dubbocarclub@hotmail.com">Dubbo City Car Club</a></td>
<td> Phone 0427 843588 
Fax (02) 6884 5383 </td>
<td> (Also known as Wellington airport) </td>
</tr>
<tr>
<td><a name="Calder" href="http://www.motorsport.com.au/index.htm"> Calder Park Raceway </a></td>
<td>Calder Freeway, Keilor, Melbourne, 
Victoria, Australia

Locked Bag 7, Sunbury, VIC, 3429 </td>
<td> Phone (03) 9217 8800 
Fax (03) 9217 8960 
Info Line 1900 937 445 </td>
<td></td>
</tr>
<tr>
<td><a name="Carnarvon"> Carnarvon </a></td>
<td>Carnarvon Airport, Carnarvon Rd, 
Carnarvon, Western Australia, Australia

Gascoyne Drag Racing Club 
PO Box 630, Carnarvon, WA, 6701 </td>
<td> Phone Dallas Wood-Harris 
(08) 9941 8084 </td>
<td></td>
</tr>
<tr>
<td><a name="Carnell"> Carnell Raceway </a> 1/8th mile </td>
<td>Rifle Range Rd, Stanthorpe, Queensland, 
Australia

Stanthorpe and District Sporting Car Club 
PO Box 83, Stanthorpe, QLD, 4380 </td>
<td> Phone (07) 4681 2577 (Track) </td>
<td></td>
</tr>
<tr>
<td><a name="Casterton"> Casterton </a></td>
<td>Henty Highway, Casterton, Victoria, 
Australia

Casterton Street Drags Club 
Owen Stephens 
61 Russell St, Casterton, VIC, 3311 </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Collie" href="http://www.motoringsouthwest.org.au"> Collie Motorplex </a> 1/8th mile </td>
<td>Collie, Western Australia, Australia </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Coonawarra"> Coonawarra </a> (closed road) </td>
<td>Lynch Rd, Coonawarra, South Australia, 
Australia

South East Drag Racing Association 
PO Box 886, Naracoorte, SA, 5271 </td>
<td> Phone Alex Van Leuven 
(08) 8762 3470 </td>
<td></td>
</tr>
<tr>
<td><a name="Cowra"> Cowra </a> 1/8th mile </td>
<td>Cowra, New South Wales, Australia

Cowra Police &amp; Community Youth Club 
PO Box 346, Cowra, NSW, 2794 </td>
<td> Phone Jeff Barnes 
(02) 6342 4646 (BH) </td>
<td></td>
</tr>
<tr>
<td><a name="Eastern_Creek" href="http://www.eastern-creek-raceway.com"> Eastern Creek Raceway </a></td>
<td>Horsley Road, Eastern Creek, Sydney, New 
South Wales, Australia

PO Box 6747, Delivery Centre, Blacktown, NSW, 2148 </td>
<td> Phone (02) 9672 1000 </td>
<td></td>
</tr>
<tr>
<td><a name="Heathcote" href="http://www.heathcoteraceway.com.au"> Heathcote Park </a> 1/4 Mile 
IHRA </td>
<td>Barnadown Rd, off McIvor Hwy, Heathcote, 
Victoria, Australia

Russell Clarke 
C/- Heathcote Park, Bullarto Rd, Cardinia, VIC, 3978 </td>
<td> Phone Joanne 0416 072 478 
(03) 5439 1288 (Track) 
Ans/Fax (03) 5998 7503 </td>
<td></td>
</tr>
<tr>
<td><a name="Hidden_Valley" href="http://www.hiddenvalleydrags.com"> Hidden Valley International Dragstrip </a></td>
<td>Tiger Brehan Drive, Darwin, Northern 
Territory, Australia

King Cobra Rod and Custom Club 
GPO Box 3726, Darwin, NT, 0801 </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Innisfail"> Innisfail Aerodrome </a> 1/8th mile </td>
<td>Innisfail, Queensland, Australia

Mundoo Drag Racing Association 
46 Rushworth Rd, Gordonvale, QLD, 4865 </td>
<td> Phone 
Les Hicks (07) 4056 3037 
Fax (07) 4051 3891 </td>
<td></td>
</tr>
<tr>
<td><a name="Ironbark" href="http://www.swdra.hwy54.com.au/"> Ironbark Raceway </a></td>
<td>Warrego Highway, Roma, Queensland

South West Drag Racing Association 
P.O. Box 1034, Roma, QLD, 4455 </td>
<td> Phone 
Randall Mohr (07) 4622 5188 </td>
<td></td>
</tr>
<tr>
<td><a name="Lakeside"> Lakeside 
International Raceway </a></td>
<td>Lakeside Road, Kurwongbah, Queensland, 
Australia

PO Box 239, Kallangur, QLD, 4503 </td>
<td> Phone (07) 3285 3333 
Fax (07) 3285 3304 </td>
<td></td>
</tr>
<tr>
<td><a name="Latrobe"> Latrobe Regional 
Airport </a> 1/8th mile </td>
<td>Airfield Rd, Traralgon, Victoria, 
Australia

Latrobe Drag Racers 
PO Box 121, Morwell, VIC, 3840 </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Leeton" href="http://www.rdra.itgo.com"> Leeton </a></td>
<td>Leeton Airport, Leeton, New South Wales, 
Australia

Riverina Drag Racers Association 
C/O Farm 103, Grevillia Street, Leeton, NSW, 2705 </td>
<td> Phone 
Carlo Luisi (02) 6953 5535 </td>
<td></td>
</tr>
<tr>
<td><a name="Newman" href="http://www.newmandrags.org.au"> Newman Airport </a></td>
<td>Great Northern Highway, Newman, Western 
Australia, Australia

Rachel Helsby (Newman Mainstreet Project Coordinator) 
Newman Mainstreet Project, PO Box 618, Newman, WA, 6753 <a href="mailto:newmanmainst@westnet.com.au"> newmanmainst@westnet.com.au</a></td>
<td> Phone/Fax (08) 9177 8290 </td>
<td></td>
</tr>
<tr>
<td><a name="Palmyra" href="http://www.palmyradragway.com.au"> Palmyra Dragway </a></td>
<td>Bells Road, Palmyra, Queensland, Australia

Palmyra Drag Racing Club 
PO Box 740, Mackay, QLD, 4740 </td>
<td> Phone 0418 186 484 
Fax (07) 4952 2762 </td>
<td></td>
</tr>
<tr>
<td><a name="Kwinana" href="http://www.motorplex.com.au/"> Perth Motorplex </a></td>
<td>Cnr Anketell &amp; Rockingham Roads

Kwinana Beach, Perth, Western Australia, Australia 
PO Box 241, Kwinana, WA, 6966 </td>
<td> Phone (08) 9383 2622 
Fax (08) 9439 4488 
Info Line 1902 291 500 </td>
<td> Formerly Quit Motorplex, Kwinana Motorplex </td>
</tr>
<tr>
<td><a name="South_Coast" href="http://www.southcoastraceway.com/"> South Coast Raceway </a> 1/8th mile </td>
<td>Portland, Victoria, Australia

South Coast Drag Racing Association 
PO Box 734, Portland, VIC, 3305 </td>
<td> Phone 
Tim Parker 0408 835 270 
Dean Cleary 0407 525 329 </td>
<td></td>
</tr>
<tr>
<td><a name="Steel_City" href="http://www.steelcitydrags.com/"> Steel City Raceway </a> 1/8th mile </td>
<td>Whyalla, South Australia, Australia

Steel City Drag Club 
PO Box 336, Whyalla, SA, 5600 </td>
<td> Phone (08) 8645 3902 </td>
<td></td>
</tr>
<tr>
<td><a name="Sunset_Strip" href="http://www.sunsetstrip.org.au"> Sunset Strip </a> 1/8th mile </td>
<td>Benetook Ave, Koorlong, Mildura, Victoria, 
Australia

Sunraysia Drag Racing Association 
PO Box 113, Mildura, VIC, 3502 </td>
<td> Phone 0419 883 057 </td>
<td></td>
</tr>
<tr>
<td><a name="Tamworth"> Tamworth </a> 1/8th mile 
(closed road) </td>
<td>Goddards Lane, Tamworth, New South Wales, 
Australia

Tamworth Drag Racing Association 
PO Box 144, Tamworth, NSW, 2340 </td>
<td> Phone 
Steve Murden (02) 6766 5977 (AH) 
or 0422 104 859 </td>
<td> home of drag racing around Tamworth, NSW. </td>
</tr>
<tr>
<td><a name="Tarmak" href="http://www.tarmakdragway.net/"> Tarmak Dragway </a></td>
<td>Powranna Road, Powranna, Launceston, 
Tasmania, Australia

25 Murphy St, Invermay, Launceston, Tas, 7248 </td>
<td> Phone (03) 6334 5561 </td>
<td></td>
</tr>
<tr>
<td><a name="Tennant"> Tennant Creek Airport </a> 1/8th mile </td>
<td>Stuart Highway, Tennant Creek, Northern 
Territory, Australia </td>
<td> Phone 
Mick Adams (08) 8962 3112 </td>
<td></td>
</tr>
<tr>
<td><a name="Townsville" href="http://www.townsvilledragway.com"> Townsville Dragway </a></td>
<td>Shaws Road, Bohle, Townsville, Queensland, 
Australia

PO Box 563, Aitkenvale, Townsville, QLD, 4814 </td>
<td> Phone (07) 4779 6879 
0412 780 838 
Fax (07) 4775 1836 </td>
<td></td>
</tr>
<tr>
<td><a name="Waikerie" href="http://www.riverlandmotorsportclub.com.au/"> Waikerie Airport </a> 1/8th mile </td>
<td>Waikerie, SA, Australia </td>
<td> Phone Angelo 
(08) 8588 2169 
0409 441 771 </td>
<td></td>
</tr>
<tr>
<td><a name="Warracknabeal"> Warracknabeal 
Aerodrome </a> 1/8th mile </td>
<td>Henty Highway, Warracknabeal, Victoria, 
Australia

Wimmera Off Street Drag Racing Club </td>
<td> Phone 
Simon Pearson 0407 803 445 </td>
<td></td>
</tr>
<tr>
<td><a name="Warrnambool" href="http://www.warrnambooldrags.com"> Warrnambool City Dragway </a> 1/8th mile </td>
<td>Warrnambool, Victoria, Australia

Warrnambool and District Drag Racing Association 
PO Box 292, Warrnambool, VIC, 3280 </td>
<td> Phone 
Darryl Porter 0418 529 707 
Darren McCosh 0407 367 737 </td>
<td></td>
</tr>
<tr>
<td><a name="Warwick" href="http://www.warwickdragway.com"> Warwick Raceway </a></td>
<td>Morgan Park, Old Stanthorpe Rd, Warwick, 
Queensland, Australia

Warwick District Drag Racing Association 
PO Box 421, Warwick, QLD, 4370 </td>
<td> Phone 
Chris Loy (07) 4661 5942 
Bob Reid (07) 4661 1054 </td>
<td></td>
</tr>
<tr>
<td><a name="Western_Sydney" href="http://www.wsid.com.au/"> Western Sydney International Dragway </a></td>
<td>Ferrers Rd, Eastern Creek, Sydney, New 
South Wales, Australia

PO Box 6306 
Wetherill Park DC, NSW, 1851 </td>
<td> Phone (02) 9672 1320 
Fax (02) 9672 1122 </td>
<td></td>
</tr>
<tr>
<td><a name="Wilby" href="http://www.wilbypark.com.au/"> Wilby Park </a> 1/8th mile </td>
<td>Wilby, Victoria, Australia

Wilby Motor Sports Club </td>
<td> Phone 
Charles Hammond 0413 004 710 </td>
<td></td>
</tr>
<tr>
<td><a name="Willowbank" href="http://www.willowbank-raceway.com.au"> Willowbank Raceway </a></td>
<td>Cunningham Highway, Willowbank, 
Queensland, Australia

PO Box 536, Mt. Gravatt, QLD, 4122 </td>
<td> Phone 
(07) 3849 6881 (Office) 
(07) 5467 3255 (Track) </td>
<td><a href="http://www.willowbank-raceway.com.au/webcam.htm"> Willowbank stripcam</a> Updated picture every 2 minutes during meetings </td>
</tr>
<tr>
<td><font face="Arial" color="#FFFF00"><a name="NZ"></a>New Zealand </td>
<td> (Thanks to <a target="_blank" href="http://dragsdownunder.tripod.com"> Ralph Smith </a> ) </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Upper_Hutt" href="http://www.westernbays.co.nz/upperhuttdrags.html"> Alexander Rd </a> 1/8th mile 
(closed road) </td>
<td>Alexander Road

Upper Hutt, New Zealand </td>
<td> Promoted by two Hot Rod Clubs 
Capital Rodders Inc 
PO Box 50 506, Porirua, 
Wellington, New Zealand 
and <a href="http://www.westernbays.com/">Western Bays Street Rodders</a> P.O.Box 57007, Paremata, 
Wellington, New Zealand. <a href="mailto:western_bays_street_rodders@hotmail.com"> western_bays_street_rodders@hotmail.com</a></td>
<td></td>
</tr>
<tr>
<td><a name="Amisfield"> Amisfield Airport </a></td>
<td>State Highway 1, Tokoroa, New Zealand

Street Rodders Inc 
PO Box 669, Tokoroa, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Blenheim"> Bristol St </a> (closed road) </td>
<td>Bristol St, Riverlands,

Blenheim, New Zealand 
Sun Valley Roadsters 
PO Box 507, Blenheim, New Zealand. Email: <a href="mailto:ljdick@ndt.co.nz"> Les Dick</a></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Levin"> Cambridge St Sth </a> (closed road) </td>
<td>Cambridge St Sth, Levin, New Zealand

Tararua Rodders Inc 
PO Box 252, Levin, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Mavillard"> De Mavillard Drive </a> (closed road) </td>
<td>De Mavillard Drive,

New Plymouth, New Zealand 
New Plymouth Rodders Inc 
PO Box 273, New Plymouth, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Champion" href="http://www.championdragway.co.nz">FRAM Autolite 
Dragway</a> (Formerly Champion Dragway) </td>
<td>State Highway 1, Meremere, New Zealand

Pukekohe Hot Rod Club 
PO Box 552, Pukekohe, New Zealand. Email: <a href="mailto:championdragway@xtra.co.nz"> PHRC</a></td>
<td> Phone (09) 238 5564 
Fax (09) 238 5538 </td>
<td></td>
</tr>
<tr>
<td><a name="Lakeside" href="http://www.lid.co.nz"> Lakeside International Dragway </a></td>
<td>Taupo, New Zealand

Broadlands Rd, Taupo, New Zealand 
Phone 0236 5516 </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Levels"> Levels Raceway </a></td>
<td>Faheys Rd,

Timaru, New Zealand 
Tornado Rod &amp; Custom Club Inc 
PO Box 738, Timaru, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Masterton" href="http://www.mastertonmotorplex.co.nz"> Masterton Motorplex </a></td>
<td>Hood Aerodrome

Masterton, New Zealand 
C/- 3 Crawford Grove, 
Lower Hutt, New Zealand </td>
<td> Phone (04) 527 8577 bus 
(04) 938 0107 a/hrs </td>
<td></td>
</tr>
<tr>
<td><a name="Nelson" href="http://www.geocities.com/knaffed/drag.html"> Nelson Airport </a></td>
<td>Inland Highway, Motueka,

Nelson, New Zealand 
Nelson Drag Racing Association 
PO Box 7030, Nelson, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Whangarei"> Okara Drive </a> (closed road) </td>
<td>Okara Drive,

Whangarei, New Zealand 
Whangarei Rod and Custom Club 
PO Box 987, Whangarei, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Otepuni"> Otepuni Ave </a> (closed road) </td>
<td>Otepuni Ave,

Invercargill, New Zealand 
Street Machines (Southland) Inc 
PO Box 1173, Invercargill, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Port"> Port Road </a> (closed road) </td>
<td>Port Rd,

Wellington, New Zealand 
Cam County Inc 
PO Box 31 055, Lower Hutt, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Dunedin"> Princess St </a> (closed road) </td>
<td>Princess St,

Dunedin, New Zealand 
Con Rodders Inc 
PO Box 5428, Dunedin, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Ruapuna" href="http://www.pegasusbaydrags.co.nz"> Ruapuna Raceway </a></td>
<td>Hasketts Rd, Templeton,

Christchurch, New Zealand 
Pegasus Bay Drag Racing Club 
PO Box 1573, Christchurch, New Zealand. Email: <a href="mailto:pbdrc.dragracing@xtra.co.nz"> PBDRC</a></td>
<td> Phone (03) 349 4991 
Fax (03) 385 3528 </td>
<td></td>
</tr>
<tr>
<td><a name="Wanganui" href="http://www.wanganui.com/hotrods/"> Taupo Quay </a> (closed road) </td>
<td>Taupo Quay,

Wanganui, New Zealand 
Wanganui Road Rodders 
PO Box 476 Wanganui, New Zealand </td>
<td></td>
<td></td>
</tr>
<tr>
<td><a name="Teretonga" href="http://www.southerndragways.co.nz/"> Teretonga Raceway </a></td>
<td>Domain Rd,

Invercargill, New Zealand </td>
<td> Phone (03) 215 9196 </td>
<td></td>
</tr>
<tr>
<td><font face="Arial"><a name="Carribean"></a> Carribean </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a href="http://racecayman.tripod.com/">Breakers Speedway</a></td>
<td><font size="1" face="Arial" color="#000000" id="role_document9">
<div> Grand Cayman 
Owner / Robert Campbell</div></td>
<td><font size="1" face="Arial" color="#000000" id="role_document9"> 345-916-2222 </td>
<td><font size="1" face="Arial" color="#000000" id="role_document10">
<div> 1/8th mile all concrete</div>
<div> Porta tree timing system</div></td>
</tr>
<tr>
<td><a href="http://www.drag-races.com/Tracks_Curacao.html"> Curacao Int'l Raceway</a></td>
<td>Rondeklip z/n

Curacao 
Netherlands Antilles </td>
<td> Phone: 599-9-7374040 
FAX: 599-9-7371331 </td>
<td> Compulink. </td>
</tr>
<tr>
<td><a name="South_Africa"></a> South Africa </td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td><a href="http://www.homestead.com/Tarlton/"> Tarlton Int'l Raceway </a></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>
