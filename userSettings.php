<?php
if(isset($_GET['pwdchg'])) {$pwdchg=true;}else{$pwdchg=false;}
if(isset($_GET['match'])) {$match=true;}else{$match=false;}
if(isset($_GET['old'])) {$old=true;}else{$old=false;}
if(isset($_GET['blank'])) {$blank=true;}else{$blank=false;}
if(isset($_GET['short'])) {$short=true;}else{$short=false;}
if(isset($_GET['name'])) {$name=true;}else{$name=false;}
if(isset($_GET['emailaddy'])) {$emailaddy=true;}else{$emailaddy=false;}
if(isset($_GET['alerts'])) {$alerts=true;}else{$alerts=false;}
if(isset($_GET['error'])) {$error=true;}else{$error=false;}

ob_start();
require("mysqlInfo.php");
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");
//$result = mysql_query("SELECT * FROM `customers` ORDER BY customer_name ASC");
$resultcarrier = mysql_query("SELECT * FROM `carrier` ORDER BY company ASC");
$resultUser = mysql_query("SELECT * FROM `users` WHERE `uid` = '".$_COOKIE["mm1"]."' limit 1");
$rowUser = mysql_fetch_array($resultUser);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/form.css" />
<script type="text/javascript">
function pwdchg()
{
	<?php 
		if($pwdchg==true){
			echo "alert(\"The password has been changed !\\n You will now be logged out so the\\n password change can take effect\");\n";
			echo "location.href='logout.php'; \n";
		}
		if($match==true){
			echo "alert(\"The password do not match !\");\n";
		}
		if($old==true){
			echo "alert(\"The password old password is incorrect !\");\n";
		}
		if($blank==true){
			echo "alert(\"The password cannot be blank !\");\n";
		}	
		if($short==true){
			echo "alert(\"The new password is to short. min 6 letters and or numbers !\");\n";
		}
		if($name==true){
			echo "alert(\"The users displayed name has been changed\");\n";
		}
		if($emailaddy==true){
			echo "alert(\"The users email has been changed\");\n";
		}
		if($alerts==true){
			echo "alert(\"The users alerts has been changed\");\n";
		}
		if($error==true){
			echo "alert(\"There was an sql error submitting the form\");\n";
		}
	 ?>
}
/////////////////////////////////////////////////////////////////////////////////////
function showhidesms(){
    if(document.getElementById("sms").checked){
		document.getElementById("smsinfo").style.display = "block";	
	}else{
		document.getElementById("smsinfo").style.display = "none";
		document.getElementById("cell").value = "";
		document.getElementById("carrier").selectedIndex = 0;		
	}
}

/////////////////////////////////////////////////////////////////////////////////////
function nameSubmit(){
	//alert("Name Change");
	if (document.getElementById("name1").value == null || document.getElementById("name1").value == ""){
		alert("Display Name cannot be blank");
	}else{
		document.getElementById("namechange").submit()
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function emailSubmit(){
	if (document.getElementById("email1").value != document.getElementById("email2").value){
		alert("New Email addresses do not match");
	}else{
		if(document.getElementById("email1").value == ""){
			 alert("Email Cannot be blank");
		}else{
			checkEmail(document.getElementById("email1").value);
			if(document.getElementById("emailcheck").value != ""){
				alert("Email not changed\n" + document.getElementById("emailcheck").value);
			}else{
				document.getElementById("emailchange").submit()
			}
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function alertsSubmit(){

	if (document.getElementById("sms").checked && document.getElementById("cell").value == ""){
		alert("Cell number cannot be blank");
	}else{
		if (document.getElementById("sms").checked && document.getElementById("carrier").selectedIndex == 0){
			alert("Cell Carrier Cannot be blank");	
		}else{
			checkCell(document.getElementById("cell").value);
			if(document.getElementById("cellcheck").value != "" && document.getElementById("sms").checked){
				alert("Alerts not saved\n" + document.getElementById("cellcheck").value);
			}else{
				//alert("Alerts Change");
				document.getElementById("alertschange").submit()
			}
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
function checkCell(str){
if (str=="")
  {
  //document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		//var emailAdd = $(xmlhttp.responseText).text().replace(/ /g,'');
	  if(xmlhttp.responseText.replace(/ /g,'') == "1"){
		  //alert("The email address is an invalid format");
		  document.getElementById("cellcheck").value = "The cell number format is invalid e.g. 123-123-1234";
	  }else{
		if(xmlhttp.responseText.replace(/ /g,'') != "0"){
			//alert("The email address already exsists for user(s) " + xmlhttp.responseText);
			document.getElementById("cellcheck").value = "The cell number already exsists for user(s) " + xmlhttp.responseText;
		}else{
			document.getElementById("cellcheck").value = "";
		}
	  }
    }
  }
xmlhttp.open("GET","editCheckCell.php?q=q="+str+"&x="+ document.getElementById("uid").value ,false);
xmlhttp.send();
	
}
/////////////////////////////////////////////////////////////////////////////////////
function checkEmail(str){
if (str=="")
  {
  //document.getElementById("txtUser").innerHTML=" ";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		//var emailAdd = $(xmlhttp.responseText).text().replace(/ /g,'');
	  if(xmlhttp.responseText.replace(/ /g,'') == "2"){
		  //alert("The email address is an invalid format");
		  document.getElementById("emailcheck").value = "The email address is an invalid format";
	  }else{
		if(xmlhttp.responseText.replace(/ /g,'') != "0"){
			//alert("The email address already exsists for user(s) " + xmlhttp.responseText);
			document.getElementById("emailcheck").value = "The email address already exsists for user(s) " + xmlhttp.responseText;
		}else{
			document.getElementById("emailcheck").value = "";
		}
	  }
    }
  }
xmlhttp.open("GET","editCheckEmail.php?q="+str+"&x="+ document.getElementById("uid").value ,false);
xmlhttp.send();     	
	
}

</script>

</head>
<body onload="pwdchg()">
<div id="center-x">
  <div id="header"></div>
  <div id="menu"><?php if(isset($_COOKIE["mm2"])){include("menu/manageMenu.php");}else{include("menu/menu.php");}?></div>
  <br>
  <div class="bodytext" style="margin:15px;margin-top:5px;">
    <div id="main-box" style="padding-left:30px;"></div>


<div id="stylized" class="myform">
<form id="pwdchange" name="pwdchange" method="post" autocomplete="off" action="pwdChange.php">
<h1>Change Password</h1>

<label>Old Password<span class="small" id="txtSite" style='color:#FF0000'>Required</span></label>
<input type="password" name="password0" id="password0" />

<label>New Password<span class="small" id="txtSite" style='color:#FF0000'>Required</span></label>
<input type="password" name="password1" id="password1" />

<label>New Password Again<span class="small" id="txtSite" style='color:#FF0000'>Required</span></label>
<input type="password" name="password2" id="password2" />

<input name="Submit" type="button" value="Change Password" class="button" onclick="document.forms['pwdchange'].submit(); return false;" />
<div class="spacer"></div>
</form>
</div>

<br />
<div class="line"></div>
<br />

<div id="stylized" class="myform">
<form id="namechange" name="namechange" method="post" autocomplete="off" action="nameChange.php">
<h1>Change Displayed Name</h1>

<label>Old Name</label>
<input type="text" name="name0" id="name0" value="<?php echo $rowUser['name']; ?>" readonly />

<label>New Name<span class="small" id="txtSite" style='color:#FF0000'>Required</span></label>
<input type="text" name="name1" id="name1" />

<input name="Submit" type="button" value="Change Displayed Name" class="button" onclick="nameSubmit(); return false;" />
<div class="spacer"></div>
</form>
</div>

<br />
<div class="line"></div>
<br />

<div id="stylized" class="myform">
<form id="emailchange" name="emailchange" method="post" autocomplete="off" action="emailChange.php">
<h1>Change Email</h1>

<label>Old Email</label>
<input type="text" name="email0" id="email0" value="<?php echo $rowUser['emailaddy']; ?>" readonly />

<label>New Email<span class="small" id="txtSite" style='color:#FF0000'>Required</span></label>
<input type="text" name="email1" id="email1" />

<label>New Email Again<span class="small" id="txtSite" style='color:#FF0000'>Required</span></label>
<input type="text" name="email2" id="email2" />

<input name="Submit" type="button" value="Change Email" class="button" onclick="emailSubmit(); return false;" />
<div class="spacer"></div>
</form>
</div>
<br />
<div class="line"></div>
<br />
<div id="stylized" class="myform">
<form id="alertschange" name="alertschange" method="post" autocomplete="off" action="alertsChange.php">
<h1>Change Alerts</h1>

<label>Email Alerts<span class="small">Alarms/Critical Alerts</span></label>
<input type="checkbox" name="email" id="email" value="Y" <?php if(strtolower($rowUser['email']) == "y"){echo "checked='checked'"; } ?> />

<label>SMS Alerts<span class="small">Critical Alerts Only</span></label>
<input type="checkbox" name="sms" id="sms" value="Y" <?php if(strtolower($rowUser['sms']) == "y"){echo "checked='checked'"; } ?> onChange="showhidesms()"/>

<div id="smsinfo" <?php if(strtolower($rowUser['sms']) != "y"){echo "style=\"display:none\""; } ?>>
          <label>Cell Number <span class="small">Required for sms alarms</span></label>
          <input name="cell" type="text" id="cell" value="<?php echo $rowUser['cell'] ?>" />
          <label class="red">Carrier <span class="small">Users Cell Carrier</span></label>
          <select name="carrier" id="carrier">
            <option value=""></option>
            <?php
while($row = mysql_fetch_array($resultcarrier))
  {
	echo "<option value='" . $row['carrierid'] . "'";
	if($row['carrierid'] == $rowUser['carrier']){echo "selected=selected";}
	echo ">" . $row['company'] . "</option>\n";  
	  
    //echo "<option value='" . $row['carrierid'] . "'>" . $row['company'] . "</option>\n";
  }
?>
          </select>
        </div>

<input name="Submit" type="button" value="Change Alerts" class="button" onclick="alertsSubmit(); return false;" />
<input type="hidden" name="uid" id="uid" value="<?php echo $rowUser['uid'];  ?>" />
<input type="hidden" name="emailcheck" id="emailcheck" value="" />
<input type="hidden" name="cellcheck" id="cellcheck" value="" />

<div class="spacer"></div>
</form>
</div>
</div>
<br>


<div id="footer"></div>
</div>

</body>
</html>
