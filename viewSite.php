<?php
if (isset($_COOKIE["mm1"]))
  {
  echo "";
  }
else
  {
  header("location:index.php");
  }

if(isset($_GET['site_id'])) {$site_id=$_GET['site_id'];}else{header("location:error.php?e=invalid site id");}
ob_start();
$settings = new SimpleXMLElement('settings.xml', null, true);
$view_site_days = $settings->view_site_days;


require("mysqlInfo.php");
// Connect to server and select databse.
mysql_connect("$host", "$username", "$password")or die("cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");

$sql="SELECT * FROM `sites` WHERE `site_id`='" . $site_id . "'";
$resultSite=mysql_query($sql);
$rowSite = mysql_fetch_array($resultSite);
$mac = strtolower($rowSite['mac']);
$sql="SELECT AVG(He) FROM `" . $mac . "` where date(`date`) > (current_date() - interval " . $view_site_days . " day) order by `index` asc";
//$sql="SELECT AVG(He) FROM `5c-86-4a-00-11-35` where date(`date`) > (current_date() - interval " . $view_site_days . " day)";
$resultAvg=mysql_query($sql);
$rowAvg = mysql_fetch_array($resultAvg);
$avg=mysql_result($resultAvg, 0);
$sql="SELECT * FROM `" . $mac . "` where date(`date`) > (current_date() - interval " . $view_site_days . " day) order by `index` asc";
$result=mysql_query($sql);
$num_rows = mysql_num_rows($result);



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="Expires" CONTENT="+1">
<TITLE>Oxford Instruments: Oi Vision</TITLE>
<META NAME="keywords" CONTENT="Nanotechnology, XRF analyzers, micro-analysis systems, superconducting wires, NMR magnets, cryogenic systems, plasma etch deposition, low temperature environments, coating thickness measurement, X-ray Fluorescence, EDS micro-analysis, Oxford Instruments, OiVision, Oi Vision">
<META NAME="description" CONTENT="Oxford Instruments specializes in the design, manufacture and support of hi-tech tools and systems for industry research.">
<META NAME="author" CONTENT="Justin Davis">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link rel="icon" type="image/png" href="/images/OIIcon.png
" />
<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="menu/menu_style.css" />
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot/excanvas.min.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="js/flot/jquery.js"></script>
<script language="javascript" type="text/javascript" src="js/flot/jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="js/flot/jquery.flot.navigate.js"></script>
 <!--
You are free to copy and use this sample in accordance with the terms of the
Apache license (http://www.apache.org/licenses/LICENSE-2.0.html)
-->

  <head>


    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>
      Google Visualization API Sample
    </title>
<script language="javascript">
  function toggleDiv(divid){
    if(document.getElementById(divid).style.display == 'none'){
      document.getElementById(divid).style.display = 'block';
    }else{
      document.getElementById(divid).style.display = 'none';
    }
  }
</script>
<!-- Helium Level -->
<script id="source">
$(function () {
var he = [
<?php
		$h1 = $avg;
		$h2 = $avg;
		
		while($row = mysql_fetch_array($result))
		{
			if((double)$row['He'] != 111.11)
			{
				//if((double)$row['He'] < $avg + 2)
				$h1 = $h2;
				$h2 = (double)$row['He'];
				
				if((double)$row['He'] < (($h1 + $h2) / 2))
				{
					//echo "[". (strtotime($row['date']) + 7200) * 1000 .", ". $row['He'] ."], ";
					echo "[". (strtotime($row['date'])) * 1000 .", ". $row['He'] ."], ";
				}
			}
			
		}
		$result=mysql_query($sql);
		echo "];";
		?>
		

		 var data = [ { data: he, label: "Helium", color: 0 } ];

    
    var placeholder = $("#HeliumGraph");
    var options = {
        series: { lines: { show: true }, shadowSize: 1 },
        xaxis: { mode: "time",
				 minTickSize: [1, "hour"],
				 maxTickSize: [1, "month"] },
        yaxis: { zoomRange: [0, 100], panRange: [0, 100], minTickSize: 0.1 },
        zoom: {
            interactive: true
        },
        pan: {
            interactive: true
        },
			
    };

    var plot = $.plot(placeholder, data, options);

    // show pan/zoom messages to illustrate events 
    placeholder.bind('plotpan', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Panning to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    placeholder.bind('plotzoom', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Zooming to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    // add zoom out button 
    $('<div class="button" style="right:20px;top:20px">zoom out</div>').appendTo(placeholder).click(function (e) {
        e.preventDefault();
        plot.zoomOut();
    });
	
	//Zoom to whole plot
	 $("#whole").click(function () {
        $.plot(placeholder, data, options);
    });
	
    // and add panning buttons
    
    // little helper for taking the repetitive work out of placing
    // panning arrows
    function addArrow(dir, right, top, offset) {
        $('<img class="button" src="js/flot/examples/arrow-' + dir + '.gif" style="right:' + right + 'px;top:' + top + 'px">').appendTo(placeholder).click(function (e) {
            e.preventDefault();
            plot.pan(offset);
        });
    }

    addArrow('left', 155, 60, { left: -100 });
    addArrow('right', 25, 60, { left: 100 });
    addArrow('up', 40, 45, { top: -100 });
    addArrow('down', 40, 75, { top: 100 });
		



});
</script>

<!--Helium Pressure-->
<script id="source">
$(function () {
var hep = [
<?php		
		while($row = mysql_fetch_array($result))
		{				
				if((double)$row['HeP'] > -0.01 )
				{
					//echo "[". (strtotime($row['date']) + 7200) * 1000 .", ". $row['HeP'] ."], ";
					echo "[". (strtotime($row['date'])) * 1000 .", ". $row['HeP'] ."], ";
				}
		}
		$result=mysql_query($sql);
		echo "];";
		?>
		
		
		 var data = [ { data: hep, label: "Pressure", color: 1 } ];

    
    var placeholder = $("#PressureGraph");
    var options = {
        series: { lines: { show: true }, shadowSize: 1 },
        xaxis: { mode: "time",
				 minTickSize: [1, "hour"] },
        yaxis: { zoomRange: [0, 100], panRange: [0, 100], minTickSize: 0.1 },
        zoom: {
            interactive: true
        },
        pan: {
            interactive: true
        },
			
    };

    var plot = $.plot(placeholder, data, options);

    // show pan/zoom messages to illustrate events 
    placeholder.bind('plotpan', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Panning to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    placeholder.bind('plotzoom', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Zooming to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    // add zoom out button 
    $('<div class="button" style="right:20px;top:20px">zoom out</div>').appendTo(placeholder).click(function (e) {
        e.preventDefault();
        plot.zoomOut();
    });
	
	//Zoom to whole plot
	 $("#whole").click(function () {
        $.plot(placeholder, data, options);
    });
    // and add panning buttons
    
    // little helper for taking the repetitive work out of placing
    // panning arrows
    function addArrow(dir, right, top, offset) {
        $('<img class="button" src="js/flot/examples/arrow-' + dir + '.gif" style="right:' + right + 'px;top:' + top + 'px">').appendTo(placeholder).click(function (e) {
            e.preventDefault();
            plot.pan(offset);
        });
    }

    addArrow('left', 155, 60, { left: -100 });
    addArrow('right', 25, 60, { left: 100 });
    addArrow('up', 40, 45, { top: -100 });
    addArrow('down', 40, 75, { top: 100 });
		



});
</script>

<!--Water Temp-->
<script id="source">
$(function () {
var wtf = [
<?php		
		while($row = mysql_fetch_array($result))
		{				
				if((double)$row['Wtf'] > -0.01 )
				{
					//echo "[". (strtotime($row['date']) + 7200) * 1000 .", ". $row['Wtf'] ."], ";
					echo "[". (strtotime($row['date'])) * 1000 .", ". $row['Wtf'] ."], ";
				}
		}
		$result=mysql_query($sql);
		echo "];";
		?>
		
		
		 var data = [ { data: wtf, label: "Water Temp Deg F", color: 2 } ];

    
    var placeholder = $("#WaterTempGraph");
    var options = {
        series: { lines: { show: true }, shadowSize: 1 },
        xaxis: { mode: "time",
				 minTickSize: [1, "hour"] },
        yaxis: { zoomRange: [0, 100], panRange: [0, 100], minTickSize: 0.1 },
        zoom: {
            interactive: true
        },
        pan: {
            interactive: true
        },
			
    };

    var plot = $.plot(placeholder, data, options);

    // show pan/zoom messages to illustrate events 
    placeholder.bind('plotpan', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Panning to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    placeholder.bind('plotzoom', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Zooming to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    // add zoom out button 
    $('<div class="button" style="right:20px;top:20px">zoom out</div>').appendTo(placeholder).click(function (e) {
        e.preventDefault();
        plot.zoomOut();
    });
	//Zoom to whole plot
	 $("#whole").click(function () {
        $.plot(placeholder, data, options);
    });	

    // and add panning buttons
    
    // little helper for taking the repetitive work out of placing
    // panning arrows
    function addArrow(dir, right, top, offset) {
        $('<img class="button" src="js/flot/examples/arrow-' + dir + '.gif" style="right:' + right + 'px;top:' + top + 'px">').appendTo(placeholder).click(function (e) {
            e.preventDefault();
            plot.pan(offset);
        });
    }

    addArrow('left', 155, 60, { left: -100 });
    addArrow('right', 25, 60, { left: 100 });
    addArrow('up', 40, 45, { top: -100 });
    addArrow('down', 40, 75, { top: 100 });
		



});
</script>
      
<!--Water Flow-->
<script id="source">
$(function () {
var wf = [
<?php		
		while($row = mysql_fetch_array($result))
		{				
				if((double)$row['Wf'] > -0.01 )
				{
					//echo "[". (strtotime($row['date']) + 7200) * 1000 .", ". $row['Wf'] ."], ";
					echo "[". (strtotime($row['date'])) * 1000 .", ". $row['Wf'] ."], ";
				}
		}
		$result=mysql_query($sql);
		echo "];";
		?>
		
		
		 var data = [ { data: wf, label: "Water Flow GPM", color: 3 } ];

    
    var placeholder = $("#WaterFlowGraph");
    var options = {
        series: { lines: { show: true }, shadowSize: 1 },
        xaxis: { mode: "time",
				 minTickSize: [1, "hour"] },
        yaxis: { zoomRange: [0, 100], panRange: [0, 100], minTickSize: 0.1 },
        zoom: {
            interactive: true
        },
        pan: {
            interactive: true
        },
			
    };

    var plot = $.plot(placeholder, data, options);

    // show pan/zoom messages to illustrate events 
    placeholder.bind('plotpan', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Panning to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    placeholder.bind('plotzoom', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Zooming to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    // add zoom out button 
    $('<div class="button" style="right:20px;top:20px">zoom out</div>').appendTo(placeholder).click(function (e) {
        e.preventDefault();
        plot.zoomOut();
    });
	//Zoom to whole plot
	 $("#whole").click(function () {
        $.plot(placeholder, data, options);
    });	

    // and add panning buttons
    
    // little helper for taking the repetitive work out of placing
    // panning arrows
    function addArrow(dir, right, top, offset) {
        $('<img class="button" src="js/flot/examples/arrow-' + dir + '.gif" style="right:' + right + 'px;top:' + top + 'px">').appendTo(placeholder).click(function (e) {
            e.preventDefault();
            plot.pan(offset);
        });
    }

    addArrow('left', 155, 60, { left: -100 });
    addArrow('right', 25, 60, { left: 100 });
    addArrow('up', 40, 45, { top: -100 });
    addArrow('down', 40, 75, { top: 100 });
		



});
</script>       

<!--Room Temp-->
<script id="source">
$(function () {
var Tf = [
<?php		
		while($row = mysql_fetch_array($result))
		{				
				if((double)$row['Tf'] > -0.01 )
				{
					//echo "[". (strtotime($row['date']) + 7200) * 1000 .", ". $row['Tf'] ."], ";
					echo "[". (strtotime($row['date'])) * 1000 .", ". $row['Tf'] ."], ";
				}
		}
		$result=mysql_query($sql);
		echo "];";
		?>
		
		
		 var data = [ { data: Tf, label: "Equip Room Temp Deg F", color: 4 } ];

    
    var placeholder = $("#RoomTempGraph");
    var options = {
        series: { lines: { show: true }, shadowSize: 1 },
        xaxis: { mode: "time",
				 minTickSize: [1, "hour"] },
        yaxis: { zoomRange: [0, 100], panRange: [0, 100], minTickSize: 0.1 },
        zoom: {
            interactive: true
        },
        pan: {
            interactive: true
        },
			
    };

    var plot = $.plot(placeholder, data, options);

    // show pan/zoom messages to illustrate events 
    placeholder.bind('plotpan', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Panning to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    placeholder.bind('plotzoom', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Zooming to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    // add zoom out button 
    $('<div class="button" style="right:20px;top:20px">zoom out</div>').appendTo(placeholder).click(function (e) {
        e.preventDefault();
        plot.zoomOut();
    });
	//Zoom to whole plot
	 $("#whole").click(function () {
        $.plot(placeholder, data, options);
    });	

    // and add panning buttons
    
    // little helper for taking the repetitive work out of placing
    // panning arrows
    function addArrow(dir, right, top, offset) {
        $('<img class="button" src="js/flot/examples/arrow-' + dir + '.gif" style="right:' + right + 'px;top:' + top + 'px">').appendTo(placeholder).click(function (e) {
            e.preventDefault();
            plot.pan(offset);
        });
    }

    addArrow('left', 155, 60, { left: -100 });
    addArrow('right', 25, 60, { left: 100 });
    addArrow('up', 40, 45, { top: -100 });
    addArrow('down', 40, 75, { top: 100 });
		



});
</script>
   
<!--Water Flow-->
<script id="source">
$(function () {
var Rh = [
<?php		
		while($row = mysql_fetch_array($result))
		{				
				if((double)$row['Rh'] > -0.01 )
				{
					//echo "[". (strtotime($row['date']) + 7200) * 1000 .", ". $row['Rh'] ."], ";
					echo "[". (strtotime($row['date'])) * 1000 .", ". $row['Rh'] ."], ";
				}
		}
		$result=mysql_query($sql);
		echo "];";
		?>
		
		
		 var data = [ { data: Rh, label: "Room Humidity %RH", color: 5 } ];

    
    var placeholder = $("#RoomRHGraph");
    var options = {
        series: { lines: { show: true }, shadowSize: 1 },
        xaxis: { mode: "time",
				 minTickSize: [1, "hour"] },
        yaxis: { zoomRange: [0, 100], panRange: [0, 100], minTickSize: 0.1 },
        zoom: {
            interactive: true
        },
        pan: {
            interactive: true
        },
			
    };

    var plot = $.plot(placeholder, data, options);

    // show pan/zoom messages to illustrate events 
    placeholder.bind('plotpan', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Panning to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    placeholder.bind('plotzoom', function (event, plot) {
        var axes = plot.getAxes();
        $(".message").html("Zooming to x: "  + axes.xaxis.min.toFixed(2)
                           + " &ndash; " + axes.xaxis.max.toFixed(2)
                           + " and y: " + axes.yaxis.min.toFixed(2)
                           + " &ndash; " + axes.yaxis.max.toFixed(2));
    });

    // add zoom out button 
    $('<div class="button" style="right:20px;top:20px">zoom out</div>').appendTo(placeholder).click(function (e) {
        e.preventDefault();
        plot.zoomOut();
    });
	//Zoom to whole plot
	 $("#whole").click(function () {
        $.plot(placeholder, data, options);
    });	

    // and add panning buttons
    
    // little helper for taking the repetitive work out of placing
    // panning arrows
    function addArrow(dir, right, top, offset) {
        $('<img class="button" src="js/flot/examples/arrow-' + dir + '.gif" style="right:' + right + 'px;top:' + top + 'px">').appendTo(placeholder).click(function (e) {
            e.preventDefault();
            plot.pan(offset);
        });
    }

    addArrow('left', 155, 60, { left: -100 });
    addArrow('right', 25, 60, { left: 100 });
    addArrow('up', 40, 45, { top: -100 });
    addArrow('down', 40, 75, { top: 100 });
		



});
</script>   

</head>
<body>

<div id="center-x">

<div id="header"></div>
<div id="menu"><?php include("menu/menu.php");?></div>
<br>

<div class="bodytext" style="margin:15px;margin-top:5px;">
  <div id="main-box" style="padding-left:30px;"></div>
  <h2>Graphs and Raw Data (<?php echo $view_site_days; ?> days) for <strong><?php echo $rowSite['site_name']; ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;Site ID:&nbsp;<strong><?php echo $rowSite['site_id']; ?></strong></h2>
  <br />
  Drag to pan, double click to zoom (or use the mouse scrollwheel). &nbsp;&nbsp; Reload page to center graphs.
  <br />
  <br />
  <div id="HeliumGraph" style="width:800px;height:400px; margin-bottom:100px; margin-left:auto; margin-right:auto"></div>
  <div id="PressureGraph" style="width:800px;height:200px; margin-bottom:100px; margin-left:auto; margin-right:auto"></div>
  <div id="WaterTempGraph" style="width:800px;height:200px; margin-bottom:100px; margin-left:auto; margin-right:auto"></div>
  <div id="WaterFlowGraph" style="width:800px;height:200px; margin-bottom:100px; margin-left:auto; margin-right:auto"></div>
  <div id="RoomTempGraph" style="width:800px;height:200px; margin-bottom:100px; margin-left:auto; margin-right:auto"></div>
  <div id="RoomRHGraph" style="width:800px;height:200px; margin-bottom:100px; margin-left:auto; margin-right:auto"></div>
    <br />
    <br />  
    <a href="javascript:;" onmousedown="toggleDiv('visualization4');">Show/Hide Raw Data</a>
    </div>
<div id="visualization4" style="display:none">
<?php
echo "Displaying " . $num_rows . " readings.\n";
echo "<br />\n";
echo "<table border='1' cellpadding='5'>\n
<tr>
 <th>Date</th>
 <th>Room Temp C</th>
 <th>Room Temp F</th>
 <th>Room %RH</th>
 <th>He Level</th>
 <th>He Pressure</th>
 <th>Water Flow</th>
 <th>Water Temp C</th>
 <th>Water Temp F</th>
 <th>Magnet Field</th>
 <th>Compressor On</th>
</tr>\n";
while($row = mysql_fetch_array($result))
  {
  echo "<tr>\n";
  echo "  <td>" . $row['date'] . "</td>\n";
  echo "  <td>" . $row['Tc'] . "</td>\n";
  echo "  <td>" . $row['Tf'] . "</td>\n";
  echo "  <td>" . $row['Rh'] . "</td>\n";
  if((double)$row['He'] == 111.11)
  {
  	echo "  <td>NA</td>\n";
  }
  else
  {
  	echo "  <td>" . $row['He'] . "</td>\n";
  }
  echo "  <td>" . $row['HeP'] . "</td>\n";
  echo "  <td>" . $row['Wf'] . "</td>\n";
  echo "  <td>" . $row['Wtc'] . "</td>\n";
  echo "  <td>" . $row['Wtf'] . "</td>\n";
  echo "  <td>" . $row['FieldOn'] . "</td>\n";
  echo "  <td>" . $row['CompOn'] . "</td>\n";
  echo "</tr>\n";
  }
echo "</table>";
echo "<br />";
?>
</div> 

<br />


<div id="footer"></div>
</div>
</body>
</html>
